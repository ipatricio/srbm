/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : remax

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-10-25 23:22:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `anexos`
-- ----------------------------
DROP TABLE IF EXISTS `anexos`;
CREATE TABLE `anexos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documento` text NOT NULL,
  `comentario` varchar(45) DEFAULT NULL,
  `areas_id` int(11) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_anexos_remax_areas1_idx` (`areas_id`),
  KEY `fk_remax_anexos_remax_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_remax_anexos_remax_areas1` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remax_anexos_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of anexos
-- ----------------------------
INSERT INTO `anexos` VALUES ('3', '', 'asd', '1', '2');
INSERT INTO `anexos` VALUES ('4', '', '', '1', '2');
INSERT INTO `anexos` VALUES ('5', '', '', '1', '2');

-- ----------------------------
-- Table structure for `areas`
-- ----------------------------
DROP TABLE IF EXISTS `areas`;
CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of areas
-- ----------------------------
INSERT INTO `areas` VALUES ('1', 'Fonoaudiologia');
INSERT INTO `areas` VALUES ('2', 'Medicina');
INSERT INTO `areas` VALUES ('3', 'Odontologia');
INSERT INTO `areas` VALUES ('4', 'Psicologia');

-- ----------------------------
-- Table structure for `areauser`
-- ----------------------------
DROP TABLE IF EXISTS `areauser`;
CREATE TABLE `areauser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of areauser
-- ----------------------------
INSERT INTO `areauser` VALUES ('1', 'Engenharia Biomedica');
INSERT INTO `areauser` VALUES ('2', 'Engenharia Mecanica');
INSERT INTO `areauser` VALUES ('3', 'Fonoaudiologia');
INSERT INTO `areauser` VALUES ('4', 'Liga');
INSERT INTO `areauser` VALUES ('5', 'Medicina');
INSERT INTO `areauser` VALUES ('6', 'Odontologia');
INSERT INTO `areauser` VALUES ('7', 'Psicologia');

-- ----------------------------
-- Table structure for `cidcancer`
-- ----------------------------
DROP TABLE IF EXISTS `cidcancer`;
CREATE TABLE `cidcancer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cidcancer
-- ----------------------------
INSERT INTO `cidcancer` VALUES ('1', 'cabeca');
INSERT INTO `cidcancer` VALUES ('2', 'pescoco');
INSERT INTO `cidcancer` VALUES ('3', 'cabeca_pescoco');

-- ----------------------------
-- Table structure for `classificacaofala`
-- ----------------------------
DROP TABLE IF EXISTS `classificacaofala`;
CREATE TABLE `classificacaofala` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classificacaofala
-- ----------------------------
INSERT INTO `classificacaofala` VALUES ('1', 'inteligibilidade de fala normal');
INSERT INTO `classificacaofala` VALUES ('2', 'inteligibilidade de fala com comprometimento leve');
INSERT INTO `classificacaofala` VALUES ('3', 'inteligibilidade de fala com comprometimento moderado');
INSERT INTO `classificacaofala` VALUES ('4', 'inteligibilidade com comprometimento grave');

-- ----------------------------
-- Table structure for `classificacaofonoarticular`
-- ----------------------------
DROP TABLE IF EXISTS `classificacaofonoarticular`;
CREATE TABLE `classificacaofonoarticular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classificacaofonoarticular
-- ----------------------------
INSERT INTO `classificacaofonoarticular` VALUES ('1', 'lenta');
INSERT INTO `classificacaofonoarticular` VALUES ('2', 'normal');
INSERT INTO `classificacaofonoarticular` VALUES ('3', 'aumentada');

-- ----------------------------
-- Table structure for `coordenacaofonoarticular`
-- ----------------------------
DROP TABLE IF EXISTS `coordenacaofonoarticular`;
CREATE TABLE `coordenacaofonoarticular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coordenacaofonoarticular
-- ----------------------------
INSERT INTO `coordenacaofonoarticular` VALUES ('1', 'presente');
INSERT INTO `coordenacaofonoarticular` VALUES ('2', 'ausente');

-- ----------------------------
-- Table structure for `estadocivil`
-- ----------------------------
DROP TABLE IF EXISTS `estadocivil`;
CREATE TABLE `estadocivil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estadocivil
-- ----------------------------
INSERT INTO `estadocivil` VALUES ('1', 'Solteiro');
INSERT INTO `estadocivil` VALUES ('2', 'Casado(a)');

-- ----------------------------
-- Table structure for `estoque`
-- ----------------------------
DROP TABLE IF EXISTS `estoque`;
CREATE TABLE `estoque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estoque
-- ----------------------------

-- ----------------------------
-- Table structure for `fabricacao`
-- ----------------------------
DROP TABLE IF EXISTS `fabricacao`;
CREATE TABLE `fabricacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fabricacao
-- ----------------------------
INSERT INTO `fabricacao` VALUES ('1', 'CAD/CAM');
INSERT INTO `fabricacao` VALUES ('2', 'Comercial');

-- ----------------------------
-- Table structure for `grupo`
-- ----------------------------
DROP TABLE IF EXISTS `grupo`;
CREATE TABLE `grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grupo
-- ----------------------------
INSERT INTO `grupo` VALUES ('1', 'Administrador');

-- ----------------------------
-- Table structure for `grupos`
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grupos
-- ----------------------------
INSERT INTO `grupos` VALUES ('1', 'Administrador');
INSERT INTO `grupos` VALUES ('2', 'Padrao');
INSERT INTO `grupos` VALUES ('3', 'Professores');
INSERT INTO `grupos` VALUES ('7', 'Fonoaudiologos');
INSERT INTO `grupos` VALUES ('8', 'Medicina');
INSERT INTO `grupos` VALUES ('9', 'Psicologia');
INSERT INTO `grupos` VALUES ('10', 'Odontologia');

-- ----------------------------
-- Table structure for `hiperhiponasalidade`
-- ----------------------------
DROP TABLE IF EXISTS `hiperhiponasalidade`;
CREATE TABLE `hiperhiponasalidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hiperhiponasalidade
-- ----------------------------
INSERT INTO `hiperhiponasalidade` VALUES ('1', 'ausente');
INSERT INTO `hiperhiponasalidade` VALUES ('2', 'leve');
INSERT INTO `hiperhiponasalidade` VALUES ('3', 'moderado');
INSERT INTO `hiperhiponasalidade` VALUES ('4', 'grave');
INSERT INTO `hiperhiponasalidade` VALUES ('5', 'muito grave');

-- ----------------------------
-- Table structure for `imgmedicina`
-- ----------------------------
DROP TABLE IF EXISTS `imgmedicina`;
CREATE TABLE `imgmedicina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of imgmedicina
-- ----------------------------
INSERT INTO `imgmedicina` VALUES ('1', 'Tomografia Computadorizada');
INSERT INTO `imgmedicina` VALUES ('2', 'Ressonancia Magnetica');
INSERT INTO `imgmedicina` VALUES ('3', 'Raio X');
INSERT INTO `imgmedicina` VALUES ('4', 'Outros');

-- ----------------------------
-- Table structure for `instituicao`
-- ----------------------------
DROP TABLE IF EXISTS `instituicao`;
CREATE TABLE `instituicao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of instituicao
-- ----------------------------
INSERT INTO `instituicao` VALUES ('1', 'UFRN');
INSERT INTO `instituicao` VALUES ('2', 'UERN');
INSERT INTO `instituicao` VALUES ('3', 'Liga');

-- ----------------------------
-- Table structure for `laboratorio`
-- ----------------------------
DROP TABLE IF EXISTS `laboratorio`;
CREATE TABLE `laboratorio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `cnpj` varchar(30) NOT NULL,
  `cnae` varchar(30) NOT NULL,
  `uf` varchar(45) NOT NULL,
  `cidade` varchar(45) NOT NULL,
  `logradouro` varchar(45) NOT NULL,
  `numero` int(11) NOT NULL,
  `cep` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `telefone1` varchar(20) NOT NULL,
  `telefone2` varchar(20) NOT NULL,
  `nome_resp` varchar(45) NOT NULL,
  `datadenascimento` date NOT NULL,
  `cpf` varchar(20) NOT NULL,
  `identidade` varchar(20) NOT NULL,
  `uf_resp` varchar(45) NOT NULL,
  `cidade_resp` varchar(45) NOT NULL,
  `cep_resp` int(11) NOT NULL,
  `endereco_resp` varchar(45) NOT NULL,
  `complemento` varchar(45) DEFAULT NULL,
  `numero_resp` int(11) NOT NULL,
  `email_resp` varchar(45) NOT NULL,
  `qualificacao` varchar(45) NOT NULL,
  `telefone1_resp` int(11) NOT NULL,
  `telefone2_resp` int(11) NOT NULL,
  `bairro_lab` varchar(100) NOT NULL,
  `bairro_resp` varchar(100) NOT NULL,
  `sexo_id` int(11) NOT NULL,
  `nacionalidade_id` int(11) NOT NULL,
  `estadoCivil_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_laboratorio_sexo1_idx` (`sexo_id`),
  KEY `fk_laboratorio_nacionalidade1_idx` (`nacionalidade_id`),
  KEY `fk_laboratorio_estadoCivil1_idx` (`estadoCivil_id`),
  CONSTRAINT `fk_laboratorio_estadoCivil1` FOREIGN KEY (`estadoCivil_id`) REFERENCES `estadocivil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_laboratorio_nacionalidade1` FOREIGN KEY (`nacionalidade_id`) REFERENCES `nacionalidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_laboratorio_sexo1` FOREIGN KEY (`sexo_id`) REFERENCES `sexo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of laboratorio
-- ----------------------------

-- ----------------------------
-- Table structure for `login`
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  `grupos_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`),
  KEY `fk_login_grupos1_idx` (`grupos_id`),
  CONSTRAINT `fk_login_grupos1` FOREIGN KEY (`grupos_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES ('1', 'italo', '202cb962ac59075b964b07152d234b70', '1');
INSERT INTO `login` VALUES ('2', 'wewe', '202cb962ac59075b964b07152d234b70', '3');
INSERT INTO `login` VALUES ('3', 'fono', '202cb962ac59075b964b07152d234b70', '7');
INSERT INTO `login` VALUES ('4', 'odonto', '202cb962ac59075b964b07152d234b70', '10');
INSERT INTO `login` VALUES ('5', 'medico', '202cb962ac59075b964b07152d234b70', '8');
INSERT INTO `login` VALUES ('6', 'psicologo', '202cb962ac59075b964b07152d234b70', '9');

-- ----------------------------
-- Table structure for `loudess`
-- ----------------------------
DROP TABLE IF EXISTS `loudess`;
CREATE TABLE `loudess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of loudess
-- ----------------------------
INSERT INTO `loudess` VALUES ('1', 'adequada');
INSERT INTO `loudess` VALUES ('2', 'aumentada');
INSERT INTO `loudess` VALUES ('3', 'reduzida');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  `valor` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'Menu', 'menus');
INSERT INTO `menus` VALUES ('2', 'Acesso/Permissao', 'menusacesso');
INSERT INTO `menus` VALUES ('3', 'Usuarios acesso', 'login');
INSERT INTO `menus` VALUES ('4', 'Grupo', 'grupos');
INSERT INTO `menus` VALUES ('5', 'Ações', 'menusacoes');
INSERT INTO `menus` VALUES ('6', 'Paciente', 'paciente');
INSERT INTO `menus` VALUES ('7', 'Fonoaudiologia', 'fono');
INSERT INTO `menus` VALUES ('8', 'Relatorio fonoaudiologia', 'relatoriofonoaudiologia');
INSERT INTO `menus` VALUES ('9', 'Medicina', 'medi');
INSERT INTO `menus` VALUES ('10', 'Odontologia', 'odonto');
INSERT INTO `menus` VALUES ('11', 'Usuario', 'usuario');

-- ----------------------------
-- Table structure for `menus_acesso`
-- ----------------------------
DROP TABLE IF EXISTS `menus_acesso`;
CREATE TABLE `menus_acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menus_id` int(11) NOT NULL,
  `grupos_id` int(11) NOT NULL,
  `menus_acoes_id` int(11) NOT NULL,
  `situacao` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_menus_acesso_menus_idx` (`menus_id`),
  KEY `fk_menus_acesso_grupos1_idx` (`grupos_id`),
  KEY `fk_menus_acesso_menu_acoes1_idx` (`menus_acoes_id`),
  CONSTRAINT `fk_menus_acesso_grupos1` FOREIGN KEY (`grupos_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_menus_acesso_menus` FOREIGN KEY (`menus_id`) REFERENCES `menus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_menus_acesso_menu_acoes1` FOREIGN KEY (`menus_acoes_id`) REFERENCES `menus_acoes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus_acesso
-- ----------------------------
INSERT INTO `menus_acesso` VALUES ('1', '1', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('3', '1', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('4', '1', '1', '4', '1');
INSERT INTO `menus_acesso` VALUES ('6', '1', '1', '5', '1');
INSERT INTO `menus_acesso` VALUES ('7', '1', '1', '6', '1');
INSERT INTO `menus_acesso` VALUES ('8', '1', '1', '7', '1');
INSERT INTO `menus_acesso` VALUES ('10', '4', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('11', '4', '1', '2', '1');
INSERT INTO `menus_acesso` VALUES ('13', '4', '1', '7', '1');
INSERT INTO `menus_acesso` VALUES ('14', '2', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('15', '2', '1', '2', '1');
INSERT INTO `menus_acesso` VALUES ('16', '2', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('17', '2', '1', '4', '1');
INSERT INTO `menus_acesso` VALUES ('18', '2', '1', '7', '1');
INSERT INTO `menus_acesso` VALUES ('19', '2', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('20', '2', '1', '6', '1');
INSERT INTO `menus_acesso` VALUES ('22', '1', '1', '2', '1');
INSERT INTO `menus_acesso` VALUES ('24', '4', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('25', '4', '1', '4', '1');
INSERT INTO `menus_acesso` VALUES ('26', '6', '3', '1', '1');
INSERT INTO `menus_acesso` VALUES ('27', '6', '3', '2', '1');
INSERT INTO `menus_acesso` VALUES ('28', '6', '3', '3', '1');
INSERT INTO `menus_acesso` VALUES ('29', '6', '3', '5', '1');
INSERT INTO `menus_acesso` VALUES ('30', '6', '3', '7', '1');
INSERT INTO `menus_acesso` VALUES ('31', '3', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('32', '3', '1', '7', '1');
INSERT INTO `menus_acesso` VALUES ('33', '1', '3', '1', '1');
INSERT INTO `menus_acesso` VALUES ('34', '2', '1', '5', '1');
INSERT INTO `menus_acesso` VALUES ('35', '7', '7', '1', '1');
INSERT INTO `menus_acesso` VALUES ('37', '7', '7', '7', '1');
INSERT INTO `menus_acesso` VALUES ('38', '3', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('39', '7', '7', '3', '1');
INSERT INTO `menus_acesso` VALUES ('40', '8', '7', '1', '1');
INSERT INTO `menus_acesso` VALUES ('41', '8', '7', '2', '1');
INSERT INTO `menus_acesso` VALUES ('42', '8', '7', '3', '1');
INSERT INTO `menus_acesso` VALUES ('43', '8', '7', '4', '1');
INSERT INTO `menus_acesso` VALUES ('44', '8', '7', '5', '1');
INSERT INTO `menus_acesso` VALUES ('45', '8', '7', '6', '1');
INSERT INTO `menus_acesso` VALUES ('46', '7', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('47', '7', '1', '2', '1');
INSERT INTO `menus_acesso` VALUES ('48', '7', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('49', '7', '1', '4', '1');
INSERT INTO `menus_acesso` VALUES ('50', '7', '1', '5', '1');
INSERT INTO `menus_acesso` VALUES ('51', '7', '1', '6', '1');
INSERT INTO `menus_acesso` VALUES ('52', '7', '1', '7', '1');
INSERT INTO `menus_acesso` VALUES ('53', '8', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('54', '8', '1', '2', '1');
INSERT INTO `menus_acesso` VALUES ('55', '8', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('56', '8', '1', '4', '1');
INSERT INTO `menus_acesso` VALUES ('57', '8', '1', '5', '1');
INSERT INTO `menus_acesso` VALUES ('58', '8', '1', '6', '1');
INSERT INTO `menus_acesso` VALUES ('84', '8', '1', '7', '0');
INSERT INTO `menus_acesso` VALUES ('85', '9', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('86', '9', '1', '2', '1');
INSERT INTO `menus_acesso` VALUES ('87', '9', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('88', '9', '1', '4', '1');
INSERT INTO `menus_acesso` VALUES ('89', '9', '1', '5', '1');
INSERT INTO `menus_acesso` VALUES ('90', '9', '1', '6', '1');
INSERT INTO `menus_acesso` VALUES ('91', '9', '1', '7', '1');
INSERT INTO `menus_acesso` VALUES ('92', '10', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('93', '10', '1', '2', '1');
INSERT INTO `menus_acesso` VALUES ('94', '10', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('95', '10', '1', '4', '1');
INSERT INTO `menus_acesso` VALUES ('96', '10', '1', '5', '1');
INSERT INTO `menus_acesso` VALUES ('97', '10', '1', '6', '1');
INSERT INTO `menus_acesso` VALUES ('98', '10', '1', '7', '1');
INSERT INTO `menus_acesso` VALUES ('99', '11', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('100', '11', '1', '2', '1');
INSERT INTO `menus_acesso` VALUES ('101', '11', '1', '3', '1');
INSERT INTO `menus_acesso` VALUES ('102', '11', '1', '4', '1');
INSERT INTO `menus_acesso` VALUES ('103', '11', '1', '5', '1');
INSERT INTO `menus_acesso` VALUES ('104', '11', '1', '6', '1');
INSERT INTO `menus_acesso` VALUES ('105', '11', '1', '7', '1');

-- ----------------------------
-- Table structure for `menus_acoes`
-- ----------------------------
DROP TABLE IF EXISTS `menus_acoes`;
CREATE TABLE `menus_acoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `abamenu` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus_acoes
-- ----------------------------
INSERT INTO `menus_acoes` VALUES ('1', 'Cadastrar', 'create', '1');
INSERT INTO `menus_acoes` VALUES ('2', 'Listar', 'admin', '1');
INSERT INTO `menus_acoes` VALUES ('3', 'Visualizar', 'view', '0');
INSERT INTO `menus_acoes` VALUES ('4', 'Excluir/Deletar', 'delete', '0');
INSERT INTO `menus_acoes` VALUES ('5', 'Atualizar', 'update', '0');
INSERT INTO `menus_acoes` VALUES ('6', 'Inicio', 'index', '1');
INSERT INTO `menus_acoes` VALUES ('7', 'Aba Menu', '#', '1');

-- ----------------------------
-- Table structure for `model`
-- ----------------------------
DROP TABLE IF EXISTS `model`;
CREATE TABLE `model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of model
-- ----------------------------
INSERT INTO `model` VALUES ('1', 'anexos');
INSERT INTO `model` VALUES ('2', 'areas');
INSERT INTO `model` VALUES ('3', 'areauser');
INSERT INTO `model` VALUES ('4', 'cidcancer');
INSERT INTO `model` VALUES ('5', 'classificacaofala');
INSERT INTO `model` VALUES ('6', 'classificacaofonoarticular');
INSERT INTO `model` VALUES ('7', 'coordenacaofonoarticular');
INSERT INTO `model` VALUES ('8', 'estadocivil');
INSERT INTO `model` VALUES ('9', 'estoque');
INSERT INTO `model` VALUES ('10', 'fabricacao');
INSERT INTO `model` VALUES ('11', 'hiperhiponasalidade');
INSERT INTO `model` VALUES ('12', 'imgmedicina');
INSERT INTO `model` VALUES ('13', 'instituicao');
INSERT INTO `model` VALUES ('14', 'laboratorio');
INSERT INTO `model` VALUES ('15', 'login');
INSERT INTO `model` VALUES ('16', 'loudess');
INSERT INTO `model` VALUES ('17', 'nacionalidade');
INSERT INTO `model` VALUES ('18', 'nivelcomprometimento');
INSERT INTO `model` VALUES ('19', 'paciente');
INSERT INTO `model` VALUES ('20', 'pitchvocal');
INSERT INTO `model` VALUES ('21', 'proced_has_proce_medicina');
INSERT INTO `model` VALUES ('22', 'procedimentocirurgia');
INSERT INTO `model` VALUES ('23', 'procedimentodentistica');
INSERT INTO `model` VALUES ('24', 'procedimentoendodontia');
INSERT INTO `model` VALUES ('25', 'procedimentofonoaudiologia');
INSERT INTO `model` VALUES ('26', 'procedimentoperiodontia');
INSERT INTO `model` VALUES ('27', 'procedimentoprotese');
INSERT INTO `model` VALUES ('28', 'procedmedicina');
INSERT INTO `model` VALUES ('29', 'procemedicina');
INSERT INTO `model` VALUES ('30', 'protese');
INSERT INTO `model` VALUES ('31', 'relatorio_medicina');
INSERT INTO `model` VALUES ('32', 'relatoriofonoaudiologia');
INSERT INTO `model` VALUES ('33', 'relatorioodontologia');
INSERT INTO `model` VALUES ('34', 'relatoriopsicologia');
INSERT INTO `model` VALUES ('35', 'resposta');
INSERT INTO `model` VALUES ('36', 'ressonanciavocal');
INSERT INTO `model` VALUES ('37', 'sexo');
INSERT INTO `model` VALUES ('38', 'tipoarticular');
INSERT INTO `model` VALUES ('39', 'usuario');
INSERT INTO `model` VALUES ('40', 'velocidadefala');
INSERT INTO `model` VALUES ('41', 'vinculoprojeto');

-- ----------------------------
-- Table structure for `nacionalidade`
-- ----------------------------
DROP TABLE IF EXISTS `nacionalidade`;
CREATE TABLE `nacionalidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nacionalidade
-- ----------------------------
INSERT INTO `nacionalidade` VALUES ('1', 'Brasileiro(a)');
INSERT INTO `nacionalidade` VALUES ('2', 'Estrangeiro(a)');

-- ----------------------------
-- Table structure for `nivelcomprometimento`
-- ----------------------------
DROP TABLE IF EXISTS `nivelcomprometimento`;
CREATE TABLE `nivelcomprometimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nivelcomprometimento
-- ----------------------------
INSERT INTO `nivelcomprometimento` VALUES ('1', 'ausente/normal');
INSERT INTO `nivelcomprometimento` VALUES ('2', 'discreto');
INSERT INTO `nivelcomprometimento` VALUES ('3', 'moderado');
INSERT INTO `nivelcomprometimento` VALUES ('4', 'intenso');

-- ----------------------------
-- Table structure for `paciente`
-- ----------------------------
DROP TABLE IF EXISTS `paciente`;
CREATE TABLE `paciente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_pai` varchar(255) NOT NULL,
  `nome_mae` varchar(255) NOT NULL,
  `data_nascimento` date NOT NULL,
  `nome_conjuge` varchar(255) NOT NULL,
  `ocupacao` varchar(100) NOT NULL,
  `cpf` varchar(25) NOT NULL,
  `identidade` int(11) NOT NULL,
  `orgao_emissor` varchar(45) NOT NULL,
  `data_emissao` date NOT NULL,
  `uf` varchar(45) NOT NULL,
  `cidade` varchar(45) NOT NULL,
  `cidade_nascimento` varchar(100) NOT NULL,
  `cep` int(11) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `endereco` varchar(150) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` varchar(25) NOT NULL,
  `celular` varchar(25) NOT NULL,
  `telefone_trabalho` varchar(25) NOT NULL,
  `N_carteira_plano_saude` int(11) NOT NULL,
  `data_validade_carteira` date NOT NULL,
  `cns` int(11) NOT NULL,
  `cod_paciente` varchar(45) DEFAULT NULL,
  `N_prontuario_liga` int(11) NOT NULL,
  `estadoCivil_id` int(11) NOT NULL,
  `sexo_id` int(11) NOT NULL,
  `nacionalidade_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cod_paciente_UNIQUE` (`cod_paciente`),
  KEY `fk_paciente_estado_civil1_idx` (`estadoCivil_id`),
  KEY `fk_paciente_sexo1_idx` (`sexo_id`),
  KEY `fk_paciente_nacionalidade1_idx` (`nacionalidade_id`),
  CONSTRAINT `fk_paciente_estado_civil1` FOREIGN KEY (`estadoCivil_id`) REFERENCES `estadocivil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_paciente_nacionalidade1` FOREIGN KEY (`nacionalidade_id`) REFERENCES `nacionalidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_paciente_sexo1` FOREIGN KEY (`sexo_id`) REFERENCES `sexo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paciente
-- ----------------------------
INSERT INTO `paciente` VALUES ('2', 'italo', '', '', '0000-00-00', '', '', '', '0', '', '0000-00-00', '', '', '', '0', '', '', '0', null, '', '', '', '', '0', '0000-00-00', '0', null, '0', '1', '1', '1');

-- ----------------------------
-- Table structure for `permissao`
-- ----------------------------
DROP TABLE IF EXISTS `permissao`;
CREATE TABLE `permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  `criar` tinyint(4) DEFAULT '0',
  `ler` tinyint(4) DEFAULT '0',
  `atualizar` tinyint(4) DEFAULT '0',
  `deletar` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_permissao_model1_idx` (`model_id`),
  KEY `fk_permissao_grupo1_idx` (`grupo_id`),
  CONSTRAINT `fk_permissao_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_permissao_model1` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `pitchvocal`
-- ----------------------------
DROP TABLE IF EXISTS `pitchvocal`;
CREATE TABLE `pitchvocal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pitchvocal
-- ----------------------------
INSERT INTO `pitchvocal` VALUES ('1', 'adequado');
INSERT INTO `pitchvocal` VALUES ('2', 'grave');
INSERT INTO `pitchvocal` VALUES ('3', 'agudo');

-- ----------------------------
-- Table structure for `procedimentocirurgia`
-- ----------------------------
DROP TABLE IF EXISTS `procedimentocirurgia`;
CREATE TABLE `procedimentocirurgia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_procedimento_cirurgia_remax_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_remax_procedimento_cirurgia_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procedimentocirurgia
-- ----------------------------

-- ----------------------------
-- Table structure for `procedimentodentistica`
-- ----------------------------
DROP TABLE IF EXISTS `procedimentodentistica`;
CREATE TABLE `procedimentodentistica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_procedimento_dentistica_remax_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_remax_procedimento_dentistica_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procedimentodentistica
-- ----------------------------

-- ----------------------------
-- Table structure for `procedimentoendodontia`
-- ----------------------------
DROP TABLE IF EXISTS `procedimentoendodontia`;
CREATE TABLE `procedimentoendodontia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_procedimento_endodontia_remax_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_remax_procedimento_endodontia_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procedimentoendodontia
-- ----------------------------

-- ----------------------------
-- Table structure for `procedimentofonoaudiologia`
-- ----------------------------
DROP TABLE IF EXISTS `procedimentofonoaudiologia`;
CREATE TABLE `procedimentofonoaudiologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_proced` date NOT NULL,
  `tmf` text NOT NULL,
  `obs` text NOT NULL,
  `result_exames` text NOT NULL,
  `outros` text NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `velocidadeFala_id` int(11) NOT NULL,
  `loudess_id` int(11) NOT NULL,
  `pitchVocal_id` int(11) NOT NULL,
  `hiperHiponasalidade_id` int(11) NOT NULL,
  `coordenacaoFonoarticular_id` int(11) NOT NULL,
  `TipoArticular_id` int(11) NOT NULL,
  `NivelComprometimento_id` int(11) NOT NULL,
  `classificacaoFala_id` int(11) NOT NULL,
  `ressonanciaVocal_id` int(11) NOT NULL,
  `classificacaoFonoarticular_id` int(11) NOT NULL,
  `proceMedicina_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_procedimento_fonoaudiologia_remax_paciente1_idx` (`paciente_id`),
  KEY `fk_procedimentoFonoaudiologia_velocidadeFala1_idx` (`velocidadeFala_id`),
  KEY `fk_procedimentoFonoaudiologia_loudess1_idx` (`loudess_id`),
  KEY `fk_procedimentoFonoaudiologia_pitchVocal1_idx` (`pitchVocal_id`),
  KEY `fk_procedimentoFonoaudiologia_hiperHiponasalidade1_idx` (`hiperHiponasalidade_id`),
  KEY `fk_procedimentoFonoaudiologia_coordenacaoFonoarticular1_idx` (`coordenacaoFonoarticular_id`),
  KEY `fk_procedimentoFonoaudiologia_TipoArticular1_idx` (`TipoArticular_id`),
  KEY `fk_procedimentoFonoaudiologia_NivelComprometimento1_idx` (`NivelComprometimento_id`),
  KEY `fk_procedimentoFonoaudiologia_classificacaoFala1_idx` (`classificacaoFala_id`),
  KEY `fk_procedimentoFonoaudiologia_ressonanciaVocal1_idx` (`ressonanciaVocal_id`),
  KEY `fk_procedimentoFonoaudiologia_proceMedicina1_idx` (`proceMedicina_id`),
  KEY `fk_procedimentoFonoaudiologia_classificacaoFonoarticular1_idx` (`classificacaoFonoarticular_id`) USING BTREE,
  CONSTRAINT `fk_procedimentoFonoaudiologia_classificacaoFala1` FOREIGN KEY (`classificacaoFala_id`) REFERENCES `classificacaofala` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_classificacaoFonoarticular1` FOREIGN KEY (`classificacaoFonoarticular_id`) REFERENCES `classificacaofonoarticular` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_coordenacaoFonoarticular1` FOREIGN KEY (`coordenacaoFonoarticular_id`) REFERENCES `coordenacaofonoarticular` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_hiperHiponasalidade1` FOREIGN KEY (`hiperHiponasalidade_id`) REFERENCES `hiperhiponasalidade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_loudess1` FOREIGN KEY (`loudess_id`) REFERENCES `loudess` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_NivelComprometimento1` FOREIGN KEY (`NivelComprometimento_id`) REFERENCES `nivelcomprometimento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_pitchVocal1` FOREIGN KEY (`pitchVocal_id`) REFERENCES `pitchvocal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_proceMedicina1` FOREIGN KEY (`proceMedicina_id`) REFERENCES `procemedicina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_ressonanciaVocal1` FOREIGN KEY (`ressonanciaVocal_id`) REFERENCES `ressonanciavocal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_TipoArticular1` FOREIGN KEY (`TipoArticular_id`) REFERENCES `tipoarticular` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_procedimentoFonoaudiologia_velocidadeFala1` FOREIGN KEY (`velocidadeFala_id`) REFERENCES `velocidadefala` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remax_procedimento_fonoaudiologia_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procedimentofonoaudiologia
-- ----------------------------

-- ----------------------------
-- Table structure for `procedimentoperiodontia`
-- ----------------------------
DROP TABLE IF EXISTS `procedimentoperiodontia`;
CREATE TABLE `procedimentoperiodontia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_procedimento_periodontia_remax_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_remax_procedimento_periodontia_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procedimentoperiodontia
-- ----------------------------

-- ----------------------------
-- Table structure for `procedimentoprotese`
-- ----------------------------
DROP TABLE IF EXISTS `procedimentoprotese`;
CREATE TABLE `procedimentoprotese` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protese_id` int(11) NOT NULL,
  `material_enviado_laboratorio` enum('Sim','Nao') NOT NULL,
  `nome_laboratorio` varchar(45) NOT NULL,
  `descricao_material_enviado` text NOT NULL,
  `tipo_imagem` varchar(45) NOT NULL,
  `escala_cor` varchar(45) NOT NULL,
  `cor` varchar(45) NOT NULL,
  `comentario` text NOT NULL,
  `solicit_trabalho` text NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `img_medicina_id` int(11) NOT NULL,
  `data_procedimento` date NOT NULL,
  `fabricacao_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_procedimento_protese_remax_tipo_protese1_idx` (`protese_id`),
  KEY `fk_remax_procedimento_protese_remax_paciente1_idx` (`paciente_id`),
  KEY `fk_remax_procedimento_protese_remax_img_medicina1_idx` (`img_medicina_id`),
  KEY `fk_procedimentoProtese_fabricacao1_idx` (`fabricacao_id`),
  CONSTRAINT `fk_procedimentoProtese_fabricacao1` FOREIGN KEY (`fabricacao_id`) REFERENCES `fabricacao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remax_procedimento_protese_remax_img_medicina1` FOREIGN KEY (`img_medicina_id`) REFERENCES `imgmedicina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remax_procedimento_protese_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remax_procedimento_protese_remax_tipo_protese1` FOREIGN KEY (`protese_id`) REFERENCES `protese` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procedimentoprotese
-- ----------------------------

-- ----------------------------
-- Table structure for `procedmedicina`
-- ----------------------------
DROP TABLE IF EXISTS `procedmedicina`;
CREATE TABLE `procedmedicina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patologia_paciente` varchar(255) NOT NULL,
  `data_inicio` date DEFAULT NULL,
  `data_termino` date DEFAULT NULL,
  `dose_tratamento_complementar` varchar(60) NOT NULL,
  `data` date NOT NULL,
  `comentarios_adicionais` text NOT NULL,
  `imgMedicina_id` int(11) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `cidCancer_id` int(11) NOT NULL,
  `resposta_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_proced_medicina_remax_img_medicina1_idx` (`imgMedicina_id`),
  KEY `fk_remax_proced_medicina_remax_paciente1_idx` (`paciente_id`),
  KEY `fk_proced_medicina_cidCancer1_idx` (`cidCancer_id`),
  KEY `fk_proced_medicina_resposta1_idx` (`resposta_id`),
  CONSTRAINT `fk_proced_medicina_cidCancer1` FOREIGN KEY (`cidCancer_id`) REFERENCES `cidcancer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proced_medicina_resposta1` FOREIGN KEY (`resposta_id`) REFERENCES `resposta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remax_proced_medicina_remax_img_medicina1` FOREIGN KEY (`imgMedicina_id`) REFERENCES `imgmedicina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remax_proced_medicina_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procedmedicina
-- ----------------------------

-- ----------------------------
-- Table structure for `proced_has_proce_medicina`
-- ----------------------------
DROP TABLE IF EXISTS `proced_has_proce_medicina`;
CREATE TABLE `proced_has_proce_medicina` (
  `proceMedicina_id` int(11) NOT NULL,
  `procedMedicina_id` int(11) NOT NULL,
  KEY `fk_proced_has_proce_medicina_proce_medicina1_idx` (`proceMedicina_id`),
  KEY `fk_proced_has_proce_medicina_proced_medicina1_idx` (`procedMedicina_id`),
  CONSTRAINT `fk_proced_has_proce_medicina_proced_medicina1` FOREIGN KEY (`procedMedicina_id`) REFERENCES `procedmedicina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proced_has_proce_medicina_proce_medicina1` FOREIGN KEY (`proceMedicina_id`) REFERENCES `procemedicina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of proced_has_proce_medicina
-- ----------------------------

-- ----------------------------
-- Table structure for `procemedicina`
-- ----------------------------
DROP TABLE IF EXISTS `procemedicina`;
CREATE TABLE `procemedicina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_proced` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of procemedicina
-- ----------------------------
INSERT INTO `procemedicina` VALUES ('1', 'Maxilarectomia Total');
INSERT INTO `procemedicina` VALUES ('2', 'Maxilarectomia Parcial');
INSERT INTO `procemedicina` VALUES ('3', 'Palato Duro Total');
INSERT INTO `procemedicina` VALUES ('4', 'Palato Duro Parcial');
INSERT INTO `procemedicina` VALUES ('5', 'Seio Maxilar Direito');
INSERT INTO `procemedicina` VALUES ('6', 'Seio Maxilar Esquerdo');
INSERT INTO `procemedicina` VALUES ('7', 'Exanteracao de Orbita Direita');
INSERT INTO `procemedicina` VALUES ('8', 'Exanteracao de Orbita Esquerda');
INSERT INTO `procemedicina` VALUES ('9', 'Resseccao de Orofaringe(Palato Mole)');
INSERT INTO `procemedicina` VALUES ('10', 'Amputacao da Orelha Total');
INSERT INTO `procemedicina` VALUES ('11', 'Amputacao da Orelha Parcial');
INSERT INTO `procemedicina` VALUES ('12', 'Amputacao do Nariz');
INSERT INTO `procemedicina` VALUES ('13', 'Outros');

-- ----------------------------
-- Table structure for `protese`
-- ----------------------------
DROP TABLE IF EXISTS `protese`;
CREATE TABLE `protese` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of protese
-- ----------------------------
INSERT INTO `protese` VALUES ('1', 'Auricular Direito');
INSERT INTO `protese` VALUES ('2', 'Auricular Esquerdo');
INSERT INTO `protese` VALUES ('3', 'Buco-Maxilar');
INSERT INTO `protese` VALUES ('4', 'Conserto de Protese');
INSERT INTO `protese` VALUES ('5', 'Coroa provisoria');
INSERT INTO `protese` VALUES ('6', 'Coroa Total Metalica');
INSERT INTO `protese` VALUES ('7', 'Elevadora');
INSERT INTO `protese` VALUES ('8', 'Encaixe Femea ou Macho Distal');
INSERT INTO `protese` VALUES ('9', 'Encaixe Femea ou Macho Mesial');
INSERT INTO `protese` VALUES ('10', 'Facetas laminadas de porcelana');
INSERT INTO `protese` VALUES ('11', 'Nasal');
INSERT INTO `protese` VALUES ('12', 'P. Fixa Adesiva Direita');
INSERT INTO `protese` VALUES ('13', 'P. Fixa Adesiva Indireta Metalo-Ceramica');
INSERT INTO `protese` VALUES ('14', 'P. Fixa Adesiva Indireta Metalo-Plastica');
INSERT INTO `protese` VALUES ('15', 'P. Fixa Metalo-Ceramica');
INSERT INTO `protese` VALUES ('16', 'Obturadora');
INSERT INTO `protese` VALUES ('17', 'Ocular Direita');
INSERT INTO `protese` VALUES ('18', 'Ocular Esquerda');
INSERT INTO `protese` VALUES ('19', 'Oculopalpebral');

-- ----------------------------
-- Table structure for `relatoriofonoaudiologia`
-- ----------------------------
DROP TABLE IF EXISTS `relatoriofonoaudiologia`;
CREATE TABLE `relatoriofonoaudiologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_proced` date NOT NULL,
  `queixa` text NOT NULL,
  `tipo_aliment` text NOT NULL,
  `freq_aliment` text NOT NULL,
  `escape_aliment` text NOT NULL,
  `condicao_resp` text NOT NULL,
  `secrecao_oral` text NOT NULL,
  `fumo` text NOT NULL,
  `dificul_abrir_boca` text NOT NULL,
  `alteracao_paladar` text NOT NULL,
  `falta_apetite` text NOT NULL,
  `tipo_saliva` text NOT NULL,
  `presenca_mucosite` text NOT NULL,
  `historico_doenca` varchar(45) NOT NULL,
  `medicao_atual` varchar(45) NOT NULL,
  `tipo_voz` varchar(45) NOT NULL,
  `paralisia_facial` varchar(45) NOT NULL,
  `alteracao_moviment` varchar(45) NOT NULL,
  `perda_auditiva` varchar(45) NOT NULL,
  `outros` varchar(45) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_relatorio_fonoaudiologia_remax_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_remax_relatorio_fonoaudiologia_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of relatoriofonoaudiologia
-- ----------------------------
INSERT INTO `relatoriofonoaudiologia` VALUES ('1', '0000-00-00', 'casd\r\n\r\n', 'asda', 'jhkj', 'kjhk', 'jk', 'kjh', 'jkj', 'hkj', 'kj', 'kjk', 'jhk', 'jkj', 'hkjhk', 'kj', 'klj', 'lkj', 'lkj', 'lkj', 'lkj', '2');

-- ----------------------------
-- Table structure for `relatorioodontologia`
-- ----------------------------
DROP TABLE IF EXISTS `relatorioodontologia`;
CREATE TABLE `relatorioodontologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tratamento_medico` text NOT NULL,
  `usa_protese` text NOT NULL,
  `doencas_familiares` text NOT NULL,
  `sofreu_intervencao_cirurgica_bucal_facial` text NOT NULL,
  `problemas_odontologicos` text NOT NULL,
  `proble_mastigacao` text NOT NULL,
  `aparelho_ortodontico` text NOT NULL,
  `dificul_fonacao` text NOT NULL,
  `osteoporose` text NOT NULL,
  `qual_motivo_procurou` text NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `data_procedimento` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_relatorio_odontologia_remax_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_remax_relatorio_odontologia_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of relatorioodontologia
-- ----------------------------

-- ----------------------------
-- Table structure for `relatoriopsicologia`
-- ----------------------------
DROP TABLE IF EXISTS `relatoriopsicologia`;
CREATE TABLE `relatoriopsicologia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  `data_internacao_paciente` date NOT NULL,
  `Setor_paciente_internado` text NOT NULL,
  `qtde_internacoes` int(11) NOT NULL,
  `info_historico_psicossocial` text NOT NULL,
  `estado_emocional` text NOT NULL,
  `rel_doenca_hosp` text NOT NULL,
  `sequelas_emocionais` text NOT NULL,
  `exame_psiquico` text NOT NULL,
  `aval_preoperatorio` text NOT NULL,
  `aval_posimediato` text NOT NULL,
  `aval_posoperatorio` text NOT NULL,
  `aval_transoperatorio` text NOT NULL,
  `aval_emocial_posoperatorio` text NOT NULL,
  `post_posimediato` text NOT NULL,
  `resposta_id` int(11) NOT NULL,
  `data_procedimento` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_relatorio_psicologia_remax_paciente1_idx` (`paciente_id`),
  KEY `fk_relatorio_psicologia_resposta1_idx` (`resposta_id`),
  CONSTRAINT `fk_relatorio_psicologia_resposta1` FOREIGN KEY (`resposta_id`) REFERENCES `resposta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_remax_relatorio_psicologia_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of relatoriopsicologia
-- ----------------------------

-- ----------------------------
-- Table structure for `relatorio_medicina`
-- ----------------------------
DROP TABLE IF EXISTS `relatorio_medicina`;
CREATE TABLE `relatorio_medicina` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paciente_id` int(11) NOT NULL,
  `alergico_med` text NOT NULL,
  `medic_atual` text NOT NULL,
  `trat_med` text NOT NULL,
  `proble_resp` text NOT NULL,
  `temp_calmo` text NOT NULL,
  `medic_dormir` text NOT NULL,
  `proble_cardi` text NOT NULL,
  `data_procedimento` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_remax_relatorio_medicina_remax_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_remax_relatorio_medicina_remax_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of relatorio_medicina
-- ----------------------------

-- ----------------------------
-- Table structure for `resposta`
-- ----------------------------
DROP TABLE IF EXISTS `resposta`;
CREATE TABLE `resposta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resposta
-- ----------------------------
INSERT INTO `resposta` VALUES ('1', 'Sim');
INSERT INTO `resposta` VALUES ('2', 'Nao');

-- ----------------------------
-- Table structure for `ressonanciavocal`
-- ----------------------------
DROP TABLE IF EXISTS `ressonanciavocal`;
CREATE TABLE `ressonanciavocal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ressonanciavocal
-- ----------------------------
INSERT INTO `ressonanciavocal` VALUES ('1', 'equilibrada');
INSERT INTO `ressonanciavocal` VALUES ('2', 'laringofaringea');
INSERT INTO `ressonanciavocal` VALUES ('3', 'hiponasal');
INSERT INTO `ressonanciavocal` VALUES ('4', 'hipernasal');

-- ----------------------------
-- Table structure for `sexo`
-- ----------------------------
DROP TABLE IF EXISTS `sexo`;
CREATE TABLE `sexo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sexo
-- ----------------------------
INSERT INTO `sexo` VALUES ('1', 'Masculino');
INSERT INTO `sexo` VALUES ('2', 'Feminino');

-- ----------------------------
-- Table structure for `submenus`
-- ----------------------------
DROP TABLE IF EXISTS `submenus`;
CREATE TABLE `submenus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `menus_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_menu_id` (`menus_id`),
  CONSTRAINT `fk_menu_id` FOREIGN KEY (`menus_id`) REFERENCES `menus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of submenus
-- ----------------------------

-- ----------------------------
-- Table structure for `tipoarticular`
-- ----------------------------
DROP TABLE IF EXISTS `tipoarticular`;
CREATE TABLE `tipoarticular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipoarticular
-- ----------------------------
INSERT INTO `tipoarticular` VALUES ('1', 'Preciso');
INSERT INTO `tipoarticular` VALUES ('2', 'impreciso');
INSERT INTO `tipoarticular` VALUES ('3', 'travado');
INSERT INTO `tipoarticular` VALUES ('4', 'exagerado');

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `data_nascimento` date NOT NULL,
  `cpf` varchar(25) NOT NULL,
  `identidade` varchar(25) NOT NULL,
  `uf` varchar(45) NOT NULL,
  `cidade` varchar(45) NOT NULL,
  `cep` int(11) NOT NULL,
  `endereco` varchar(45) NOT NULL,
  `complemento` varchar(45) NOT NULL,
  `numero` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `telefone` varchar(45) NOT NULL,
  `instituicao_id` int(11) NOT NULL,
  `vinculoProjeto_id` int(11) NOT NULL,
  `areaUser_id` int(11) NOT NULL,
  `sexo_id` int(11) NOT NULL,
  `estadoCivil_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_instituicao1_idx` (`instituicao_id`),
  KEY `fk_usuario_vinculo_projeto1_idx` (`vinculoProjeto_id`),
  KEY `fk_usuario_area_user1_idx` (`areaUser_id`),
  KEY `fk_usuario_sexo1_idx` (`sexo_id`),
  KEY `fk_usuario_estadoCivil1_idx` (`estadoCivil_id`),
  KEY `fk_usuario_grupo1_idx` (`grupo_id`),
  CONSTRAINT `fk_usuario_area_user1` FOREIGN KEY (`areaUser_id`) REFERENCES `areauser` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_estadoCivil1` FOREIGN KEY (`estadoCivil_id`) REFERENCES `estadocivil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_instituicao1` FOREIGN KEY (`instituicao_id`) REFERENCES `instituicao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_sexo1` FOREIGN KEY (`sexo_id`) REFERENCES `sexo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_vinculo_projeto1` FOREIGN KEY (`vinculoProjeto_id`) REFERENCES `vinculoprojeto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario
-- ----------------------------

-- ----------------------------
-- Table structure for `velocidadefala`
-- ----------------------------
DROP TABLE IF EXISTS `velocidadefala`;
CREATE TABLE `velocidadefala` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of velocidadefala
-- ----------------------------
INSERT INTO `velocidadefala` VALUES ('1', 'lenta');
INSERT INTO `velocidadefala` VALUES ('2', 'normal');
INSERT INTO `velocidadefala` VALUES ('3', 'aumentada');

-- ----------------------------
-- Table structure for `vinculoprojeto`
-- ----------------------------
DROP TABLE IF EXISTS `vinculoprojeto`;
CREATE TABLE `vinculoprojeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(225) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vinculoprojeto
-- ----------------------------
INSERT INTO `vinculoprojeto` VALUES ('1', 'Aluno/Bolsista');
INSERT INTO `vinculoprojeto` VALUES ('2', 'Coladorador externo');
INSERT INTO `vinculoprojeto` VALUES ('3', 'Docente');
