/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : isrbasedinamico

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-10-05 01:45:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `grupos`
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grupos
-- ----------------------------
INSERT INTO `grupos` VALUES ('1', 'Administrador');
INSERT INTO `grupos` VALUES ('2', 'Padrao');

-- ----------------------------
-- Table structure for `login`
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  `grupos_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`),
  KEY `fk_login_grupos1_idx` (`grupos_id`),
  CONSTRAINT `fk_login_grupos1` FOREIGN KEY (`grupos_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES ('1', 'italo', '321456', '1');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  `valor` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'Menu', 'menus');
INSERT INTO `menus` VALUES ('2', 'Acesso/Permissao', 'menusacesso');
INSERT INTO `menus` VALUES ('3', 'Usuarios acesso', 'login');
INSERT INTO `menus` VALUES ('4', 'Grupo', 'grupos');
INSERT INTO `menus` VALUES ('5', 'Ações', 'menusacoes');

-- ----------------------------
-- Table structure for `menus_acesso`
-- ----------------------------
DROP TABLE IF EXISTS `menus_acesso`;
CREATE TABLE `menus_acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menus_id` int(11) NOT NULL,
  `grupos_id` int(11) NOT NULL,
  `menus_acoes_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_menus_acesso_menus_idx` (`menus_id`),
  KEY `fk_menus_acesso_grupos1_idx` (`grupos_id`),
  KEY `fk_menus_acesso_menu_acoes1_idx` (`menus_acoes_id`),
  CONSTRAINT `fk_menus_acesso_grupos1` FOREIGN KEY (`grupos_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_menus_acesso_menus` FOREIGN KEY (`menus_id`) REFERENCES `menus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_menus_acesso_menu_acoes1` FOREIGN KEY (`menus_acoes_id`) REFERENCES `menus_acoes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus_acesso
-- ----------------------------
INSERT INTO `menus_acesso` VALUES ('1', '1', '1', '1');
INSERT INTO `menus_acesso` VALUES ('3', '1', '1', '3');
INSERT INTO `menus_acesso` VALUES ('4', '1', '1', '4');
INSERT INTO `menus_acesso` VALUES ('6', '1', '1', '5');
INSERT INTO `menus_acesso` VALUES ('7', '1', '1', '6');
INSERT INTO `menus_acesso` VALUES ('8', '1', '1', '7');
INSERT INTO `menus_acesso` VALUES ('10', '4', '1', '1');
INSERT INTO `menus_acesso` VALUES ('11', '4', '1', '2');
INSERT INTO `menus_acesso` VALUES ('13', '4', '1', '7');
INSERT INTO `menus_acesso` VALUES ('14', '2', '1', '1');
INSERT INTO `menus_acesso` VALUES ('15', '2', '1', '2');
INSERT INTO `menus_acesso` VALUES ('16', '2', '1', '3');
INSERT INTO `menus_acesso` VALUES ('17', '2', '1', '4');
INSERT INTO `menus_acesso` VALUES ('18', '2', '1', '7');
INSERT INTO `menus_acesso` VALUES ('19', '2', '1', '3');
INSERT INTO `menus_acesso` VALUES ('20', '2', '1', '6');
INSERT INTO `menus_acesso` VALUES ('22', '1', '1', '2');

-- ----------------------------
-- Table structure for `menus_acoes`
-- ----------------------------
DROP TABLE IF EXISTS `menus_acoes`;
CREATE TABLE `menus_acoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `abamenu` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus_acoes
-- ----------------------------
INSERT INTO `menus_acoes` VALUES ('1', 'Cadastrar', 'create', '1');
INSERT INTO `menus_acoes` VALUES ('2', 'Listar', 'admin', '1');
INSERT INTO `menus_acoes` VALUES ('3', 'Visualizar', 'view', '0');
INSERT INTO `menus_acoes` VALUES ('4', 'Excluir/Deletar', 'delete', '0');
INSERT INTO `menus_acoes` VALUES ('5', 'Atualizar', 'update', '0');
INSERT INTO `menus_acoes` VALUES ('6', 'Inicio', 'index', '1');
INSERT INTO `menus_acoes` VALUES ('7', 'Aba Menu', '#', '1');
