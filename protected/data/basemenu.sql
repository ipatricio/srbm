/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : base

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-09-30 00:05:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `grupos`
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grupos
-- ----------------------------
INSERT INTO `grupos` VALUES ('3', 'Administrador');
INSERT INTO `grupos` VALUES ('4', 'Funcionários');

-- ----------------------------
-- Table structure for `login`
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_login_grupos1_idx` (`grupo_id`),
  CONSTRAINT `fk_login_grupos1` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES ('5', 'italo', 'a9a708eebbfd48267afb1f146caf5229', '3');
INSERT INTO `login` VALUES ('6', 'Admin', 'a9a708eebbfd48267afb1f146caf5229', '3');
INSERT INTO `login` VALUES ('7', 'usuario', 'f8032d5cae3de20fcec887f395ec9a6a', '4');
INSERT INTO `login` VALUES ('8', 'test', '202cb962ac59075b964b07152d234b70', '4');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create` tinyint(4) NOT NULL DEFAULT '0',
  `update` tinyint(4) NOT NULL DEFAULT '0',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  `view` tinyint(4) NOT NULL DEFAULT '1',
  `index` tinyint(4) NOT NULL DEFAULT '1',
  `menuvg_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idmenuvg_idx` (`menuvg_id`),
  KEY `idgrupo_idx` (`grupo_id`),
  CONSTRAINT `grupo_id` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `menuvg_id` FOREIGN KEY (`menuvg_id`) REFERENCES `menuvg` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('2', '0', '0', '0', '0', '1', '1', '2', '3');
INSERT INTO `menus` VALUES ('3', '1', '1', '1', '1', '1', '1', '3', '3');
INSERT INTO `menus` VALUES ('4', '1', '1', '1', '1', '1', '1', '4', '3');
INSERT INTO `menus` VALUES ('6', '0', '0', '0', '0', '1', '1', '5', '4');
INSERT INTO `menus` VALUES ('7', '1', '1', '1', '1', '1', '1', '7', '3');
INSERT INTO `menus` VALUES ('8', '0', '0', '0', '0', '1', '1', '6', '4');
INSERT INTO `menus` VALUES ('9', '0', '0', '0', '0', '0', '0', '3', '3');

-- ----------------------------
-- Table structure for `menuvg`
-- ----------------------------
DROP TABLE IF EXISTS `menuvg`;
CREATE TABLE `menuvg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  `name_controller` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menuvg
-- ----------------------------
INSERT INTO `menuvg` VALUES ('2', 'Menu', 'menuvg');
INSERT INTO `menuvg` VALUES ('3', 'Acesso', 'menus');
INSERT INTO `menuvg` VALUES ('4', 'Grupo', 'grupos');
INSERT INTO `menuvg` VALUES ('5', 'Home', 'site');
INSERT INTO `menuvg` VALUES ('6', 'Contato', 'site');
INSERT INTO `menuvg` VALUES ('7', 'Usuario-Acesso', 'login');
