<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'update',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'delete',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'admin',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'view',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'index',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'menuvg_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'grupo_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
