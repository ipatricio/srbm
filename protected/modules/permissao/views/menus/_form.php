<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'menus-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'create',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'update',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'delete',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'admin',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'view',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'index',array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model,'menuvg_id', Menuvg::model()->listData(),array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model,'grupo_id', Grupos::model()->listData(),array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
