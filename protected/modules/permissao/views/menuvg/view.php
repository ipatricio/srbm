<?php
$this->breadcrumbs=array(
	'Menuvgs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Menuvg','url'=>array('index')),
	array('label'=>'Create Menuvg','url'=>array('create')),
	array('label'=>'Update Menuvg','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Menuvg','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Menuvg','url'=>array('admin')),
);
?>

<h1>View Menuvg #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descricao',
		'name_controller',
	),
)); ?>
