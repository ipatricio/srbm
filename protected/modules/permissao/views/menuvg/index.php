<?php
$this->breadcrumbs=array(
	'Menuvgs',
);

$this->menu=array(
	array('label'=>'Create Menuvg','url'=>array('create')),
	array('label'=>'Manage Menuvg','url'=>array('admin')),
);
?>

<h1>Menuvgs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
