<?php
$this->breadcrumbs=array(
	'Menuvgs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Menuvg','url'=>array('index')),
	array('label'=>'Manage Menuvg','url'=>array('admin')),
);
?>

<h1>Create Menuvg</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>