<?php
$this->breadcrumbs=array(
	'Menuvgs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Menuvg','url'=>array('index')),
	array('label'=>'Create Menuvg','url'=>array('create')),
	array('label'=>'View Menuvg','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Menuvg','url'=>array('admin')),
);
?>

<h1>Update Menuvg <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>