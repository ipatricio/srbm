<?php

class RelatoriofonoaudiologiaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		
		if(Yii::app()->user->isGuest){
			$access = array(
				array('deny',  // deny all users
					'users'=>array('*'),
				),
			 );
		}
	   else{
	   	  $access = Login::model()->buildAccessRules(__CLASS__);	
	   }


	 return $access;
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)//Id do paciente
	{
            $criteria=new CDbCriteria;
            
            $criteria->condition = "paciente_id = {$id}";
            
	        $id = Relatoriofonoaudiologia::model()->find($criteria);
              
              $this->render('view',array(
			'model'=>$this->loadModel($id->id),
                  
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new Relatoriofonoaudiologia;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Relatoriofonoaudiologia']))
		{	
			if(isset($_GET['id']))
			$_POST['Relatoriofonoaudiologia']['paciente_id'] = $_GET['id'];

			$model->attributes=$_POST['Relatoriofonoaudiologia'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
			
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Relatoriofonoaudiologia']))
		{
			$model->attributes=$_POST['Relatoriofonoaudiologia'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Relatoriofonoaudiologia');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Relatoriofonoaudiologia('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Relatoriofonoaudiologia']))
			$model->attributes=$_GET['Relatoriofonoaudiologia'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Relatoriofonoaudiologia::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='relatoriofonoaudiologia-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
