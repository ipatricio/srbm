<?php

/**
 * This is the model class for table "paciente".
 *
 * The followings are the available columns in table 'paciente':
 * @property integer $id
 * @property string $nome
 * @property string $nome_pai
 * @property string $nome_mae
 * @property string $data_nascimento
 * @property string $nome_conjuge
 * @property string $ocupacao
 * @property string $cpf
 * @property integer $identidade
 * @property string $orgao_emissor
 * @property string $data_emissao
 * @property string $uf
 * @property string $cidade
 * @property string $cidade_nascimento
 * @property integer $cep
 * @property string $bairro
 * @property string $endereco
 * @property integer $numero
 * @property string $complemento
 * @property string $email
 * @property string $telefone
 * @property string $celular
 * @property string $telefone_trabalho
 * @property integer $N_carteira_plano_saude
 * @property string $data_validade_carteira
 * @property integer $cns
 * @property string $cod_paciente
 * @property integer $N_prontuario_liga
 * @property integer $estadoCivil_id
 * @property integer $sexo_id
 * @property integer $nacionalidade_id
 *
 * The followings are the available model relations:
 * @property Anexos[] $anexoses
 * @property Estadocivil $estadoCivil
 * @property Nacionalidade $nacionalidade
 * @property Sexo $sexo
 * @property Procedimentocirurgia[] $procedimentocirurgias
 * @property Procedimentodentistica[] $procedimentodentisticas
 * @property Procedimentoendodontia[] $procedimentoendodontias
 * @property Procedimentofonoaudiologia[] $procedimentofonoaudiologias
 * @property Procedimentoperiodontia[] $procedimentoperiodontias
 * @property Procedimentoprotese[] $procedimentoproteses
 * @property Procedmedicina[] $procedmedicinas
 * @property RelatorioMedicina[] $relatorioMedicinas
 * @property Relatoriofonoaudiologia[] $relatoriofonoaudiologias
 * @property Relatorioodontologia[] $relatorioodontologias
 * @property Relatoriopsicologia[] $relatoriopsicologias
 */
class Paciente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paciente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, nome_pai, nome_mae, data_nascimento, ocupacao, cpf, identidade, orgao_emissor, data_emissao, uf, cidade, cidade_nascimento, cep, bairro, endereco, numero, email, telefone, celular, telefone_trabalho, cns, N_prontuario_liga, estadoCivil_id, sexo_id, nacionalidade_id', 'required'),
			array('identidade, cep, numero, N_carteira_plano_saude, cns, N_prontuario_liga, estadoCivil_id, sexo_id, nacionalidade_id', 'numerical', 'integerOnly'=>true),
			array('nome, nome_pai, nome_mae, nome_conjuge, email', 'length', 'max'=>255),
			array('ocupacao, cidade_nascimento, bairro, complemento', 'length', 'max'=>100),
			array('cpf, telefone, celular, telefone_trabalho', 'length', 'max'=>25),
			array('orgao_emissor, uf, cidade, cod_paciente', 'length', 'max'=>45),
			array('endereco', 'length', 'max'=>150),
			array('data_validade_carteira', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, nome_pai, nome_mae, data_nascimento, nome_conjuge, ocupacao, cpf, identidade, orgao_emissor, data_emissao, uf, cidade, cidade_nascimento, cep, bairro, endereco, numero, complemento, email, telefone, celular, telefone_trabalho, N_carteira_plano_saude, data_validade_carteira, cns, cod_paciente, N_prontuario_liga, estadoCivil_id, sexo_id, nacionalidade_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'anexoses' => array(self::HAS_MANY, 'Anexos', 'paciente_id'),
			'estadoCivil' => array(self::BELONGS_TO, 'Estadocivil', 'estadoCivil_id'),
			'nacionalidade' => array(self::BELONGS_TO, 'Nacionalidade', 'nacionalidade_id'),
			'sexo' => array(self::BELONGS_TO, 'Sexo', 'sexo_id'),
			'procedimentocirurgias' => array(self::HAS_MANY, 'Procedimentocirurgia', 'paciente_id'),
			'procedimentodentisticas' => array(self::HAS_MANY, 'Procedimentodentistica', 'paciente_id'),
			'procedimentoendodontias' => array(self::HAS_MANY, 'Procedimentoendodontia', 'paciente_id'),
			'procedimentofonoaudiologias' => array(self::HAS_MANY, 'Procedimentofonoaudiologia', 'paciente_id'),
			'procedimentoperiodontias' => array(self::HAS_MANY, 'Procedimentoperiodontia', 'paciente_id'),
			'procedimentoproteses' => array(self::HAS_MANY, 'Procedimentoprotese', 'paciente_id'),
			'procedmedicinas' => array(self::HAS_MANY, 'Procedmedicina', 'paciente_id'),
			'relatorioMedicinas' => array(self::HAS_MANY, 'RelatorioMedicina', 'paciente_id'),
			'relatoriofonoaudiologias' => array(self::HAS_MANY, 'Relatoriofonoaudiologia', 'paciente_id'),
			'relatorioodontologias' => array(self::HAS_MANY, 'Relatorioodontologia', 'paciente_id'),
			'relatoriopsicologias' => array(self::HAS_MANY, 'Relatoriopsicologia', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'nome_pai' => 'Nome Pai',
			'nome_mae' => 'Nome Mãe',
			'data_nascimento' => 'Data de nascimento',
			'nome_conjuge' => 'Nome do cônjuge',
			'ocupacao' => 'Ocupação',
			'cpf' => 'CPF',
			'identidade' => 'Identidade',
			'orgao_emissor' => 'Órgão emissor',
			'data_emissao' => 'Data de emissão',
			'uf' => 'UF',
			'cidade' => 'Cidade',
			'cidade_nascimento' => 'Cidade de nascimento',
			'cep' => 'CEP',
			'bairro' => 'Bairro',
			'endereco' => 'Endereço',
			'numero' => 'Número',
			'complemento' => 'Complemento',
			'email' => 'Email',
			'telefone' => 'Telefone',
			'celular' => 'Celular',
			'telefone_trabalho' => 'Telefone do trabalho',
			'N_carteira_plano_saude' => 'Nº Carteira Plano de Saude',
			'data_validade_carteira' => 'Data Validade Carteira',
			'cns' => 'CNS',
			'cod_paciente' => 'Código do Paciente',
			'N_prontuario_liga' => 'Nº do Prontuario da Liga',
			'estadoCivil_id' => 'Estado Civil',
			'sexo_id' => 'Sexo',
			'nacionalidade_id' => 'Nacionalidade',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('nome_pai',$this->nome_pai,true);
		$criteria->compare('nome_mae',$this->nome_mae,true);
		$criteria->compare('data_nascimento',$this->data_nascimento,true);
		$criteria->compare('nome_conjuge',$this->nome_conjuge,true);
		$criteria->compare('ocupacao',$this->ocupacao,true);
		$criteria->compare('cpf',$this->cpf,true);
		$criteria->compare('identidade',$this->identidade);
		$criteria->compare('orgao_emissor',$this->orgao_emissor,true);
		$criteria->compare('data_emissao',$this->data_emissao,true);
		$criteria->compare('uf',$this->uf,true);
		$criteria->compare('cidade',$this->cidade,true);
		$criteria->compare('cidade_nascimento',$this->cidade_nascimento,true);
		$criteria->compare('cep',$this->cep);
		$criteria->compare('bairro',$this->bairro,true);
		$criteria->compare('endereco',$this->endereco,true);
		$criteria->compare('numero',$this->numero);
		$criteria->compare('complemento',$this->complemento,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telefone',$this->telefone,true);
		$criteria->compare('celular',$this->celular,true);
		$criteria->compare('telefone_trabalho',$this->telefone_trabalho,true);
		$criteria->compare('N_carteira_plano_saude',$this->N_carteira_plano_saude);
		$criteria->compare('data_validade_carteira',$this->data_validade_carteira,true);
		$criteria->compare('cns',$this->cns);
		$criteria->compare('cod_paciente',$this->cod_paciente,true);
		$criteria->compare('N_prontuario_liga',$this->N_prontuario_liga);
		$criteria->compare('estadoCivil_id',$this->estadoCivil_id);
		$criteria->compare('sexo_id',$this->sexo_id);
		$criteria->compare('nacionalidade_id',$this->nacionalidade_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paciente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
