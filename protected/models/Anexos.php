<?php

/**
 * This is the model class for table "anexos".
 *
 * The followings are the available columns in table 'anexos':
 * @property integer $id
 * @property string $comentario
 * @property integer $areas_id
 * @property integer $paciente_id
 *
 * The followings are the available model relations:
 * @property Areas $areas
 * @property Paciente $paciente
 */
class Anexos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'anexos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('areas_id, paciente_id, documento', 'required'),
			array('areas_id, paciente_id', 'numerical', 'integerOnly'=>true),
			array('comentario', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, comentario, areas_id, paciente_id', 'safe', 'on'=>'search'),
                                  
		);

	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'areas' => array(self::BELONGS_TO, 'Areas', 'areas_id'),
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'comentario' => 'Comentario',
			'areas_id' => 'Area',
			'paciente_id' => 'Paciente',
                        'documento'=>'Imagem',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('comentario',$this->comentario,true);
		$criteria->compare('areas_id',$this->areas_id);
		$criteria->compare('paciente_id',$this->paciente_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Anexos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
