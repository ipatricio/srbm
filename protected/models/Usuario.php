<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property integer $id
 * @property string $nome
 * @property string $data_nascimento
 * @property string $cpf
 * @property string $identidade
 * @property string $uf
 * @property string $cidade
 * @property integer $cep
 * @property string $endereco
 * @property string $complemento
 * @property integer $numero
 * @property string $email
 * @property string $bairro
 * @property string $telefone
 * @property integer $instituicao_id
 * @property integer $vinculoProjeto_id
 * @property integer $areaUser_id
 * @property integer $sexo_id
 * @property integer $estadoCivil_id
 * @property integer $grupo_id
 *
 * The followings are the available model relations:
 * @property Areauser $areaUser
 * @property Estadocivil $estadoCivil
 * @property Grupo $grupo
 * @property Instituicao $instituicao
 * @property Sexo $sexo
 * @property Vinculoprojeto $vinculoProjeto
 */
class Usuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, data_nascimento, cpf, identidade, uf, cidade, cep, endereco, complemento, numero, email, bairro, telefone, instituicao_id, vinculoProjeto_id, areaUser_id, sexo_id, estadoCivil_id, grupo_id', 'required'),
			array('cep, numero, instituicao_id, vinculoProjeto_id, areaUser_id, sexo_id, estadoCivil_id, grupo_id', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>255),
			array('cpf, identidade', 'length', 'max'=>25),
			array('uf, cidade, endereco, complemento, email, telefone', 'length', 'max'=>45),
			array('bairro', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, data_nascimento, cpf, identidade, uf, cidade, cep, endereco, complemento, numero, email, bairro, telefone, instituicao_id, vinculoProjeto_id, areaUser_id, sexo_id, estadoCivil_id, grupo_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'areaUser' => array(self::BELONGS_TO, 'Areauser', 'areaUser_id'),
			'estadoCivil' => array(self::BELONGS_TO, 'Estadocivil', 'estadoCivil_id'),
			'grupo' => array(self::BELONGS_TO, 'Grupo', 'grupo_id'),
			'instituicao' => array(self::BELONGS_TO, 'Instituicao', 'instituicao_id'),
			'sexo' => array(self::BELONGS_TO, 'Sexo', 'sexo_id'),
			'vinculoProjeto' => array(self::BELONGS_TO, 'Vinculoprojeto', 'vinculoProjeto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'data_nascimento' => 'Data Nascimento',
			'cpf' => 'Cpf',
			'identidade' => 'Identidade',
			'uf' => 'Uf',
			'cidade' => 'Cidade',
			'cep' => 'Cep',
			'endereco' => 'Endereco',
			'complemento' => 'Complemento',
			'numero' => 'Numero',
			'email' => 'Email',
			'bairro' => 'Bairro',
			'telefone' => 'Telefone',
			'instituicao_id' => 'Instituicao',
			'vinculoProjeto_id' => 'Vinculo Projeto',
			'areaUser_id' => 'Area User',
			'sexo_id' => 'Sexo',
			'estadoCivil_id' => 'Estado Civil',
			'grupo_id' => 'Grupo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('data_nascimento',$this->data_nascimento,true);
		$criteria->compare('cpf',$this->cpf,true);
		$criteria->compare('identidade',$this->identidade,true);
		$criteria->compare('uf',$this->uf,true);
		$criteria->compare('cidade',$this->cidade,true);
		$criteria->compare('cep',$this->cep);
		$criteria->compare('endereco',$this->endereco,true);
		$criteria->compare('complemento',$this->complemento,true);
		$criteria->compare('numero',$this->numero);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('bairro',$this->bairro,true);
		$criteria->compare('telefone',$this->telefone,true);
		$criteria->compare('instituicao_id',$this->instituicao_id);
		$criteria->compare('vinculoProjeto_id',$this->vinculoProjeto_id);
		$criteria->compare('areaUser_id',$this->areaUser_id);
		$criteria->compare('sexo_id',$this->sexo_id);
		$criteria->compare('estadoCivil_id',$this->estadoCivil_id);
		$criteria->compare('grupo_id',$this->grupo_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
