<?php

/**
 * This is the model class for table "relatorio_medicina".
 *
 * The followings are the available columns in table 'relatorio_medicina':
 * @property integer $id
 * @property integer $paciente_id
 * @property string $alergico_med
 * @property string $medic_atual
 * @property string $trat_med
 * @property string $proble_resp
 * @property string $temp_calmo
 * @property string $medic_dormir
 * @property string $proble_cardi
 * @property string $data_procedimento
 *
 * The followings are the available model relations:
 * @property Paciente $paciente
 */
class RelatorioMedicina extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'relatorio_medicina';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paciente_id, alergico_med, medic_atual, trat_med, proble_resp, temp_calmo, medic_dormir, proble_cardi, data_procedimento', 'required'),
			array('paciente_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, paciente_id, alergico_med, medic_atual, trat_med, proble_resp, temp_calmo, medic_dormir, proble_cardi, data_procedimento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'paciente_id' => 'Paciente',
			'alergico_med' => 'É alérgico a algum tipo de medicamento?',
			'medic_atual' => 'Está tomando algum medicamento atualmente?',
			'trat_med' => 'Está fazendo algum tratamento médico?',
			'proble_resp' => 'Apresenta algum problema respiratório?',
			'temp_calmo' => 'Possui temperamento calmo?',
			'medic_dormir' => 'Dorme bem? Quantas horas por dia? faz uso de alguma medicação para dormir?',
			'proble_cardi' => 'Problemas cardíacos? Quais? Que medicação ingere?',
			'data_procedimento' => 'Data do procedimento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('alergico_med',$this->alergico_med,true);
		$criteria->compare('medic_atual',$this->medic_atual,true);
		$criteria->compare('trat_med',$this->trat_med,true);
		$criteria->compare('proble_resp',$this->proble_resp,true);
		$criteria->compare('temp_calmo',$this->temp_calmo,true);
		$criteria->compare('medic_dormir',$this->medic_dormir,true);
		$criteria->compare('proble_cardi',$this->proble_cardi,true);
		$criteria->compare('data_procedimento',$this->data_procedimento,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RelatorioMedicina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
