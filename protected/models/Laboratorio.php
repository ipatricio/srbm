<?php

/**
 * This is the model class for table "laboratorio".
 *
 * The followings are the available columns in table 'laboratorio':
 * @property integer $id
 * @property string $nome
 * @property string $cnpj
 * @property string $cnae
 * @property string $uf
 * @property string $cidade
 * @property string $logradouro
 * @property integer $numero
 * @property integer $cep
 * @property string $email
 * @property string $telefone1
 * @property string $telefone2
 * @property string $nome_resp
 * @property string $datadenascimento
 * @property string $cpf
 * @property string $identidade
 * @property string $uf_resp
 * @property string $cidade_resp
 * @property integer $cep_resp
 * @property string $endereco_resp
 * @property string $complemento
 * @property integer $numero_resp
 * @property string $email_resp
 * @property string $qualificacao
 * @property integer $telefone1_resp
 * @property integer $telefone2_resp
 * @property string $bairro_lab
 * @property string $bairro_resp
 * @property integer $sexo_id
 * @property integer $nacionalidade_id
 * @property integer $estadoCivil_id
 *
 * The followings are the available model relations:
 * @property Estadocivil $estadoCivil
 * @property Nacionalidade $nacionalidade
 * @property Sexo $sexo
 */
class Laboratorio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laboratorio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, cnpj, cnae, uf, cidade, logradouro, numero, cep, email, telefone1, telefone2, nome_resp, datadenascimento, cpf, identidade, uf_resp, cidade_resp, cep_resp, endereco_resp, numero_resp, email_resp, qualificacao, telefone1_resp, telefone2_resp, bairro_lab, bairro_resp, sexo_id, nacionalidade_id, estadoCivil_id', 'required'),
			array('numero, cep, cep_resp, numero_resp, telefone1_resp, telefone2_resp, sexo_id, nacionalidade_id, estadoCivil_id', 'numerical', 'integerOnly'=>true),
			array('nome, uf, cidade, logradouro, email, nome_resp, uf_resp, cidade_resp, endereco_resp, complemento, email_resp, qualificacao', 'length', 'max'=>45),
			array('cnpj, cnae', 'length', 'max'=>30),
			array('telefone1, telefone2, cpf, identidade', 'length', 'max'=>20),
			array('bairro_lab, bairro_resp', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, cnpj, cnae, uf, cidade, logradouro, numero, cep, email, telefone1, telefone2, nome_resp, datadenascimento, cpf, identidade, uf_resp, cidade_resp, cep_resp, endereco_resp, complemento, numero_resp, email_resp, qualificacao, telefone1_resp, telefone2_resp, bairro_lab, bairro_resp, sexo_id, nacionalidade_id, estadoCivil_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estadoCivil' => array(self::BELONGS_TO, 'Estadocivil', 'estadoCivil_id'),
			'nacionalidade' => array(self::BELONGS_TO, 'Nacionalidade', 'nacionalidade_id'),
			'sexo' => array(self::BELONGS_TO, 'Sexo', 'sexo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'cnpj' => 'Cnpj',
			'cnae' => 'Cnae',
			'uf' => 'Uf',
			'cidade' => 'Cidade',
			'logradouro' => 'Logradouro',
			'numero' => 'Numero',
			'cep' => 'Cep',
			'email' => 'Email',
			'telefone1' => 'Telefone1',
			'telefone2' => 'Telefone2',
			'nome_resp' => 'Nome Resp',
			'datadenascimento' => 'Datadenascimento',
			'cpf' => 'Cpf',
			'identidade' => 'Identidade',
			'uf_resp' => 'Uf Resp',
			'cidade_resp' => 'Cidade Resp',
			'cep_resp' => 'Cep Resp',
			'endereco_resp' => 'Endereco Resp',
			'complemento' => 'Complemento',
			'numero_resp' => 'Numero Resp',
			'email_resp' => 'Email Resp',
			'qualificacao' => 'Qualificacao',
			'telefone1_resp' => 'Telefone1 Resp',
			'telefone2_resp' => 'Telefone2 Resp',
			'bairro_lab' => 'Bairro Lab',
			'bairro_resp' => 'Bairro Resp',
			'sexo_id' => 'Sexo',
			'nacionalidade_id' => 'Nacionalidade',
			'estadoCivil_id' => 'Estado Civil',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('cnpj',$this->cnpj,true);
		$criteria->compare('cnae',$this->cnae,true);
		$criteria->compare('uf',$this->uf,true);
		$criteria->compare('cidade',$this->cidade,true);
		$criteria->compare('logradouro',$this->logradouro,true);
		$criteria->compare('numero',$this->numero);
		$criteria->compare('cep',$this->cep);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telefone1',$this->telefone1,true);
		$criteria->compare('telefone2',$this->telefone2,true);
		$criteria->compare('nome_resp',$this->nome_resp,true);
		$criteria->compare('datadenascimento',$this->datadenascimento,true);
		$criteria->compare('cpf',$this->cpf,true);
		$criteria->compare('identidade',$this->identidade,true);
		$criteria->compare('uf_resp',$this->uf_resp,true);
		$criteria->compare('cidade_resp',$this->cidade_resp,true);
		$criteria->compare('cep_resp',$this->cep_resp);
		$criteria->compare('endereco_resp',$this->endereco_resp,true);
		$criteria->compare('complemento',$this->complemento,true);
		$criteria->compare('numero_resp',$this->numero_resp);
		$criteria->compare('email_resp',$this->email_resp,true);
		$criteria->compare('qualificacao',$this->qualificacao,true);
		$criteria->compare('telefone1_resp',$this->telefone1_resp);
		$criteria->compare('telefone2_resp',$this->telefone2_resp);
		$criteria->compare('bairro_lab',$this->bairro_lab,true);
		$criteria->compare('bairro_resp',$this->bairro_resp,true);
		$criteria->compare('sexo_id',$this->sexo_id);
		$criteria->compare('nacionalidade_id',$this->nacionalidade_id);
		$criteria->compare('estadoCivil_id',$this->estadoCivil_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Laboratorio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
