<?php

/**
 * This is the model class for table "menus_acesso".
 *
 * The followings are the available columns in table 'menus_acesso':
 * @property integer $id
 * @property integer $menus_id
 * @property integer $grupos_id
 * @property integer $menus_acoes_id
 *
 * The followings are the available model relations:
 * @property Menus $menus
 * @property Grupos $grupos
 * @property MenusAcoes $menusAcoes
 */
class MenusAcesso extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menus_acesso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('menus_id, grupos_id, menus_acoes_id', 'required'),
			array('menus_id, grupos_id, menus_acoes_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, menus_id, grupos_id, menus_acoes_id, situacao ', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menus' => array(self::BELONGS_TO, 'Menus', 'menus_id'),
			'grupos' => array(self::BELONGS_TO, 'Grupos', 'grupos_id'),
			'menusAcoes' => array(self::BELONGS_TO, 'MenusAcoes', 'menus_acoes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'menus_id' => 'Menus',
			'grupos_id' => 'Grupos',
			'menus_acoes_id' => 'Menus Acoes',
			'situacao'=>'Ativo'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('menus_id',$this->menus_id);
		$criteria->compare('grupos_id',$this->grupos_id);
		$criteria->compare('menus_acoes_id',$this->menus_acoes_id);
		$criteria->compare('situacao',$this->situacao);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MenusAcesso the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public  function listData(){
        $model = $this->findAll();
       return CHtml::listData($model,'id','descricao');
    }

}
