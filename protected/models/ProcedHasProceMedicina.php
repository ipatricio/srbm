<?php

/**
 * This is the model class for table "proced_has_proce_medicina".
 *
 * The followings are the available columns in table 'proced_has_proce_medicina':
 * @property integer $proceMedicina_id
 * @property integer $procedMedicina_id
 *
 * The followings are the available model relations:
 * @property Procedmedicina $procedMedicina
 * @property Procemedicina $proceMedicina
 */
class ProcedHasProceMedicina extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proced_has_proce_medicina';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('proceMedicina_id, procedMedicina_id', 'required'),
			array('proceMedicina_id, procedMedicina_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('proceMedicina_id, procedMedicina_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'procedMedicina' => array(self::BELONGS_TO, 'Procedmedicina', 'procedMedicina_id'),
			'proceMedicina' => array(self::BELONGS_TO, 'Procemedicina', 'proceMedicina_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'proceMedicina_id' => 'Proce Medicina',
			'procedMedicina_id' => 'Proced Medicina',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('proceMedicina_id',$this->proceMedicina_id);
		$criteria->compare('procedMedicina_id',$this->procedMedicina_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProcedHasProceMedicina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
