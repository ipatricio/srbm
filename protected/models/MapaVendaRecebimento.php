<?php

/**
 * This is the model class for table "mapa_venda_recebimento".
 *
 * The followings are the available columns in table 'mapa_venda_recebimento':
 * @property integer $id
 * @property string $data
 * @property integer $vidas
 * @property integer $contrato
 * @property integer $cod_plano
 * @property integer $id_cliente
 * @property integer $id_vendedor
 * @property double $valor
 * @property string $pri_rest
 * @property string $data_pri_rest
 * @property string $pri_conf
 * @property string $data_pri_conf
 * @property string $seg_conf
 * @property string $data_seg_conf
 *
 * The followings are the available model relations:
 * @property Clientes $idCliente
 * @property Corretores $idVendedor
 */
class MapaVendaRecebimento extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mapa_venda_recebimento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data, vidas, cod_plano, id_cliente, id_vendedor, valor', 'required'),
			array('vidas, contrato, cod_plano, id_cliente, id_vendedor', 'numerical', 'integerOnly'=>true),
			array('valor', 'numerical'),
			array('pri_rest, pri_conf, seg_conf', 'length', 'max'=>45),
			array('data_pri_rest, data_pri_conf, data_seg_conf', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data, vidas, contrato, cod_plano, id_cliente, id_vendedor, valor, pri_rest, data_pri_rest, pri_conf, data_pri_conf, seg_conf, data_seg_conf', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente' => array(self::BELONGS_TO, 'Clientes', 'id_cliente'),
			'idVendedor' => array(self::BELONGS_TO, 'Corretores', 'id_vendedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data' => 'Data',
			'vidas' => 'Vidas',
			'contrato' => 'Contrato',
			'cod_plano' => 'Cod Plano',
			'id_cliente' => 'Id Cliente',
			'id_vendedor' => 'Id Vendedor',
			'valor' => 'Valor',
			'pri_rest' => 'Pri Rest',
			'data_pri_rest' => 'Data Pri Rest',
			'pri_conf' => 'Pri Conf',
			'data_pri_conf' => 'Data Pri Conf',
			'seg_conf' => 'Seg Conf',
			'data_seg_conf' => 'Data Seg Conf',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('vidas',$this->vidas);
		$criteria->compare('contrato',$this->contrato);
		$criteria->compare('cod_plano',$this->cod_plano);
		$criteria->compare('id_cliente',$this->id_cliente);
		$criteria->compare('id_vendedor',$this->id_vendedor);
		$criteria->compare('valor',$this->valor);
		$criteria->compare('pri_rest',$this->pri_rest,true);
		$criteria->compare('data_pri_rest',$this->data_pri_rest,true);
		$criteria->compare('pri_conf',$this->pri_conf,true);
		$criteria->compare('data_pri_conf',$this->data_pri_conf,true);
		$criteria->compare('seg_conf',$this->seg_conf,true);
		$criteria->compare('data_seg_conf',$this->data_seg_conf,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MapaVendaRecebimento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
