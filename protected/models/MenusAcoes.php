<?php

/**
 * This is the model class for table "menus_acoes".
 *
 * The followings are the available columns in table 'menus_acoes':
 * @property integer $id
 * @property string $descricao
 * @property string $valor
 * @property integer $abamenu
 *
 * The followings are the available model relations:
 * @property MenusAcesso[] $menusAcessos
 */
class MenusAcoes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menus_acoes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descricao, valor, abamenu', 'required'),
			array('abamenu', 'numerical', 'integerOnly'=>true),
			array('descricao', 'length', 'max'=>45),
			array('valor', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, descricao, valor, abamenu', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menusAcessos' => array(self::HAS_MANY, 'MenusAcesso', 'menus_acoes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descricao' => 'Descricao',
			'valor' => 'Valor',
			'abamenu' => 'Abamenu',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('abamenu',$this->abamenu);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MenusAcoes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public  function listData(){
        $model = $this->findAll();
       return CHtml::listData($model,'id','descricao');
    }
}
