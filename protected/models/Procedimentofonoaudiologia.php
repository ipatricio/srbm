<?php

/**
 * This is the model class for table "procedimentofonoaudiologia".
 *
 * The followings are the available columns in table 'procedimentofonoaudiologia':
 * @property integer $id
 * @property string $data_proced
 * @property string $tmf
 * @property string $obs
 * @property string $result_exames
 * @property string $outros
 * @property integer $paciente_id
 * @property integer $velocidadeFala_id
 * @property integer $loudess_id
 * @property integer $pitchVocal_id
 * @property integer $hiperHiponasalidade_id
 * @property integer $coordenacaoFonoarticular_id
 * @property integer $TipoArticular_id
 * @property integer $NivelComprometimento_id
 * @property integer $classificacaoFala_id
 * @property integer $ressonanciaVocal_id
 * @property integer $classificacaoFonoarticular_id
 * @property integer $proceMedicina_id
 *
 * The followings are the available model relations:
 * @property Classificacaofala $classificacaoFala
 * @property Classificacaofonoarticular $classificacaoFonoarticular
 * @property Coordenacaofonoarticular $coordenacaoFonoarticular
 * @property Hiperhiponasalidade $hiperHiponasalidade
 * @property Loudess $loudess
 * @property Nivelcomprometimento $nivelComprometimento
 * @property Pitchvocal $pitchVocal
 * @property Procemedicina $proceMedicina
 * @property Ressonanciavocal $ressonanciaVocal
 * @property Tipoarticular $tipoArticular
 * @property Velocidadefala $velocidadeFala
 * @property Paciente $paciente
 */
class Procedimentofonoaudiologia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'procedimentofonoaudiologia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_proced, tmf, obs, result_exames, outros, paciente_id, velocidadeFala_id, loudess_id, pitchVocal_id, hiperHiponasalidade_id, coordenacaoFonoarticular_id, TipoArticular_id, NivelComprometimento_id, classificacaoFala_id, ressonanciaVocal_id, classificacaoFonoarticular_id, proceMedicina_id', 'required'),
			array('paciente_id, velocidadeFala_id, loudess_id, pitchVocal_id, hiperHiponasalidade_id, coordenacaoFonoarticular_id, TipoArticular_id, NivelComprometimento_id, classificacaoFala_id, ressonanciaVocal_id, classificacaoFonoarticular_id, proceMedicina_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_proced, tmf, obs, result_exames, outros, paciente_id, velocidadeFala_id, loudess_id, pitchVocal_id, hiperHiponasalidade_id, coordenacaoFonoarticular_id, TipoArticular_id, NivelComprometimento_id, classificacaoFala_id, ressonanciaVocal_id, classificacaoFonoarticular_id, proceMedicina_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'classificacaoFala' => array(self::BELONGS_TO, 'Classificacaofala', 'classificacaoFala_id'),
			'classificacaoFonoarticular' => array(self::BELONGS_TO, 'Classificacaofonoarticular', 'classificacaoFonoarticular_id'),
			'coordenacaoFonoarticular' => array(self::BELONGS_TO, 'Coordenacaofonoarticular', 'coordenacaoFonoarticular_id'),
			'hiperHiponasalidade' => array(self::BELONGS_TO, 'Hiperhiponasalidade', 'hiperHiponasalidade_id'),
			'loudess' => array(self::BELONGS_TO, 'Loudess', 'loudess_id'),
			'nivelComprometimento' => array(self::BELONGS_TO, 'Nivelcomprometimento', 'NivelComprometimento_id'),
			'pitchVocal' => array(self::BELONGS_TO, 'Pitchvocal', 'pitchVocal_id'),
			'proceMedicina' => array(self::BELONGS_TO, 'Procemedicina', 'proceMedicina_id'),
			'ressonanciaVocal' => array(self::BELONGS_TO, 'Ressonanciavocal', 'ressonanciaVocal_id'),
			'tipoArticular' => array(self::BELONGS_TO, 'Tipoarticular', 'TipoArticular_id'),
			'velocidadeFala' => array(self::BELONGS_TO, 'Velocidadefala', 'velocidadeFala_id'),
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_proced' => 'Data Procedimento',
			'tmf' => '3.5.TMF: /a/____',
			'obs' => 'Observações',
			'result_exames' => 'resultado dos exames',
			'outros' => 'Outros',
			'paciente_id' => 'Paciente',
			'velocidadeFala_id' => '2.4.Velocidade da fala:',
			'loudess_id' => '3.4.Loudness:',
			'pitchVocal_id' => '3.3.Pitch vocal: ',
			'hiperHiponasalidade_id' => 'Hipernasalidade',
			'coordenacaoFonoarticular_id' => '2.3.Coordenação peneumofonoarticulatória:',
			'TipoArticular_id' => '2.2 Tipo articulatório:',
			'NivelComprometimento_id' => '3.1.G__R__B__A__S__I__ Nivel de comprometimento',
			'classificacaoFala_id' => '2.1.	Classificação da inteligibilidade de fala espontânea:',
			'ressonanciaVocal_id' => '3.2.Ressonância vocal',
			'classificacaoFonoarticular_id' => 'Classificacao Fonoarticular',
			'proceMedicina_id' => 'Proce Medicina',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_proced',$this->data_proced,true);
		$criteria->compare('tmf',$this->tmf,true);
		$criteria->compare('obs',$this->obs,true);
		$criteria->compare('result_exames',$this->result_exames,true);
		$criteria->compare('outros',$this->outros,true);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('velocidadeFala_id',$this->velocidadeFala_id);
		$criteria->compare('loudess_id',$this->loudess_id);
		$criteria->compare('pitchVocal_id',$this->pitchVocal_id);
		$criteria->compare('hiperHiponasalidade_id',$this->hiperHiponasalidade_id);
		$criteria->compare('coordenacaoFonoarticular_id',$this->coordenacaoFonoarticular_id);
		$criteria->compare('TipoArticular_id',$this->TipoArticular_id);
		$criteria->compare('NivelComprometimento_id',$this->NivelComprometimento_id);
		$criteria->compare('classificacaoFala_id',$this->classificacaoFala_id);
		$criteria->compare('ressonanciaVocal_id',$this->ressonanciaVocal_id);
		$criteria->compare('classificacaoFonoarticular_id',$this->classificacaoFonoarticular_id);
		$criteria->compare('proceMedicina_id',$this->proceMedicina_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Procedimentofonoaudiologia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
