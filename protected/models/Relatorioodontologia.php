<?php

/**
 * This is the model class for table "relatorioodontologia".
 *
 * The followings are the available columns in table 'relatorioodontologia':
 * @property integer $id
 * @property string $tratamento_medico
 * @property string $usa_protese
 * @property string $doencas_familiares
 * @property string $sofreu_intervencao_cirurgica_bucal_facial
 * @property string $problemas_odontologicos
 * @property string $proble_mastigacao
 * @property string $aparelho_ortodontico
 * @property string $dificul_fonacao
 * @property string $osteoporose
 * @property string $qual_motivo_procurou
 * @property integer $paciente_id
 * @property string $data_procedimento
 *
 * The followings are the available model relations:
 * @property Paciente $paciente
 */
class Relatorioodontologia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'relatorioodontologia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tratamento_medico, usa_protese, doencas_familiares, sofreu_intervencao_cirurgica_bucal_facial, problemas_odontologicos, proble_mastigacao, aparelho_ortodontico, dificul_fonacao, osteoporose, qual_motivo_procurou, paciente_id, data_procedimento', 'required'),
			array('paciente_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tratamento_medico, usa_protese, doencas_familiares, sofreu_intervencao_cirurgica_bucal_facial, problemas_odontologicos, proble_mastigacao, aparelho_ortodontico, dificul_fonacao, osteoporose, qual_motivo_procurou, paciente_id, data_procedimento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tratamento_medico' => 'Está fazendo algum tratamento médico?',
			'usa_protese' => 'Usa alguma prótese? Qual tipo? Há quanto tempo? Alguma queixa?',
			'doencas_familiares' => 'Doenças bucais familiares? Quais?',
			'sofreu_intervencao_cirurgica_bucal_facial' => 'Sofreu alguma intervenção cirúrgica bucal/facial? Qual?',
			'problemas_odontologicos' => 'Quais problemas odontológicos já teve?',
			'proble_mastigacao' => 'Tem dificuldade na mastigação? Qual tipo de alimento?',
			'aparelho_ortodontico' => 'Usa ou usou aparelho ortodôntico? Por quanto tempo? Motivo? Tipo? Nome do ortodontista',
			'dificul_fonacao' => 'Dificuldade na fonação?',
			'osteoporose' => 'Tem osteoporose?',
			'qual_motivo_procurou' => 'Qual o motivo pelo qual nos procurou?',
			'paciente_id' => 'Paciente',
			'data_procedimento' => 'Data do Procedimento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tratamento_medico',$this->tratamento_medico,true);
		$criteria->compare('usa_protese',$this->usa_protese,true);
		$criteria->compare('doencas_familiares',$this->doencas_familiares,true);
		$criteria->compare('sofreu_intervencao_cirurgica_bucal_facial',$this->sofreu_intervencao_cirurgica_bucal_facial,true);
		$criteria->compare('problemas_odontologicos',$this->problemas_odontologicos,true);
		$criteria->compare('proble_mastigacao',$this->proble_mastigacao,true);
		$criteria->compare('aparelho_ortodontico',$this->aparelho_ortodontico,true);
		$criteria->compare('dificul_fonacao',$this->dificul_fonacao,true);
		$criteria->compare('osteoporose',$this->osteoporose,true);
		$criteria->compare('qual_motivo_procurou',$this->qual_motivo_procurou,true);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('data_procedimento',$this->data_procedimento,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Relatorioodontologia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
