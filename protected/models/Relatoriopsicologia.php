<?php

/**
 * This is the model class for table "relatoriopsicologia".
 *
 * The followings are the available columns in table 'relatoriopsicologia':
 * @property integer $id
 * @property integer $paciente_id
 * @property string $data_internacao_paciente
 * @property string $Setor_paciente_internado
 * @property integer $qtde_internacoes
 * @property string $info_historico_psicossocial
 * @property string $estado_emocional
 * @property string $rel_doenca_hosp
 * @property string $sequelas_emocionais
 * @property string $exame_psiquico
 * @property string $aval_preoperatorio
 * @property string $aval_posimediato
 * @property string $aval_posoperatorio
 * @property string $aval_transoperatorio
 * @property string $aval_emocial_posoperatorio
 * @property string $post_posimediato
 * @property integer $resposta_id
 * @property string $data_procedimento
 *
 * The followings are the available model relations:
 * @property Resposta $resposta
 * @property Paciente $paciente
 */
class Relatoriopsicologia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'relatoriopsicologia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paciente_id, data_internacao_paciente, Setor_paciente_internado, qtde_internacoes, info_historico_psicossocial, estado_emocional, rel_doenca_hosp, sequelas_emocionais, exame_psiquico, aval_preoperatorio, aval_posimediato, aval_posoperatorio, aval_transoperatorio, aval_emocial_posoperatorio, post_posimediato, resposta_id, data_procedimento', 'required'),
			array('paciente_id, qtde_internacoes, resposta_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, paciente_id, data_internacao_paciente, Setor_paciente_internado, qtde_internacoes, info_historico_psicossocial, estado_emocional, rel_doenca_hosp, sequelas_emocionais, exame_psiquico, aval_preoperatorio, aval_posimediato, aval_posoperatorio, aval_transoperatorio, aval_emocial_posoperatorio, post_posimediato, resposta_id, data_procedimento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'resposta' => array(self::BELONGS_TO, 'Resposta', 'resposta_id'),
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_procedimento' => 'Data do Procedimento',
			'paciente_id' => 'Paciente',
			'data_internacao_paciente' => 'Data de internação do paciente',
			'Setor_paciente_internado' => 'Setor em que o paciente está internado',
			'qtde_internacoes' => 'Quantidade de Internações',
			'info_historico_psicossocial' => 'Informação/Histórico sobre a doença com enfoque psicossocial',
			'estado_emocional' => 'Estado emocional geral atual frente ao quadro',
			'rel_doenca_hosp' => 'relação com a doença e postura frente à hospitalização e à vida',
			'sequelas_emocionais' => 'Sequelas emocionais?',
			'exame_psiquico' => 'Exame psíquico',
			'aval_preoperatorio' => 'Avaliação do auto-conceito, auto imagem e auto-estima pré-operatório.',
			'aval_posimediato' => 'Avaliação do auto-conceito, auto imagem e auto-estima pós-imediato.',
			'aval_posoperatorio' => 'Avaliação do auto-conceito, auto imagem e auto-estima pós-operatório.',
			'aval_transoperatorio' => 'Avaliação emocial trans-operatório.',
			'aval_emocial_posoperatorio' => 'Avaliação emocional pós-operatório.',
			'post_posimediato' => 'postura frente a cirurgia pós-imediato.',
			'resposta_id' => 'Resposta',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('data_internacao_paciente',$this->data_internacao_paciente,true);
		$criteria->compare('Setor_paciente_internado',$this->Setor_paciente_internado,true);
		$criteria->compare('qtde_internacoes',$this->qtde_internacoes);
		$criteria->compare('info_historico_psicossocial',$this->info_historico_psicossocial,true);
		$criteria->compare('estado_emocional',$this->estado_emocional,true);
		$criteria->compare('rel_doenca_hosp',$this->rel_doenca_hosp,true);
		$criteria->compare('sequelas_emocionais',$this->sequelas_emocionais,true);
		$criteria->compare('exame_psiquico',$this->exame_psiquico,true);
		$criteria->compare('aval_preoperatorio',$this->aval_preoperatorio,true);
		$criteria->compare('aval_posimediato',$this->aval_posimediato,true);
		$criteria->compare('aval_posoperatorio',$this->aval_posoperatorio,true);
		$criteria->compare('aval_transoperatorio',$this->aval_transoperatorio,true);
		$criteria->compare('aval_emocial_posoperatorio',$this->aval_emocial_posoperatorio,true);
		$criteria->compare('post_posimediato',$this->post_posimediato,true);
		$criteria->compare('resposta_id',$this->resposta_id);
		$criteria->compare('data_procedimento',$this->data_procedimento,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Relatoriopsicologia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
