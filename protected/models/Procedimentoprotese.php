<?php

/**
 * This is the model class for table "procedimentoprotese".
 *
 * The followings are the available columns in table 'procedimentoprotese':
 * @property integer $id
 * @property integer $protese_id
 * @property string $material_enviado_laboratorio
 * @property string $nome_laboratorio
 * @property string $descricao_material_enviado
 * @property string $tipo_imagem
 * @property string $escala_cor
 * @property string $cor
 * @property string $comentario
 * @property string $solicit_trabalho
 * @property integer $paciente_id
 * @property integer $imgmedicina_id
 * @property string $data_procedimento
 * @property integer $fabricacao_id
 *
 * The followings are the available model relations:
 * @property Fabricacao $fabricacao
 * @property Imgmedicina $imgmedicina
 * @property Paciente $paciente
 * @property Protese $protese
 */
class Procedimentoprotese extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'procedimentoprotese';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('protese_id, material_enviado_laboratorio, nome_laboratorio, descricao_material_enviado, tipo_imagem, escala_cor, cor, comentario, solicit_trabalho, paciente_id, imgmedicina_id, data_procedimento, fabricacao_id', 'required'),
			array('protese_id, paciente_id, imgmedicina_id, fabricacao_id', 'numerical', 'integerOnly'=>true),
			array('material_enviado_laboratorio', 'length', 'max'=>3),
			array('nome_laboratorio, tipo_imagem, escala_cor, cor', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, protese_id, material_enviado_laboratorio, nome_laboratorio, descricao_material_enviado, tipo_imagem, escala_cor, cor, comentario, solicit_trabalho, paciente_id, imgmedicina_id, data_procedimento, fabricacao_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fabricacao' => array(self::BELONGS_TO, 'Fabricacao', 'fabricacao_id'),
			'imgmedicina' => array(self::BELONGS_TO, 'Imgmedicina', 'imgmedicina_id'),
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
			'protese' => array(self::BELONGS_TO, 'Protese', 'protese_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'protese_id' => 'Prótese',
			'material_enviado_laboratorio' => 'Material enviado para o laboratorio',
			'nome_laboratorio' => 'Nome do Laboratório',
			'descricao_material_enviado' => 'descrição do material enviado para o laboratório',
			'tipo_imagem' => 'informações adicionais sobre a imagem',
			'escala_cor' => 'Escala Cor',
			'cor' => 'Cor',
			'comentario' => 'Comentários',
			'solicit_trabalho' => 'solicitação de trabalho',
			'paciente_id' => 'Paciente',
			'imgmedicina_id' => 'Tipo de imagem',
			'data_procedimento' => 'Data do Procedimento',
			'fabricacao_id' => 'Tipo de fabricação',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('protese_id',$this->protese_id);
		$criteria->compare('material_enviado_laboratorio',$this->material_enviado_laboratorio,true);
		$criteria->compare('nome_laboratorio',$this->nome_laboratorio,true);
		$criteria->compare('descricao_material_enviado',$this->descricao_material_enviado,true);
		$criteria->compare('tipo_imagem',$this->tipo_imagem,true);
		$criteria->compare('escala_cor',$this->escala_cor,true);
		$criteria->compare('cor',$this->cor,true);
		$criteria->compare('comentario',$this->comentario,true);
		$criteria->compare('solicit_trabalho',$this->solicit_trabalho,true);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('imgmedicina_id',$this->imgmedicina_id);
		$criteria->compare('data_procedimento',$this->data_procedimento,true);
		$criteria->compare('fabricacao_id',$this->fabricacao_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Procedimentoprotese the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
