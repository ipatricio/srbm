<?php

/**
 * This is the model class for table "login".
 *
 * The followings are the available columns in table 'login':
 * @property integer $id
 * @property string $usuario
 * @property string $senha
 * @property integer $grupos_id
 *
 * The followings are the available model relations:
 * @property Empresa $empresa
 */
class Login extends CActiveRecord
{
	public $repeat_senha	;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'login';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuario, grupos_id', 'required'),
			array('grupos_id', 'numerical', 'integerOnly'=>true),
			array('usuario, senha', 'length', 'max'=>255),

			#array('senha', 'compare', 'compareAttribute'=>'repeat_senha'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, usuario, grupos_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'grupos' => array(self::BELONGS_TO, 'Grupos', 'grupos_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'usuario' => 'Nome',
			'senha' => 'Senha',
			'grupos_id' => 'Grupo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('senha',$this->senha,true);
		$criteria->compare('grupos_id',$this->grupos_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Login the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave() {
        $pass = md5($this->senha);
        $this->senha = $pass;
        return true;
    }


    public function buildAccessRules($className){

    $actions = array();

    $className = strtolower(str_replace('Controller', '', $className));


	$criteriaMenu=new CDbCriteria;
	$criteriaMenu->select='*';  
	$criteriaMenu->condition="menus_id = :menu AND grupos_id =:grupo_id ";

	$criteriaMenu->params=array(':menu'=>Menus::model()->findByAttributes(array('valor'=>strtolower($className)))->id,
								':grupo_id'=>Yii::app()->user->id
	);

    $menus_acesso  = MenusAcesso::model()->findAll($criteriaMenu); 	
    
    foreach ($menus_acesso as $menu_acesso) {
    	
		array_push($actions, $menu_acesso->menusAcoes->valor);
											
	} 

		if(count($actions) > 0) {
		 $actionsIn = array('allow',
					'actions'=>$actions,
					'users'=>array(Yii::app()->user->name));
		}

		$access = array(
				
				(isset($actionsIn))? $actionsIn : array('deny', 'users' => array('*'))
				,
				array('deny',  // deny all users
					'users'=>array('*'),
				),
			 );


	   return $access;
    }

	public  function buildMenu(){

		if(Yii::app()->user->isGuest){
			return array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				#array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				#rray('label'=>'Contact', 'url'=>array('/site/contact')),
				#array('label'=>'Menu+', 'url'=>'#','items'=>array(
				#	array('label'=>'SubMenu1', 'url'=>array('/site/contact')),
				#	array('label'=>'SubMenu2', 'url'=>array('/site/contact')),
				#	)

				#	),
				/*
				  array('label'=>'Home <span class="caret"></span>', 
				  'url'=>array('/site/index'),
				  'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),
				  'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown",
				  "data-description"=>"our home page"), 
                        'items'=>array(
                            array('label'=>'Home 1 - Nivoslider', 'url'=>array('/site/index')),
                            array('label'=>'Home 2 - Bootstrap carousal', 'url'=>array('/site/page', 'view'=>'home2')),
                            array('label'=>'Home 3 - Piecemaker2', 'url'=>array('/site/page', 'view'=>'home3')),
                            array('label'=>'Home 4 - Static image', 'url'=>array('/site/page', 'view'=>'home4')),
                            array('label'=>'Home 5 - Video header', 'url'=>array('/site/page', 'view'=>'home5')),
                            array('label'=>'Home 6 - Without slider', 'url'=>array('/site/page', 'view'=>'home6')),
                        )),*/
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			);
		}
		else{
		$criteria=new CDbCriteria;
		$criteria->select='*';  
		$criteria->condition="situacao = 1 AND grupos_id= :grupo_id AND menus_acoes_id = (SELECT id FROM menus_acoes WHERE abamenu = 1 AND id = menus_acoes_id)";
	
		$criteria->order = 'menus_id asc, menus_acoes_id asc';
		$criteria->params=array(':grupo_id'=>Yii::app()->user->id);

		$menus_acesso = MenusAcesso::model()->findAll($criteria);
/*
		echo "<pre>";
		print_r($menus_acesso);
		exit;*/
		$arrayMenu = array();


		$menuAtual = '';
		
		$arrayActions = array();
		$arrayNameMenus = array();
		$abaOn = false;


		foreach ($menus_acesso as  $menuAcesso) {
			 if($menuAcesso->menus_acoes_id == 1){
			 	$arrayActions = array();
			 }
			 if($menuAtual == ''){

			 	$menuAtual = $menuAcesso->menus->descricao;
						
			 }
			 if( $menuAcesso->menus_acoes_id == 7){
			 	$abaOn = true;
			 }
			 if($abaOn){
					
						$arrayNameMenus[] = array($menuAtual,$arrayActions);
						$abaOn = false;

					$arrayActions = array();
			 }else{		
						array_push($arrayActions, array('label'=>$menuAcesso->menusAcoes->descricao,
						  'url'=>array('/'.strtolower($menuAcesso->menus->valor).'/'.$menuAcesso->menusAcoes->valor)));

			 }
			 if($menuAtual != $menuAcesso->menus->valor){
					$menuAtual = $menuAcesso->menus->descricao;
			 }
		}
		foreach ($arrayNameMenus as $value) {
					 
					 $arrayMenu[] = array('label' => $value[0].' <span class="caret"></span>',//name do menu
						 'url'=>'#', 'items'=>$value[1],//actions do menu
						 'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),
				  		 'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
					);
		}
			

		array_push($arrayMenu, array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest));
	    array_push($arrayMenu,
	    	array('label'=>'Logout ('.Yii::app()->user->name.')', 
	    		'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest));
				
		return $arrayMenu;
		}
		/*
		return array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Menu', 'url'=>array('/menuvg/index')),
				array('label'=>'Grupo', 'url'=>array('/grupos/index')),
				array('label'=>'Acesso', 'url'=>array('/menus/index')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			);*/
	}

}
