<?php

/**
 * This is the model class for table "permissao".
 *
 * The followings are the available columns in table 'permissao':
 * @property integer $id
 * @property integer $model_id
 * @property integer $grupo_id
 * @property integer $criar
 * @property integer $ler
 * @property integer $atualizar
 * @property integer $deletar
 *
 * The followings are the available model relations:
 * @property Grupo $grupo
 * @property Model $model
 */
class Permissao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'permissao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('model_id, grupo_id', 'required'),
			array('model_id, grupo_id, criar, ler, atualizar, deletar', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, model_id, grupo_id, criar, ler, atualizar, deletar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'grupo' => array(self::BELONGS_TO, 'Grupo', 'grupo_id'),
			'model' => array(self::BELONGS_TO, 'Model', 'model_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'model_id' => 'Model',
			'grupo_id' => 'Grupo',
			'criar' => 'Criar',
			'ler' => 'Ler',
			'atualizar' => 'Atualizar',
			'deletar' => 'Deletar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('model_id',$this->model_id);
		$criteria->compare('grupo_id',$this->grupo_id);
		$criteria->compare('criar',$this->criar);
		$criteria->compare('ler',$this->ler);
		$criteria->compare('atualizar',$this->atualizar);
		$criteria->compare('deletar',$this->deletar);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Permissao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
