<?php

/**
 * This is the model class for table "procedmedicina".
 *
 * The followings are the available columns in table 'procedmedicina':
 * @property integer $id
 * @property string $patologia_paciente
 * @property string $data_inicio
 * @property string $data_termino
 * @property string $dose_tratamento_complementar
 * @property string $data
 * @property string $comentarios_adicionais
 * @property integer $imgMedicina_id
 * @property integer $paciente_id
 * @property integer $cidCancer_id
 * @property integer $resposta_id
 *
 * The followings are the available model relations:
 * @property ProcedHasProceMedicina[] $procedHasProceMedicinas
 * @property Cidcancer $cidCancer
 * @property Resposta $resposta
 * @property Imgmedicina $imgMedicina
 * @property Paciente $paciente
 */
class Procedmedicina extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'procedmedicina';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('patologia_paciente, dose_tratamento_complementar, data, comentarios_adicionais, imgMedicina_id, paciente_id, cidCancer_id, resposta_id', 'required'),
			array('imgMedicina_id, paciente_id, cidCancer_id, resposta_id', 'numerical', 'integerOnly'=>true),
			array('patologia_paciente', 'length', 'max'=>255),
			array('dose_tratamento_complementar', 'length', 'max'=>60),
			array('data_inicio, data_termino', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, patologia_paciente, data_inicio, data_termino, dose_tratamento_complementar, data, comentarios_adicionais, imgMedicina_id, paciente_id, cidCancer_id, resposta_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'procedHasProceMedicinas' => array(self::HAS_MANY, 'ProcedHasProceMedicina', 'procedMedicina_id'),
			'cidCancer' => array(self::BELONGS_TO, 'Cidcancer', 'cidCancer_id'),
			'resposta' => array(self::BELONGS_TO, 'Resposta', 'resposta_id'),
			'imgMedicina' => array(self::BELONGS_TO, 'Imgmedicina', 'imgMedicina_id'),
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'patologia_paciente' => 'Qual patologia acometeu o paciente?',
			'data_inicio' => 'Data de início',
			'data_termino' => 'Data de término',
			'dose_tratamento_complementar' => 'Dose do tratamento Complementar',
			'data' => 'Data',
			'comentarios_adicionais' => 'Comentários adicionais',
			'imgMedicina_id' => 'Se sim, qual?',
			'paciente_id' => 'Paciente',
			'cidCancer_id' => 'CID do Câncer',
			'resposta_id' => 'Foi realizado algum tratamento complementar?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('patologia_paciente',$this->patologia_paciente,true);
		$criteria->compare('data_inicio',$this->data_inicio,true);
		$criteria->compare('data_termino',$this->data_termino,true);
		$criteria->compare('dose_tratamento_complementar',$this->dose_tratamento_complementar,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('comentarios_adicionais',$this->comentarios_adicionais,true);
		$criteria->compare('imgMedicina_id',$this->imgMedicina_id);
		$criteria->compare('paciente_id',$this->paciente_id);
		$criteria->compare('cidCancer_id',$this->cidCancer_id);
		$criteria->compare('resposta_id',$this->resposta_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Procedmedicina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
