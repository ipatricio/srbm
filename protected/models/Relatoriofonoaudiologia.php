<?php

/**
 * This is the model class for table "relatoriofonoaudiologia".
 *
 * The followings are the available columns in table 'relatoriofonoaudiologia':
 * @property integer $id
 * @property string $data_proced
 * @property string $queixa
 * @property string $tipo_aliment
 * @property string $freq_aliment
 * @property string $escape_aliment
 * @property string $condicao_resp
 * @property string $secrecao_oral
 * @property string $fumo
 * @property string $dificul_abrir_boca
 * @property string $alteracao_paladar
 * @property string $falta_apetite
 * @property string $tipo_saliva
 * @property string $presenca_mucosite
 * @property string $historico_doenca
 * @property string $medicao_atual
 * @property string $tipo_voz
 * @property string $paralisia_facial
 * @property string $alteracao_moviment
 * @property string $perda_auditiva
 * @property string $outros
 * @property integer $paciente_id
 *
 * The followings are the available model relations:
 * @property Paciente $paciente
 */
class Relatoriofonoaudiologia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'relatoriofonoaudiologia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_proced, queixa, tipo_aliment, freq_aliment, escape_aliment, condicao_resp, secrecao_oral, fumo, dificul_abrir_boca, alteracao_paladar, falta_apetite, tipo_saliva, presenca_mucosite, historico_doenca, medicao_atual, tipo_voz, paralisia_facial, alteracao_moviment, perda_auditiva, outros, paciente_id', 'required'),
			array('paciente_id', 'numerical', 'integerOnly'=>true),
			array('historico_doenca, medicao_atual, tipo_voz, paralisia_facial, alteracao_moviment, perda_auditiva, outros', 'length', 'max'=>45),
			array('data_proced','length','max'=>10),
			array('data_proced', 'date', 'format'=>'dd/MM/yyyy', 'message'=>'A data deve ter o formato dd/mm/aaaa.', 'allowEmpty'=>false),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_proced, queixa, tipo_aliment, freq_aliment, escape_aliment, condicao_resp, secrecao_oral, fumo, dificul_abrir_boca, alteracao_paladar, falta_apetite, tipo_saliva, presenca_mucosite, historico_doenca, medicao_atual, tipo_voz, paralisia_facial, alteracao_moviment, perda_auditiva, outros, paciente_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paciente' => array(self::BELONGS_TO, 'Paciente', 'paciente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_proced' => 'Data Procedimento',
			'queixa' => 'Queixa (rouquidão/ dif. para deglutir/ odinofagia/ falta de ar/ outros?',
			'tipo_aliment' => 'Tipo de alimentação: VO, sondas e ou estomias?',
			'freq_aliment' => 'Freqüência da alimentação ao dia e quantidade, tipo de alimentação ofertado.',
			'escape_aliment' => 'Há escape de alimento para cavidade nasal?',
			'condicao_resp' => 'Condições respiratórias? Traqueostomias? Hipersecretivos? Tipo de secreção?',
			'secrecao_oral' => 'Tem hábito de eliminar secreções orais (tipo saliva)?',
			'fumo' => 'Fumo/ álcool/ associação de ambos/ em quantidade e há quanto tempo?',
			'dificul_abrir_boca' => 'Sente dor ou dificuldade para abrir a boca/ diminuição da abertura oral?',
			'alteracao_paladar' => 'Alteração de paladar/ olfato?',
			'falta_apetite' => 'Falta de apetite, tem perdido peso ultimamente? Qual o peso atual?',
			'tipo_saliva' => 'Quanto à saliva: normal/ excesso/ secreção/ sensação de boca seca?',
			'presenca_mucosite' => 'Presença de mucosite?',
			'historico_doenca' => 'Histórico de doenças anteriores/ Histórico familiar de câncer?',
			'medicao_atual' => 'Medicação atual?',
			'tipo_voz' => 'Tipo de voz? Tipo de ressonância (leve/moderado/severo)?',
			'paralisia_facial' => 'Presença de paralisia facial? Há comprometimento da mímica facial?',
			'alteracao_moviment' => 'Alteração nos movimentos labiais da fala e na contenção oral da deglutição? Integibilidade da fala? Prejudicada? Inteligibilidade?',
			'perda_auditiva' => 'Perda auditiva? Se sim, qual tipo?',
			'outros' => 'Outros',
			'paciente_id' => 'Paciente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_proced',$this->data_proced,true);
		$criteria->compare('queixa',$this->queixa,true);
		$criteria->compare('tipo_aliment',$this->tipo_aliment,true);
		$criteria->compare('freq_aliment',$this->freq_aliment,true);
		$criteria->compare('escape_aliment',$this->escape_aliment,true);
		$criteria->compare('condicao_resp',$this->condicao_resp,true);
		$criteria->compare('secrecao_oral',$this->secrecao_oral,true);
		$criteria->compare('fumo',$this->fumo,true);
		$criteria->compare('dificul_abrir_boca',$this->dificul_abrir_boca,true);
		$criteria->compare('alteracao_paladar',$this->alteracao_paladar,true);
		$criteria->compare('falta_apetite',$this->falta_apetite,true);
		$criteria->compare('tipo_saliva',$this->tipo_saliva,true);
		$criteria->compare('presenca_mucosite',$this->presenca_mucosite,true);
		$criteria->compare('historico_doenca',$this->historico_doenca,true);
		$criteria->compare('medicao_atual',$this->medicao_atual,true);
		$criteria->compare('tipo_voz',$this->tipo_voz,true);
		$criteria->compare('paralisia_facial',$this->paralisia_facial,true);
		$criteria->compare('alteracao_moviment',$this->alteracao_moviment,true);
		$criteria->compare('perda_auditiva',$this->perda_auditiva,true);
		$criteria->compare('outros',$this->outros,true);
		$criteria->compare('paciente_id',$this->paciente_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Relatoriofonoaudiologia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
