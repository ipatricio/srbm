<?php
$this->breadcrumbs=array(
	'Procedimentodentisticas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procedimentodentistica','url'=>array('index')),
	array('label'=>'Create Procedimentodentistica','url'=>array('create')),
	array('label'=>'View Procedimentodentistica','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Procedimentodentistica','url'=>array('admin')),
);
?>

<h1>Update Procedimentodentistica <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>