<?php
$this->breadcrumbs=array(
	'Procedimentodentisticas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Procedimentodentistica','url'=>array('index')),
	array('label'=>'Create Procedimentodentistica','url'=>array('create')),
	array('label'=>'Update Procedimentodentistica','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Procedimentodentistica','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procedimentodentistica','url'=>array('admin')),
);
?>

<h1>View Procedimentodentistica #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'paciente_id',
	),
)); ?>
