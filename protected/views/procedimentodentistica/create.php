<?php
$this->breadcrumbs=array(
	'Procedimentodentisticas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Procedimentodentistica','url'=>array('index')),
	array('label'=>'Manage Procedimentodentistica','url'=>array('admin')),
);
?>

<h1>Create Procedimentodentistica</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>