<?php
$this->breadcrumbs=array(
	'Procedimentodentisticas',
);

$this->menu=array(
	array('label'=>'Create Procedimentodentistica','url'=>array('create')),
	array('label'=>'Manage Procedimentodentistica','url'=>array('admin')),
);
?>

<h1>Procedimentodentisticas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
