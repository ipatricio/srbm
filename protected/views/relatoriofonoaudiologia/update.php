<?php
$this->breadcrumbs=array(
	'Relatoriofonoaudiologias'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Relatoriofonoaudiologia','url'=>array('index')),
	array('label'=>'Create Relatoriofonoaudiologia','url'=>array('create')),
	array('label'=>'View Relatoriofonoaudiologia','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Relatoriofonoaudiologia','url'=>array('admin')),
);
?>

<h1>Update Relatoriofonoaudiologia <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>