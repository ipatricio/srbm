<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_proced')); ?>:</b>
	<?php echo CHtml::encode($data->data_proced); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('queixa')); ?>:</b>
	<?php echo CHtml::encode($data->queixa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_aliment')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_aliment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('freq_aliment')); ?>:</b>
	<?php echo CHtml::encode($data->freq_aliment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('escape_aliment')); ?>:</b>
	<?php echo CHtml::encode($data->escape_aliment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('condicao_resp')); ?>:</b>
	<?php echo CHtml::encode($data->condicao_resp); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('secrecao_oral')); ?>:</b>
	<?php echo CHtml::encode($data->secrecao_oral); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fumo')); ?>:</b>
	<?php echo CHtml::encode($data->fumo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dificul_abrir_boca')); ?>:</b>
	<?php echo CHtml::encode($data->dificul_abrir_boca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alteracao_paladar')); ?>:</b>
	<?php echo CHtml::encode($data->alteracao_paladar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('falta_apetite')); ?>:</b>
	<?php echo CHtml::encode($data->falta_apetite); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_saliva')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_saliva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('presenca_mucosite')); ?>:</b>
	<?php echo CHtml::encode($data->presenca_mucosite); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('historico_doenca')); ?>:</b>
	<?php echo CHtml::encode($data->historico_doenca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('medicao_atual')); ?>:</b>
	<?php echo CHtml::encode($data->medicao_atual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_voz')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_voz); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paralisia_facial')); ?>:</b>
	<?php echo CHtml::encode($data->paralisia_facial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alteracao_moviment')); ?>:</b>
	<?php echo CHtml::encode($data->alteracao_moviment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('perda_auditiva')); ?>:</b>
	<?php echo CHtml::encode($data->perda_auditiva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('outros')); ?>:</b>
	<?php echo CHtml::encode($data->outros); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paciente_id')); ?>:</b>
	<?php echo CHtml::encode($data->paciente_id); ?>
	<br />

	*/ ?>

</div>