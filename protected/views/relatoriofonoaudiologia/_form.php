<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'relatoriofonoaudiologia-form',
	'enableAjaxValidation'=>false,
)); 


?>

	<p class="help-block">Campos com <span class="required">*</span> são obrigatórios.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php #echo $form->textAreaRow($model,'data_proced',array('class'=>'span5')); ?>
	
	<p>Data Procedimento<span class="required">*</span></p>
	<?php 

		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $model,
    'attribute' => 'data_proced',
    #'flat'=>true,
    'language' => 'pt',
    // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js',
    'htmlOptions' => array(
        'size' => '10',
        'maxlength' => '10',
    ),	
	));
	?>


	<?php echo $form->textFieldRow($model,'paciente_id',
	array('class'=>'span5','value'=>Paciente::model()->findByPk($_GET['id'])->nome,'disabled'=>true)); ?>
	
	
        <div class="form group col-lg-6">
	<?php echo $form->textAreaRow($model,'queixa',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>
        
	<?php echo $form->textAreaRow($model,'tipo_aliment',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'freq_aliment',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'escape_aliment',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'condicao_resp',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'secrecao_oral',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'fumo',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'dificul_abrir_boca',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'alteracao_paladar',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'falta_apetite',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'tipo_saliva',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'presenca_mucosite',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'historico_doenca',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'medicao_atual',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'tipo_voz',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'paralisia_facial',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'alteracao_moviment',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'perda_auditiva',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'outros',array('rows'=>3, 'cols'=>25, 'class'=>'span12')); ?>

	</div>
        <div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Enviar' : 'Save',
                    
		)); ?>
             <input type="button" class="btn btn-primary"  onclick="javascript: history.go(-1)" value="Voltar"> 

	</div>

<?php $this->endWidget(); ?>
