<?php
$this->breadcrumbs=array(
	'Relatoriofonoaudiologias'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Relatoriofonoaudiologia','url'=>array('index')),
	array('label'=>'Create Relatoriofonoaudiologia','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('relatoriofonoaudiologia-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Relatoriofonoaudiologias</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'relatoriofonoaudiologia-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'data_proced',
		'queixa',
		'tipo_aliment',
		'freq_aliment',
		'escape_aliment',
		/*
		'condicao_resp',
		'secrecao_oral',
		'fumo',
		'dificul_abrir_boca',
		'alteracao_paladar',
		'falta_apetite',
		'tipo_saliva',
		'presenca_mucosite',
		'historico_doenca',
		'medicao_atual',
		'tipo_voz',
		'paralisia_facial',
		'alteracao_moviment',
		'perda_auditiva',
		'outros',
		'paciente_id',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
