<?php
/*
$this->breadcrumbs=array(
	'Relatório de fonoaudiologia'=>array('index'),
	'Create',
);
*/

$this->menu=array(
	array('label'=>'Listar Relatório de fonoaudiologia','url'=>array('index')),
	array('label'=>'Gerenciar Relatório fonoaudiologia','url'=>array('admin')),
);
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>