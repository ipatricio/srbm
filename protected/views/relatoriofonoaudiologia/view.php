<?php
/*
$this->breadcrumbs=array(
	'Relatoriofonoaudiologias'=>array('index'),
	$model->id,
);
*/
$this->menu=array(
	array('label'=>'List Relatoriofonoaudiologia','url'=>array('index')),
	array('label'=>'Create Relatoriofonoaudiologia','url'=>array('create')),
	array('label'=>'Update Relatoriofonoaudiologia','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Relatoriofonoaudiologia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Relatoriofonoaudiologia','url'=>array('admin')),
);
?>

<h4 class="center">Relatório Fonoaudiologia  de <?php echo  $model->paciente->nome; ?></h4>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		#'id',
		'data_proced',
		'queixa',
		'tipo_aliment',
		'freq_aliment',
		'escape_aliment',
		'condicao_resp',
		'secrecao_oral',
		'fumo',
		'dificul_abrir_boca',
		'alteracao_paladar',
		'falta_apetite',
		'tipo_saliva',
		'presenca_mucosite',
		'historico_doenca',
		'medicao_atual',
		'tipo_voz',
		'paralisia_facial',
		'alteracao_moviment',
		'perda_auditiva',
		'outros',
		'paciente.nome',
	),
)); ?>
