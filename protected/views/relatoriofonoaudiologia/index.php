<?php
$this->breadcrumbs=array(
	'Relatoriofonoaudiologias',
);

$this->menu=array(
	array('label'=>'Create Relatoriofonoaudiologia','url'=>array('create')),
	array('label'=>'Manage Relatoriofonoaudiologia','url'=>array('admin')),
);
?>

<h4>Relatorio de fonoaudiologia</h4>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
