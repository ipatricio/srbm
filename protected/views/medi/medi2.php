<?php
$this->breadcrumbs=array(
	'Pacientes'=>array('index'),
	'Gerenciar',
);

$this->menu=array(
	array('label'=>'Listar Paciente','url'=>array('index')),
	array('label'=>'Criar Paciente','url'=>array('create')),
);

?>


    <a  href="<?php echo Yii::app()->baseUrl.'/relatoriomedicina/admin'.'/'.$model->id ?>">Cadastro Específico de Medicina</a> 
    <br><br>
     <a  href="<?php echo 'anexos/admin'.'?id='.$model->id  ?>">Anexos de Medicina</a>
    <br><br>
    <a  href="<?php echo  Yii::app()->baseUrl.'/procedimentomedicina/admin/'.$model->id ?>">Procedimento de Medicina</a>
    <br><br>
    <input type="button" class="btn btn-primary"  onclick="javascript: history.go(-1)" value="Voltar"> 
