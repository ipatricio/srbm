
                
                    
<div class="section">
<div class="container">
<div class="row">
 

<!-- Form Name -->
<legend><a href="/srbm-novo/fono/index">Área de Fonoaudiologia</a> -&gt; <a href="/srbm-novo/fono/fono1/id/">Fonoaudiologia</a> -&gt; Dados Gerais</legend>     

<legend>ATUAÇÃO FONOAUDIOLÓGICA EM PACIENTES COM CANCER DE BOCA E OROFARINGE</legend>

<div class="form-group col-lg-12">

<h4>Objetivo da reabilitação:</h4> <h5>Maximizar o uso das estruturas remanescentes para restabelecer a alimentação via oral e melhorar a comunicação oral.</h5>
</div>

<div class="form-group col-lg-12">
<h4>FONOTERAPIA:</h4> <h5> A fonoterapia deve ser definido de acordo com as necessidades funcionais, inicialmente no caso de disfagias, o atendimento será realizado duas vezes por semana, passando para uma vez, conforme evolução clinica do paciente. Vai enfocar a retomada da alimentação por via oral , por se tratar se uma função neurofisiológica ligada à sobrevivência; por alguns exercícios fonoarticulatorios, tais como: fala automática, para melhorar a clareza articulatória do paciente, favorecendo a comunicação oral; a fonoterapia pode se estender de 2 a 4 meses, dependendo das particularidade de cada caso.</h5>
</div>

  </div>
 </div>
</div>



<div class="section">
 <div class="container">
     <div class="row">
         
<legend>PROTOCOLOS DE  AVALIAÇÃO E REABILITAÇÃO.</legend>

<h4>ATUAÇÃO FONOAUDIOLÓGICA ANTES DA ADAPTAÇÃO DAS PRÓTESES.</h4>
<h5>•	Boa abertura bucal vertical, no mínimo de 30mm ; boa abertura oral horizontal de, pelo menos, 60mm, para possibilitar a moldagem da prótese pelo dentista e uma boa adaptação desse aparelho pelo paciente durante as funções de fala e deglutição.</h5>
<h5>•	Paciente sem sonda nasoenteral.</h5>
<br>
<h4>ATUAÇÃO FONOAUDIOLÓGICA DURANTE DA ADAPTAÇÃO DA PRÓTESE.</h4>
<h5>•	O fonoaudiólogo acompanhará e auxiliará o cirurgião dentista na moldagem e na adaptação funcional do paciente à prótese: obturadora de maxila, obturadora faríngea, elevadora de palato, rebaixadora de palato e próteses mandibulares;</h5>

<br>
<h4>Segue sugestão de etapas a serem realizadas na confecção da prótese.</h4>

<h5>•	A moldagem funcional de todas as próteses segue as etapas a seguir. Porem primeiramente serão detalhadas as principais etapas de confecção  do obturador faríngeo e rebaixador de palato:</h5>
<h5>1° etapa: confecção e fixação de um tutor metálico na região posterior da prótese. Esse tutor normalmente esta localizado na região de maior contração entre as paredes laterais e posterior da faringe.</h5>
<h5>2° etapa: colocação de um arcabouço de cera. O objetivo deste é preencher a cavidade interna do tutor alem dos limites deste.</h5>
<h5>3° etapa: colocação de material de moldagem sobre o aracbouço de cera. Os materiais utilizados p este tipo de moldagem pode ser de resina resiliente ou resinas de condicionadores de tecido ou ainda resilientes para reembasamento. Esses tipos de materiais apresentam um escoamento lento e gradativo, proporcionando um tempo adequado para a moldagem da região esfíncter velofaringeo.</h5>  
<h5>4° etapa: Realização da moldagem funcional:  com a prótese em posição na boca do paciente é solicitado ao paciente deglutir liquida, liquido - pastoso e saliva para que seja avaliada a presença ou ausência do refluxo nasal e de estases alimentares na boca e orofaringe. E seguida é realizada a avaliação da voz. O paciente é solicitado a realizar a emissão de sons que apresentam uma maior mobilidade das paredes laterais e posterior da faringe, como as vogais /i/ e /u/, os fonemas /s/ e /z/, contagem de números e silabas contendo sons fricativos e plosivo surdo, que necessitam de uma maior pressão intraoral. </h5>     
<h5>5° etapa: Avaliação da moldagem: a prótese é retirada, verificando-se a  presença de impressões no material de moldagem decorrentes dos movimentos da musculatura faríngea. Se necessário, repetem-se a fases anterior para uma melhor adaptação deste obturador.</h5>     
<h5>6° etapa: finalização do obturador: O obturador é considerado pronto para ser acrilisado quando se alcança, no mínimo, algum ganho funcional, como, por exemplo, redução de hipernasalidade, redução do refluxo nasal, melhora da inteligibilidade de fala e ausência de desconforto com o uso do obturador durante a deglutição, fala e respiração nasal. A experiência clinica mostra que,muitas vezes, não é possível obter-se um resultado funcional completo numa primeira moldagem. É necessário que haja uma adaptação das estruturas remanescentes ao obturador faríngeo, que age como um estimulo proprioceptivo para a movimentação das paredes laterais e posterior da faringe.</h5> 
     
<br>
<br>

<h4>Obs.: A nasofibroscopia para a realização e o monitoração da moldagem funcional do obturador faríngeo; pois este exame permite a visualização completa do esfíncter velofaringeo durante a fala e deglutição, possibilitando ao dentista posicionar e confeccionar o obturador na altura, largura e extensão correspondentes ao gap velofaringeo observado nestas funções.</h4>

<h4>Obs.: reavaliação após 1 mês e depois 3 meses, para reajustes, como desgastes ou aumento do obturador, quando possível ou necessário.</h4>     

<h4>Obs.: Nas próteses de rebaixador de palato é solicitado a produção de sons que normalmente estão alterados como os /k/ , /g/, /r/ e os linguodentais /t,d,n,l/, associado a vogais e a palavras. Importante avaliar os aspectos ressonantais, verificando se houve mudança na ressonância como redução de hipernasalidade ou presença de hiponasalidade. Em seguida é avaliado a deglutição de  liquido, liquido - pastoso e pastoso. Após isso verifica-se a quantidade de estase em cavidade oral e bucal. O paciente será questionado quanto à sensação de alimento parado na faringe e também se a prótese facilitou ou não a deglutição. De acordo com os resultados da avaliação, podem ser realizados novos ajustes no rebaixador, aumentando-o ou reduzindo-o para melhorar os aspectos funcionais.</h4>     
 <br>
     </div>
 </div>
</div>

<div class="section">
 <div class="container">
     <div class="row">

<legend>ATUAÇÃO FONOAUDIOLOGICA APÓS A ADAPTAÇÃO DA PRÓTESE.</legend>    
<h5>•	A fonoterapia deve continuar após o recebimento da prótese para que o fonoaudiólogo auxilie o paciente a se adaptar com esse novo aparelho durante as funções de mastigação (se for o caso), deglutição e fonoarticulação. </h5>
<h5>•	O paciente pode referir nas primeiras semanas de uso de prótese, piora ou dificuldade na realização das funções orais. Por isso é importante que se verifique se a queixa do paciente se deve a um problema técnico da prótese ou se é apenas uma questão de adaptação.</h5> 
<h5>•	Orientá-lo quanto  à sobrearticualação e à redução da velocidade da fala para que haja maior clareza articulatória, que pode ser favorecida pelas próteses dentarias, caso o paciente realize uma boa abertura bucal; no inicio o paciente pode apresentar uma tendência de manter os dentes cerrados, por falta de habilidade e segurança no uso da prótese.</h5>     
<h5>•	Realizar treinos de deglutição e da mastigação com as novas próteses, iniciando com as consistências que o paciente esta mais acostumado e depois incentivando-o a testar consistências mais firmes, quando possível.</h5>     
<h5>•	Caso o paciente apresente muita dificuldade para se alimentar com as próteses, é possível, na primeira semana, orienta-lo a usar a prótese durante todo o dia, exceto nos momentos das refeições. Na segunda semana, o fonoaudiólogo realiza o treino da deglutição com consistência líquida, líquido-pastosa, com o paciente fazendo uso da prótese, e o mesmo treino também deverá ser orientado para casa. Gradativamente, vão sendo introduzidas as demais consistências, até que o paciente consiga realizar todas as refeições com as próteses dentarias.</h5>     
     

<div class="form-group col-lg-12">
        <br><br>   
    <label for="singlebutton"></label>
    <input class="btn btn-primary" onclick="javascript: history.go(-1)" value="Voltar" type="button"> 
    
  
    </div>
     </div>
 </div>
</div>                 

            