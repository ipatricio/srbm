<?php
$this->breadcrumbs=array(
	'Relatório de medicina'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar relatórios de medicina','url'=>array('index')),
	array('label'=>'Gerenciar relatório medicina','url'=>array('admin')),
);
?>



<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>