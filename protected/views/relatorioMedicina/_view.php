<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paciente_id')); ?>:</b>
	<?php echo CHtml::encode($data->paciente_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alergico_med')); ?>:</b>
	<?php echo CHtml::encode($data->alergico_med); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('medic_atual')); ?>:</b>
	<?php echo CHtml::encode($data->medic_atual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trat_med')); ?>:</b>
	<?php echo CHtml::encode($data->trat_med); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proble_resp')); ?>:</b>
	<?php echo CHtml::encode($data->proble_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_calmo')); ?>:</b>
	<?php echo CHtml::encode($data->temp_calmo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('medic_dormir')); ?>:</b>
	<?php echo CHtml::encode($data->medic_dormir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proble_cardi')); ?>:</b>
	<?php echo CHtml::encode($data->proble_cardi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_procedimento')); ?>:</b>
	<?php echo CHtml::encode($data->data_procedimento); ?>
	<br />

	*/ ?>

</div>