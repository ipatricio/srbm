<?php
$this->breadcrumbs=array(
	'Relatorio Medicinas'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List RelatorioMedicina','url'=>array('index')),
	array('label'=>'Create RelatorioMedicina','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('relatorio-medicina-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Relatorio Medicinas</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'relatorio-medicina-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'paciente_id',
		'alergico_med',
		'medic_atual',
		'trat_med',
		'proble_resp',
		/*
		'temp_calmo',
		'medic_dormir',
		'proble_cardi',
		'data_procedimento',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
