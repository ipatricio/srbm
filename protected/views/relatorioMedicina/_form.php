<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'relatorio-medicina-form',
	'enableAjaxValidation'=>false,
)); ?>
    
	<p class="help-block">Campos com <span class="required">*</span> são obrigatórios.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php echo $form->textFieldRow($model,'data_procedimento',array('class'=>'span5')); ?>
        <?php echo $form->textFieldRow($model,'paciente_id',
	array('class'=>'span5','value'=>Paciente::model()->findByPk($_GET['id'])->nome,'disabled'=>true)); ?>

	<?php echo $form->textAreaRow($model,'alergico_med',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'medic_atual',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'trat_med',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'proble_resp',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'temp_calmo',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'medic_dormir',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'proble_cardi',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Salvar' : 'Save',
		)); ?>
               <input type="button" class="btn btn-primary"  onclick="javascript: history.go(-1)" value="Voltar"> 

	</div>

<?php $this->endWidget(); ?>
