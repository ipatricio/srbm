<?php
$this->breadcrumbs=array(
	'Relatorio Medicinas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List RelatorioMedicina','url'=>array('index')),
	array('label'=>'Create RelatorioMedicina','url'=>array('create')),
	array('label'=>'Update RelatorioMedicina','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete RelatorioMedicina','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage RelatorioMedicina','url'=>array('admin')),
);
?>

<h1>View RelatorioMedicina #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'paciente_id',
		'alergico_med',
		'medic_atual',
		'trat_med',
		'proble_resp',
		'temp_calmo',
		'medic_dormir',
		'proble_cardi',
		'data_procedimento',
	),
)); ?>
