<?php
$this->breadcrumbs=array(
	'Relatorio Medicinas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RelatorioMedicina','url'=>array('index')),
	array('label'=>'Create RelatorioMedicina','url'=>array('create')),
	array('label'=>'View RelatorioMedicina','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage RelatorioMedicina','url'=>array('admin')),
);
?>

<h1>Update RelatorioMedicina <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>