<?php
$this->breadcrumbs=array(
	'Loudesses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Loudess','url'=>array('index')),
	array('label'=>'Create Loudess','url'=>array('create')),
	array('label'=>'View Loudess','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Loudess','url'=>array('admin')),
);
?>

<h1>Update Loudess <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>