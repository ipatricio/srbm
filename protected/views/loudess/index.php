<?php
$this->breadcrumbs=array(
	'Loudesses',
);

$this->menu=array(
	array('label'=>'Create Loudess','url'=>array('create')),
	array('label'=>'Manage Loudess','url'=>array('admin')),
);
?>

<h1>Loudesses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
