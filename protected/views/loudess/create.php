<?php
$this->breadcrumbs=array(
	'Loudesses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Loudess','url'=>array('index')),
	array('label'=>'Manage Loudess','url'=>array('admin')),
);
?>

<h1>Create Loudess</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>