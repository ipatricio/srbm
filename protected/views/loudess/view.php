<?php
$this->breadcrumbs=array(
	'Loudesses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Loudess','url'=>array('index')),
	array('label'=>'Create Loudess','url'=>array('create')),
	array('label'=>'Update Loudess','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Loudess','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Loudess','url'=>array('admin')),
);
?>

<h1>View Loudess #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
