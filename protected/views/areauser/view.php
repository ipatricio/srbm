<?php
$this->breadcrumbs=array(
	'Areausers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Areauser','url'=>array('index')),
	array('label'=>'Create Areauser','url'=>array('create')),
	array('label'=>'Update Areauser','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Areauser','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Areauser','url'=>array('admin')),
);
?>

<h1>View Areauser #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
