<?php
$this->breadcrumbs=array(
	'Areausers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Areauser','url'=>array('index')),
	array('label'=>'Manage Areauser','url'=>array('admin')),
);
?>

<h1>Create Areauser</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>