<?php
$this->breadcrumbs=array(
	'Areausers',
);

$this->menu=array(
	array('label'=>'Create Areauser','url'=>array('create')),
	array('label'=>'Manage Areauser','url'=>array('admin')),
);
?>

<h1>Areausers</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
