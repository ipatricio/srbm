<?php
$this->breadcrumbs=array(
	'Areausers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Areauser','url'=>array('index')),
	array('label'=>'Create Areauser','url'=>array('create')),
	array('label'=>'View Areauser','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Areauser','url'=>array('admin')),
);
?>

<h1>Update Areauser <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>