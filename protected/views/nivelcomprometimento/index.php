<?php
$this->breadcrumbs=array(
	'Nivelcomprometimentos',
);

$this->menu=array(
	array('label'=>'Create Nivelcomprometimento','url'=>array('create')),
	array('label'=>'Manage Nivelcomprometimento','url'=>array('admin')),
);
?>

<h1>Nivelcomprometimentos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
