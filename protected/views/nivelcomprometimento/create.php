<?php
$this->breadcrumbs=array(
	'Nivelcomprometimentos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Nivelcomprometimento','url'=>array('index')),
	array('label'=>'Manage Nivelcomprometimento','url'=>array('admin')),
);
?>

<h1>Create Nivelcomprometimento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>