<?php
$this->breadcrumbs=array(
	'Nivelcomprometimentos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Nivelcomprometimento','url'=>array('index')),
	array('label'=>'Create Nivelcomprometimento','url'=>array('create')),
	array('label'=>'View Nivelcomprometimento','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Nivelcomprometimento','url'=>array('admin')),
);
?>

<h1>Update Nivelcomprometimento <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>