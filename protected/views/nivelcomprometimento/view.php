<?php
$this->breadcrumbs=array(
	'Nivelcomprometimentos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Nivelcomprometimento','url'=>array('index')),
	array('label'=>'Create Nivelcomprometimento','url'=>array('create')),
	array('label'=>'Update Nivelcomprometimento','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Nivelcomprometimento','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Nivelcomprometimento','url'=>array('admin')),
);
?>

<h1>View Nivelcomprometimento #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
