<?php
$this->breadcrumbs=array(
	'Relatório de psicologia'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar relatório de psicologia','url'=>array('index')),
	array('label'=>'Gerenciar relatório psicologia','url'=>array('admin')),
);
?>



<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>