<?php
$this->breadcrumbs=array(
	'Relatoriopsicologias'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Relatoriopsicologia','url'=>array('index')),
	array('label'=>'Create Relatoriopsicologia','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('relatoriopsicologia-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Relatoriopsicologias</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'relatoriopsicologia-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'paciente_id',
		'data_internacao_paciente',
		'Setor_paciente_internado',
		'qtde_internacoes',
		'info_historico_psicossocial',
		/*
		'estado_emocional',
		'rel_doenca_hosp',
		'sequelas_emocionais',
		'exame_psiquico',
		'aval_preoperatorio',
		'aval_posimediato',
		'aval_posoperatorio',
		'aval_transoperatorio',
		'aval_emocial_posoperatorio',
		'post_posimediato',
		'resposta_id',
		'data_procedimento',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
