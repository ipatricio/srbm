<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paciente_id')); ?>:</b>
	<?php echo CHtml::encode($data->paciente_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_internacao_paciente')); ?>:</b>
	<?php echo CHtml::encode($data->data_internacao_paciente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Setor_paciente_internado')); ?>:</b>
	<?php echo CHtml::encode($data->Setor_paciente_internado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qtde_internacoes')); ?>:</b>
	<?php echo CHtml::encode($data->qtde_internacoes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('info_historico_psicossocial')); ?>:</b>
	<?php echo CHtml::encode($data->info_historico_psicossocial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado_emocional')); ?>:</b>
	<?php echo CHtml::encode($data->estado_emocional); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('rel_doenca_hosp')); ?>:</b>
	<?php echo CHtml::encode($data->rel_doenca_hosp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sequelas_emocionais')); ?>:</b>
	<?php echo CHtml::encode($data->sequelas_emocionais); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exame_psiquico')); ?>:</b>
	<?php echo CHtml::encode($data->exame_psiquico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aval_preoperatorio')); ?>:</b>
	<?php echo CHtml::encode($data->aval_preoperatorio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aval_posimediato')); ?>:</b>
	<?php echo CHtml::encode($data->aval_posimediato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aval_posoperatorio')); ?>:</b>
	<?php echo CHtml::encode($data->aval_posoperatorio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aval_transoperatorio')); ?>:</b>
	<?php echo CHtml::encode($data->aval_transoperatorio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aval_emocial_posoperatorio')); ?>:</b>
	<?php echo CHtml::encode($data->aval_emocial_posoperatorio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_posimediato')); ?>:</b>
	<?php echo CHtml::encode($data->post_posimediato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('resposta_id')); ?>:</b>
	<?php echo CHtml::encode($data->resposta_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_procedimento')); ?>:</b>
	<?php echo CHtml::encode($data->data_procedimento); ?>
	<br />

	*/ ?>

</div>