<?php
$this->breadcrumbs=array(
	'Relatoriopsicologias'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Relatoriopsicologia','url'=>array('index')),
	array('label'=>'Create Relatoriopsicologia','url'=>array('create')),
	array('label'=>'Update Relatoriopsicologia','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Relatoriopsicologia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Relatoriopsicologia','url'=>array('admin')),
);
?>
<h1>Relatório Psicologia  de <?php echo  $model->paciente->nome; ?></h1>


<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
#		'id',
		'paciente.nome',
		'data_internacao_paciente',
		'Setor_paciente_internado',
		'qtde_internacoes',
		'info_historico_psicossocial',
		'estado_emocional',
		'rel_doenca_hosp',
		'sequelas_emocionais',
		'exame_psiquico',
		'aval_preoperatorio',
		'aval_posimediato',
		'aval_posoperatorio',
		'aval_transoperatorio',
		'aval_emocial_posoperatorio',
		'post_posimediato',
		'resposta_id',
		'data_procedimento',
	),
)); ?>
