<?php
$this->breadcrumbs=array(
	'Relatoriopsicologias',
);

$this->menu=array(
	array('label'=>'Create Relatoriopsicologia','url'=>array('create')),
	array('label'=>'Manage Relatoriopsicologia','url'=>array('admin')),
);
?>

<h1>Relatoriopsicologias</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
