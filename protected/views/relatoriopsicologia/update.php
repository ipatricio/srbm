<?php
$this->breadcrumbs=array(
	'Relatoriopsicologias'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Relatoriopsicologia','url'=>array('index')),
	array('label'=>'Create Relatoriopsicologia','url'=>array('create')),
	array('label'=>'View Relatoriopsicologia','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Relatoriopsicologia','url'=>array('admin')),
);
?>

<h1>Update Relatoriopsicologia <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>