<?php
$this->breadcrumbs=array(
	'Menus Acessos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MenusAcesso','url'=>array('index')),
	array('label'=>'Manage MenusAcesso','url'=>array('admin')),
);
?>

<h1>Create MenusAcesso</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>