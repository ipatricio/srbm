<?php 


$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'menus-acesso-form',
	'enableAjaxValidation'=>false,
));


 ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'menus_id', Menus::model()->listData(),
	array('class'=>'span5')); ?>


	<?php echo $form->dropDownListRow($model,'grupos_id', Grupos::model()->listData(),
			array('class'=>'span5')); ?>

	<?php echo $form->dropDownListRow($model,'menus_acoes_id', MenusAcoes::model()->listData(),
			array('class'=>'span5')); ?>
	<?php echo $form->checkBoxRow($model,'situacao', array('class'=>'span1')); ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
