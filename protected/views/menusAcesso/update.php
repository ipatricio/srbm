<?php
$this->breadcrumbs=array(
	'Menus Acessos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MenusAcesso','url'=>array('index')),
	array('label'=>'Create MenusAcesso','url'=>array('create')),
	array('label'=>'View MenusAcesso','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage MenusAcesso','url'=>array('admin')),
);
?>

<h1>Update MenusAcesso <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_formUp',array('model'=>$model)); ?>