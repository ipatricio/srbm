<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('menus_id')); ?>:</b>
	<?php echo CHtml::encode($data->menus->descricao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grupos_id')); ?>:</b>
	<?php echo CHtml::encode($data->grupos->descricao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('menus_acoes_id')); ?>:</b>
	<?php echo CHtml::encode($data->menusAcoes->descricao); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('situacao')); ?>:</b>
	<?php echo CHtml::encode($data->menusAcoes->situacao); ?>
	<br />


</div>