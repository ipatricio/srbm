<?php
$this->breadcrumbs=array(
	'Menus Acessos',
);

$this->menu=array(
	array('label'=>'Create MenusAcesso','url'=>array('create')),
	array('label'=>'Manage MenusAcesso','url'=>array('admin')),
);
?>

<h1>Menus Acessos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
