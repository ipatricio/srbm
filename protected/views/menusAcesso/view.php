<?php
$this->breadcrumbs=array(
	'Menus Acessos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MenusAcesso','url'=>array('index')),
	array('label'=>'Create MenusAcesso','url'=>array('create')),
	array('label'=>'Update MenusAcesso','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MenusAcesso','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MenusAcesso','url'=>array('admin')),
);
?>

<h1>View MenusAcesso #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		#'id',
		array('label'=>'Menus','value'=>$model->menus->descricao),
		array('label'=>'Grupos','value'=>$model->grupos->descricao),
		array('label'=>'Ação','value'=>$model->menusAcoes->descricao),
		array('label'=>'Situação','value'=> $model->situacao ? 'Ativo' : 'Inativo' ),

	),
)); ?>
