<?php
$this->breadcrumbs=array(
	'Procemedicinas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Procemedicina','url'=>array('index')),
	array('label'=>'Create Procemedicina','url'=>array('create')),
	array('label'=>'Update Procemedicina','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Procemedicina','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procemedicina','url'=>array('admin')),
);
?>

<h1>View Procemedicina #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tipo_proced',
	),
)); ?>
