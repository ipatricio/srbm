<?php
$this->breadcrumbs=array(
	'Procemedicinas',
);

$this->menu=array(
	array('label'=>'Create Procemedicina','url'=>array('create')),
	array('label'=>'Manage Procemedicina','url'=>array('admin')),
);
?>

<h1>Procemedicinas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
