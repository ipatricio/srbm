<?php
$this->breadcrumbs=array(
	'Procemedicinas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procemedicina','url'=>array('index')),
	array('label'=>'Create Procemedicina','url'=>array('create')),
	array('label'=>'View Procemedicina','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Procemedicina','url'=>array('admin')),
);
?>

<h1>Update Procemedicina <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>