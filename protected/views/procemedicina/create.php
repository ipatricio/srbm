<?php
$this->breadcrumbs=array(
	'Procemedicinas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Procemedicina','url'=>array('index')),
	array('label'=>'Manage Procemedicina','url'=>array('admin')),
);
?>

<h1>Create Procemedicina</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>