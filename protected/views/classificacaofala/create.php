<?php
$this->breadcrumbs=array(
	'Classificacaofalas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Classificacaofala','url'=>array('index')),
	array('label'=>'Manage Classificacaofala','url'=>array('admin')),
);
?>

<h1>Create Classificacaofala</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>