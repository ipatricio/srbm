<?php
$this->breadcrumbs=array(
	'Classificacaofalas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Classificacaofala','url'=>array('index')),
	array('label'=>'Create Classificacaofala','url'=>array('create')),
	array('label'=>'View Classificacaofala','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Classificacaofala','url'=>array('admin')),
);
?>

<h1>Update Classificacaofala <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>