<?php
$this->breadcrumbs=array(
	'Classificacaofalas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Classificacaofala','url'=>array('index')),
	array('label'=>'Create Classificacaofala','url'=>array('create')),
	array('label'=>'Update Classificacaofala','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Classificacaofala','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Classificacaofala','url'=>array('admin')),
);
?>

<h1>View Classificacaofala #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
