<?php
$this->breadcrumbs=array(
	'Vinculoprojetos',
);

$this->menu=array(
	array('label'=>'Create Vinculoprojeto','url'=>array('create')),
	array('label'=>'Manage Vinculoprojeto','url'=>array('admin')),
);
?>

<h1>Vinculoprojetos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
