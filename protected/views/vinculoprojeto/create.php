<?php
$this->breadcrumbs=array(
	'Vinculoprojetos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Vinculoprojeto','url'=>array('index')),
	array('label'=>'Manage Vinculoprojeto','url'=>array('admin')),
);
?>

<h1>Create Vinculoprojeto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>