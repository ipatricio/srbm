<?php
$this->breadcrumbs=array(
	'Vinculoprojetos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Vinculoprojeto','url'=>array('index')),
	array('label'=>'Create Vinculoprojeto','url'=>array('create')),
	array('label'=>'View Vinculoprojeto','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Vinculoprojeto','url'=>array('admin')),
);
?>

<h1>Update Vinculoprojeto <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>