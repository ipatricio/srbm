<?php
$this->breadcrumbs=array(
	'Vinculoprojetos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Vinculoprojeto','url'=>array('index')),
	array('label'=>'Create Vinculoprojeto','url'=>array('create')),
	array('label'=>'Update Vinculoprojeto','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Vinculoprojeto','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Vinculoprojeto','url'=>array('admin')),
);
?>

<h1>View Vinculoprojeto #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
