<?php
$this->breadcrumbs=array(
	'Pacientes',
);

$this->menu=array(
	array('label'=>'Criar Paciente','url'=>array('create')),
	array('label'=>'Gerenciar Paciente','url'=>array('admin')),
);
?>

<h1>Pacientes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
