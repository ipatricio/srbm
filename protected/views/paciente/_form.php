<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'paciente-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Campos com <span class="required">*</span> são requeridos.</p>

	<?php echo $form->errorSummary($model); ?>
     <div class="form-group col-lg-6">    
     <div class="span6">
		<?php echo $form->textFieldRow($model,'nome',array('class'=>'span12','maxlength'=>255)); ?>
     </div>
        
      <div class="span6">
        <?php echo $form->textFieldRow($model,'nome_pai',array('class'=>'span12','maxlength'=>255)); ?>
     </div>  
     </div>   
        
     <div class="form-group col-lg-6">     
        <div class="span6">
		<?php echo $form->textFieldRow($model,'nome_mae',array('class'=>' span12 ','maxlength'=>255)); ?>
        </div>
       
        <div class="span6">
	<?php echo $form->dropDownListRow($model,'sexo_id', Sexo::model()->listData(),array('class'=>'span10')); ?>
        </div>   
     </div>    
     
    <div class="form-group col-lg-6" >  
     <div class="span6">
	<?php echo $form->textFieldRow($model,'data_nascimento',array('class'=>'span8')); ?> 
     </div>
     
     <div class="span6">
	<?php echo $form->dropDownListRow($model,'estadoCivil_id', Estadocivil::model()->listData(),array('class'=>'span6')); ?>
     </div>   
    </div>
        
        
     <div class="form-group col-lg-6" >      
     <div class="span6">
	<?php echo $form->textFieldRow($model,'nome_conjuge',array('class'=>'span12','maxlength'=>255)); ?>
     </div>

     <div class="span6">
	<?php echo $form->textFieldRow($model,'ocupacao',array('class'=>'span10','maxlength'=>100)); ?>
     </div>
      </div>    
        
     <div class="form-group col-lg-6" >    
     <div class="span3">
	<?php echo $form->textFieldRow($model,'cpf',array('class'=>'span10','maxlength'=>25)); ?>
     </div>
      
     <div class="span3">
	<?php echo $form->textFieldRow($model,'identidade',array('class'=>'span10')); ?>
     </div>
        
     <div class="span3">
	<?php echo $form->textFieldRow($model,'orgao_emissor',array('class'=>'span10','maxlength'=>45)); ?>
     </div>
        
     <div class="span3">  
	<?php echo $form->textFieldRow($model,'data_emissao',array('class'=>'span12')); ?>
     </div>  
         
     </div>    
     
     <div class="form-group col-lg-6" >      
     <div class="span3">
	<?php echo $form->dropDownListRow($model,'nacionalidade_id', Nacionalidade::model()->listData(),array('class'=>'span11')); ?>
     </div>   
       
     <div class="span3">
	<?php echo $form->textFieldRow($model,'cep',array('class'=>'span10')); ?>
     </div>
        
     <div class="span1">   
	<?php echo $form->textFieldRow($model,'uf',array('class'=>'span12','maxlength'=>45)); ?>
     </div>
        
        <div class="span5">
	<?php echo $form->textFieldRow($model,'cidade',array('class'=>'span12','maxlength'=>45)); ?>
        </div>
     </div> 
     
     <div class="form-group col-lg-6" >     
        <div class="span6">
	<?php echo $form->textFieldRow($model,'cidade_nascimento',array('class'=>'span12','maxlength'=>100)); ?>
        </div>
    
        <div class="span6">
	<?php echo $form->textFieldRow($model,'bairro',array('class'=>'span12','maxlength'=>100)); ?>
        </div>
     </div>
        
      <div class="form-group col-lg-6">     
        <div class="span6">
	<?php echo $form->textFieldRow($model,'endereco',array('class'=>'span12','maxlength'=>150)); ?>
      </div>
        
	<div class="span2">
	<?php echo $form->textFieldRow($model,'numero',array('class'=>'span10')); ?>
        </div>
        
        <div class="span4">
	<?php echo $form->textFieldRow($model,'complemento',array('class'=>'span12','maxlength'=>100)); ?>
        </div>
      </div>
        
      <div class="form-group col-lg-6">      
        <div class="span6">
	<?php echo $form->textFieldRow($model,'email',array('class'=>'span12','maxlength'=>255)); ?>
        </div>
        
       <div class="span3">
	<?php echo $form->textFieldRow($model,'telefone',array('class'=>'span11','maxlength'=>25)); ?>
        </div>
        
        <div class="span3">
	<?php echo $form->textFieldRow($model,'celular',array('class'=>'span11','maxlength'=>25)); ?>
        </div>
      </div>
        
      <div class="form-group col-lg-4">  
        
        <div class="span3">
	<?php echo $form->textFieldRow($model,'telefone_trabalho',array('class'=>'span10','maxlength'=>25)); ?>
        </div>
        
        <div class="span4">
	<?php echo $form->textFieldRow($model,'N_carteira_plano_saude',array('class'=>'span12')); ?>
	</div>
        
        <div class="span3">
	<?php echo $form->textFieldRow($model,'data_validade_carteira',array('class'=>'span10')); ?>
        </div>
        
        <div class="span2">
	<?php echo $form->textFieldRow($model,'cns',array('class'=>'span12')); ?>
        </div>
      </div>
        
        <div class="form-group col-lg-6">  
        
        <div class="span3">
	<?php echo $form->textFieldRow($model,'cod_paciente',array('class'=>'span10','maxlength'=>45)); ?>
        </div>
        <div class="span4">
	<?php echo $form->textFieldRow($model,'N_prontuario_liga',array('class'=>'span10')); ?>
        </div>
        
        </div> 
            
        
        
	<div style="clear:both;"></div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Cadastrar' : 'Salvar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
