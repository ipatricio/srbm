<?php
$this->breadcrumbs=array(
	'Pacientes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar Paciente','url'=>array('index')),
	array('label'=>'Criar Paciente','url'=>array('create')),
	array('label'=>'Atualizar Paciente','url'=>array('update','id'=>$model->id)),
	array('label'=>'Deletar Paciente','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Gerenciar Paciente','url'=>array('admin')),
);
?>

<h1>Paciente <?php echo utf8_decode($model->nome); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		#'id',
		array('label'=>'Nome','value'=>utf8_decode($model->nome)),
		'nome_pai',
		'nome_mae',
		array('label'=>'Data nascimento','value'=>$model->data_nascimento),
		'data_nascimento',
		'nome_conjuge',
		'ocupacao',
		'cpf',
		'identidade',
		'orgao_emissor',
		'data_emissao',
		'uf',
		'cidade',
		'cidade_nascimento',
		'cep',
		'bairro',
		'endereco',
		'numero',
		'complemento',
		'email',
		'telefone',
		'celular',
		'telefone_trabalho',
		'N_carteira_plano_saude',
		'data_validade_carteira',
		'cns',
		'cod_paciente',
		'N_prontuario_liga',
		array('label'=>'Estado Civil','value'=>$model->estadoCivil->nome),
		array('label'=>'Sexo','value'=>$model->sexo->nome),
		array('label'=>'Nacionalidade','value'=>$model->nacionalidade->nome),
		
	),
)); ?>
