<?php
$this->breadcrumbs=array(
	'Procedimentoendodontias'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Procedimentoendodontia','url'=>array('index')),
	array('label'=>'Manage Procedimentoendodontia','url'=>array('admin')),
);
?>

<h1>Create Procedimentoendodontia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>