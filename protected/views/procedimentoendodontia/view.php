<?php
$this->breadcrumbs=array(
	'Procedimentoendodontias'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Procedimentoendodontia','url'=>array('index')),
	array('label'=>'Create Procedimentoendodontia','url'=>array('create')),
	array('label'=>'Update Procedimentoendodontia','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Procedimentoendodontia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procedimentoendodontia','url'=>array('admin')),
);
?>

<h1>View Procedimentoendodontia #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'paciente_id',
		'nome',
	),
)); ?>
