<?php
$this->breadcrumbs=array(
	'Procedimentoendodontias',
);

$this->menu=array(
	array('label'=>'Create Procedimentoendodontia','url'=>array('create')),
	array('label'=>'Manage Procedimentoendodontia','url'=>array('admin')),
);
?>

<h1>Procedimentoendodontias</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
