<?php
$this->breadcrumbs=array(
	'Procedimentoendodontias'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procedimentoendodontia','url'=>array('index')),
	array('label'=>'Create Procedimentoendodontia','url'=>array('create')),
	array('label'=>'View Procedimentoendodontia','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Procedimentoendodontia','url'=>array('admin')),
);
?>

<h1>Update Procedimentoendodontia <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>