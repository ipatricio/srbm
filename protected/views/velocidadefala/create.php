<?php
$this->breadcrumbs=array(
	'Velocidadefalas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Velocidadefala','url'=>array('index')),
	array('label'=>'Manage Velocidadefala','url'=>array('admin')),
);
?>

<h1>Create Velocidadefala</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>