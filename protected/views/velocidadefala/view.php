<?php
$this->breadcrumbs=array(
	'Velocidadefalas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Velocidadefala','url'=>array('index')),
	array('label'=>'Create Velocidadefala','url'=>array('create')),
	array('label'=>'Update Velocidadefala','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Velocidadefala','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Velocidadefala','url'=>array('admin')),
);
?>

<h1>View Velocidadefala #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
