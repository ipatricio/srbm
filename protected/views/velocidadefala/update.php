<?php
$this->breadcrumbs=array(
	'Velocidadefalas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Velocidadefala','url'=>array('index')),
	array('label'=>'Create Velocidadefala','url'=>array('create')),
	array('label'=>'View Velocidadefala','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Velocidadefala','url'=>array('admin')),
);
?>

<h1>Update Velocidadefala <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>