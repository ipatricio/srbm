<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'patologia_paciente',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'data_inicio',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'data_termino',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'dose_tratamento_complementar',array('class'=>'span5','maxlength'=>60)); ?>

	<?php echo $form->textFieldRow($model,'data',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'comentarios_adicionais',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'imgMedicina_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'paciente_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cidCancer_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'resposta_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
