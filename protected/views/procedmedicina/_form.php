<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'procedmedicina-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Campos com <span class="required">*</span> são obrigatórios.</p>

        <?php echo $form->textFieldRow($model,'paciente_id',array('class'=>'span5')); ?>
        <?php echo $form->textFieldRow($model,'cidCancer_id',array('class'=>'span5')); ?>   
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textAreaRow($model,'patologia_paciente',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
        
        <h2>Procedimentos realizados</h2>
        <?php echo $form->textFieldRow($model,'data',array('class'=>'span5')); ?>
	
        <h2>Tratamentos complementares</h2>
        <div class="form-group col-lg-6">
            <div class="span3">
        <?php echo $form->textFieldRow($model,'resposta_id',array('class'=>'span7')); ?>
            </div>
            <div class="span3">
        <?php echo $form->textFieldRow($model,'data_inicio',array('class'=>'span10')); ?>
            </div>
          <div class="span3">
	<?php echo $form->textFieldRow($model,'data_termino',array('class'=>'span10')); ?>
            </div>   
            <div class="span3">
        <?php echo $form->textFieldRow($model,'imgMedicina_id',array('class'=>'span12')); ?>
            </div>
              
        </div>    
        
        <div class="span-12"> 
	<?php echo $form->textAreaRow($model,'dose_tratamento_complementar',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
	<?php echo $form->textAreaRow($model,'comentarios_adicionais',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
        </div> 
	

	

	
        <div class="span12">
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Salvar' : 'Save',
		)); ?>
          <input type="button" class="btn btn-primary"  onclick="javascript: history.go(-1)" value="Voltar"> 


	</div>
        </div>

<?php $this->endWidget(); ?>
