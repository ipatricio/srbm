<?php
$this->breadcrumbs=array(
	'Procedmedicinas'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Procedmedicina','url'=>array('index')),
	array('label'=>'Create Procedmedicina','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('procedmedicina-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Procedmedicinas</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'procedmedicina-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'patologia_paciente',
		'data_inicio',
		'data_termino',
		'dose_tratamento_complementar',
		'data',
		/*
		'comentarios_adicionais',
		'imgMedicina_id',
		'paciente_id',
		'cidCancer_id',
		'resposta_id',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
