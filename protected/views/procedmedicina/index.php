<?php
$this->breadcrumbs=array(
	'Procedmedicinas',
);

$this->menu=array(
	array('label'=>'Create Procedmedicina','url'=>array('create')),
	array('label'=>'Manage Procedmedicina','url'=>array('admin')),
);
?>

<h1>Procedmedicinas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
