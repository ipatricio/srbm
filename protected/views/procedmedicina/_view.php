<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('patologia_paciente')); ?>:</b>
	<?php echo CHtml::encode($data->patologia_paciente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_inicio')); ?>:</b>
	<?php echo CHtml::encode($data->data_inicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_termino')); ?>:</b>
	<?php echo CHtml::encode($data->data_termino); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dose_tratamento_complementar')); ?>:</b>
	<?php echo CHtml::encode($data->dose_tratamento_complementar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data')); ?>:</b>
	<?php echo CHtml::encode($data->data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comentarios_adicionais')); ?>:</b>
	<?php echo CHtml::encode($data->comentarios_adicionais); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('imgMedicina_id')); ?>:</b>
	<?php echo CHtml::encode($data->imgMedicina_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paciente_id')); ?>:</b>
	<?php echo CHtml::encode($data->paciente_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cidCancer_id')); ?>:</b>
	<?php echo CHtml::encode($data->cidCancer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('resposta_id')); ?>:</b>
	<?php echo CHtml::encode($data->resposta_id); ?>
	<br />

	*/ ?>

</div>