<?php
$this->breadcrumbs=array(
	'Procedmedicinas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procedmedicina','url'=>array('index')),
	array('label'=>'Create Procedmedicina','url'=>array('create')),
	array('label'=>'View Procedmedicina','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Procedmedicina','url'=>array('admin')),
);
?>

<h1>Update Procedmedicina <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>