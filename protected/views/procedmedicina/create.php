<?php
$this->breadcrumbs=array(
	'Procedmedicinas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Procedimentos de medicina','url'=>array('index')),
	array('label'=>'Gerenciar Procedimentos medicina','url'=>array('admin')),
);
?>



<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>