<?php
$this->breadcrumbs=array(
	'Procedmedicinas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Procedmedicina','url'=>array('index')),
	array('label'=>'Create Procedmedicina','url'=>array('create')),
	array('label'=>'Update Procedmedicina','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Procedmedicina','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procedmedicina','url'=>array('admin')),
);
?>

<h1>View Procedmedicina #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'patologia_paciente',
		'data_inicio',
		'data_termino',
		'dose_tratamento_complementar',
		'data',
		'comentarios_adicionais',
		'imgMedicina_id',
		'paciente_id',
		'cidCancer_id',
		'resposta_id',
	),
)); ?>
