<?php
$this->breadcrumbs=array(
	'Areases'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Areas','url'=>array('index')),
	array('label'=>'Create Areas','url'=>array('create')),
	array('label'=>'Update Areas','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Areas','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Areas','url'=>array('admin')),
);
?>

<h1>View Areas #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
