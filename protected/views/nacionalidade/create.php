<?php
$this->breadcrumbs=array(
	'Nacionalidades'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Nacionalidade','url'=>array('index')),
	array('label'=>'Manage Nacionalidade','url'=>array('admin')),
);
?>

<h1>Create Nacionalidade</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>