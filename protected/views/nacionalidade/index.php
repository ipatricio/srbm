<?php
$this->breadcrumbs=array(
	'Nacionalidades',
);

$this->menu=array(
	array('label'=>'Create Nacionalidade','url'=>array('create')),
	array('label'=>'Manage Nacionalidade','url'=>array('admin')),
);
?>

<h1>Nacionalidades</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
