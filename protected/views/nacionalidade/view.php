<?php
$this->breadcrumbs=array(
	'Nacionalidades'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Nacionalidade','url'=>array('index')),
	array('label'=>'Create Nacionalidade','url'=>array('create')),
	array('label'=>'Update Nacionalidade','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Nacionalidade','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Nacionalidade','url'=>array('admin')),
);
?>

<h1>View Nacionalidade #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
