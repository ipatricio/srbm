<?php
$this->breadcrumbs=array(
	'Nacionalidades'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Nacionalidade','url'=>array('index')),
	array('label'=>'Create Nacionalidade','url'=>array('create')),
	array('label'=>'View Nacionalidade','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Nacionalidade','url'=>array('admin')),
);
?>

<h1>Update Nacionalidade <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>