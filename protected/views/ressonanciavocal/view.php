<?php
$this->breadcrumbs=array(
	'Ressonanciavocals'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ressonanciavocal','url'=>array('index')),
	array('label'=>'Create Ressonanciavocal','url'=>array('create')),
	array('label'=>'Update Ressonanciavocal','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Ressonanciavocal','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ressonanciavocal','url'=>array('admin')),
);
?>

<h1>View Ressonanciavocal #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
