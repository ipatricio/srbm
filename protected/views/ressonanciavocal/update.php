<?php
$this->breadcrumbs=array(
	'Ressonanciavocals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ressonanciavocal','url'=>array('index')),
	array('label'=>'Create Ressonanciavocal','url'=>array('create')),
	array('label'=>'View Ressonanciavocal','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Ressonanciavocal','url'=>array('admin')),
);
?>

<h1>Update Ressonanciavocal <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>