<?php
$this->breadcrumbs=array(
	'Ressonanciavocals',
);

$this->menu=array(
	array('label'=>'Create Ressonanciavocal','url'=>array('create')),
	array('label'=>'Manage Ressonanciavocal','url'=>array('admin')),
);
?>

<h1>Ressonanciavocals</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
