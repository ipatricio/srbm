<?php
$this->breadcrumbs=array(
	'Ressonanciavocals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ressonanciavocal','url'=>array('index')),
	array('label'=>'Manage Ressonanciavocal','url'=>array('admin')),
);
?>

<h1>Create Ressonanciavocal</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>