<?php
$this->breadcrumbs=array(
	'Coordenacaofonoarticulars'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Coordenacaofonoarticular','url'=>array('index')),
	array('label'=>'Manage Coordenacaofonoarticular','url'=>array('admin')),
);
?>

<h1>Create Coordenacaofonoarticular</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>