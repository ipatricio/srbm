<?php
$this->breadcrumbs=array(
	'Coordenacaofonoarticulars'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Coordenacaofonoarticular','url'=>array('index')),
	array('label'=>'Create Coordenacaofonoarticular','url'=>array('create')),
	array('label'=>'Update Coordenacaofonoarticular','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Coordenacaofonoarticular','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Coordenacaofonoarticular','url'=>array('admin')),
);
?>

<h1>View Coordenacaofonoarticular #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
