<?php
$this->breadcrumbs=array(
	'Coordenacaofonoarticulars'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Coordenacaofonoarticular','url'=>array('index')),
	array('label'=>'Create Coordenacaofonoarticular','url'=>array('create')),
	array('label'=>'View Coordenacaofonoarticular','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Coordenacaofonoarticular','url'=>array('admin')),
);
?>

<h1>Update Coordenacaofonoarticular <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>