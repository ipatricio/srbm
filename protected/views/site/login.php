<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
/*
$this->breadcrumbs=array(
	'Login',
);*/
?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	 'htmlOptions' => array('class' => 'well'),
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->textFieldGroup($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->passwordFieldGroup($model,'password'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkboxGroup($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php $this->widget(
			'booster.widgets.TbButton',
			array('buttonType' => 'submit', 'label' => 'Login')
			);
		?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
