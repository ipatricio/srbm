<?php
$this->breadcrumbs=array(
	'Procedimentoproteses',
);

$this->menu=array(
	array('label'=>'Create Procedimentoprotese','url'=>array('create')),
	array('label'=>'Manage Procedimentoprotese','url'=>array('admin')),
);
?>

<h1>Procedimentoproteses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
