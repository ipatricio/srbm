<?php
$this->breadcrumbs=array(
	'Procedimentoproteses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procedimentoprotese','url'=>array('index')),
	array('label'=>'Create Procedimentoprotese','url'=>array('create')),
	array('label'=>'View Procedimentoprotese','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Procedimentoprotese','url'=>array('admin')),
);
?>

<h1>Update Procedimentoprotese <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>