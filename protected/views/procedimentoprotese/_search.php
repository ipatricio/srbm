<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'protese_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'material_enviado_laboratorio',array('class'=>'span5','maxlength'=>3)); ?>

	<?php echo $form->textFieldRow($model,'nome_laboratorio',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textAreaRow($model,'descricao_material_enviado',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'tipo_imagem',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'escala_cor',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cor',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textAreaRow($model,'comentario',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'solicit_trabalho',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'paciente_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'imgmedicina_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'data_procedimento',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'fabricacao_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
