<?php
$this->breadcrumbs=array(
	'Procedimentoproteses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Procedimentoprotese','url'=>array('index')),
	array('label'=>'Create Procedimentoprotese','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('procedimentoprotese-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Procedimentoproteses</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'procedimentoprotese-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'protese_id',
		'material_enviado_laboratorio',
		'nome_laboratorio',
		'descricao_material_enviado',
		'tipo_imagem',
		/*
		'escala_cor',
		'cor',
		'comentario',
		'solicit_trabalho',
		'paciente_id',
		'imgmedicina_id',
		'data_procedimento',
		'fabricacao_id',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
