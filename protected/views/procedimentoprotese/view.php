<?php
$this->breadcrumbs=array(
	'Procedimentoproteses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Procedimentoprotese','url'=>array('index')),
	array('label'=>'Create Procedimentoprotese','url'=>array('create')),
	array('label'=>'Update Procedimentoprotese','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Procedimentoprotese','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procedimentoprotese','url'=>array('admin')),
);
?>

<h1>View Procedimentoprotese #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'protese_id',
		'material_enviado_laboratorio',
		'nome_laboratorio',
		'descricao_material_enviado',
		'tipo_imagem',
		'escala_cor',
		'cor',
		'comentario',
		'solicit_trabalho',
		'paciente_id',
		'imgmedicina_id',
		'data_procedimento',
		'fabricacao_id',
	),
)); ?>
