<?php
$this->breadcrumbs=array(
	'Procedimentoproteses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Procedimentos de prótese','url'=>array('index')),
	array('label'=>'Gerenciar Procedimento prótese','url'=>array('admin')),
);
?>



<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>