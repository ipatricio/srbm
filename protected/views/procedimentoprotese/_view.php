<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('protese_id')); ?>:</b>
	<?php echo CHtml::encode($data->protese_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('material_enviado_laboratorio')); ?>:</b>
	<?php echo CHtml::encode($data->material_enviado_laboratorio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_laboratorio')); ?>:</b>
	<?php echo CHtml::encode($data->nome_laboratorio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descricao_material_enviado')); ?>:</b>
	<?php echo CHtml::encode($data->descricao_material_enviado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_imagem')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_imagem); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('escala_cor')); ?>:</b>
	<?php echo CHtml::encode($data->escala_cor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cor')); ?>:</b>
	<?php echo CHtml::encode($data->cor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comentario')); ?>:</b>
	<?php echo CHtml::encode($data->comentario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('solicit_trabalho')); ?>:</b>
	<?php echo CHtml::encode($data->solicit_trabalho); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paciente_id')); ?>:</b>
	<?php echo CHtml::encode($data->paciente_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imgmedicina_id')); ?>:</b>
	<?php echo CHtml::encode($data->imgmedicina_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_procedimento')); ?>:</b>
	<?php echo CHtml::encode($data->data_procedimento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fabricacao_id')); ?>:</b>
	<?php echo CHtml::encode($data->fabricacao_id); ?>
	<br />

	*/ ?>

</div>