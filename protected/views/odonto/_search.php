<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php #echo $form->textFieldRow($model,'id',array('class'=>'span1')); ?>

	<?php echo $form->textFieldRow($model,'nome',array('class'=>'span3','maxlength'=>255)); ?>

	<?php #echo $form->textFieldRow($model,'nome_pai',array('class'=>'span5','maxlength'=>255)); ?>

	<?php #echo $form->textFieldRow($model,'nome_mae',array('class'=>'span5','maxlength'=>255)); ?>

	<?php #echo $form->textFieldRow($model,'data_nascimento',array('class'=>'span5')); ?>

	<?php #echo $form->textFieldRow($model,'nome_conjuge',array('class'=>'span5','maxlength'=>255)); ?>

	<?php #echo $form->textFieldRow($model,'ocupacao',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'cpf',array('class'=>'span3','maxlength'=>25)); ?>

	<?php #echo $form->textFieldRow($model,'identidade',array('class'=>'span5')); ?>

	<?php #echo $form->textFieldRow($model,'orgao_emissor',array('class'=>'span5','maxlength'=>45)); ?>

	<?php #echo $form->textFieldRow($model,'data_emissao',array('class'=>'span5')); ?>

	<?php #echo $form->textFieldRow($model,'uf',array('class'=>'span5','maxlength'=>45)); ?>

	<?php #echo $form->textFieldRow($model,'cidade',array('class'=>'span5','maxlength'=>45)); ?>

	<?php #echo $form->textFieldRow($model,'cidade_nascimento',array('class'=>'span5','maxlength'=>100)); ?>

	<?php #echo $form->textFieldRow($model,'cep',array('class'=>'span5')); ?>

	<?php #echo $form->textFieldRow($model,'bairro',array('class'=>'span5','maxlength'=>100)); ?>

	<?php #echo $form->textFieldRow($model,'endereco',array('class'=>'span5','maxlength'=>150)); ?>

	<?php #echo $form->textFieldRow($model,'numero',array('class'=>'span5')); ?>

	<?php #echo $form->textFieldRow($model,'complemento',array('class'=>'span5','maxlength'=>100)); ?>

	<?php #echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255)); ?>

	<?php #echo $form->textFieldRow($model,'telefone',array('class'=>'span5','maxlength'=>25)); ?>

	<?php #echo $form->textFieldRow($model,'celular',array('class'=>'span5','maxlength'=>25)); ?>

	<?php #echo $form->textFieldRow($model,'telefone_trabalho',array('class'=>'span5','maxlength'=>25)); ?>

	<?php #echo $form->textFieldRow($model,'N_carteira_plano_saude',array('class'=>'span3')); ?>

	<?php #echo $form->textFieldRow($model,'data_validade_carteira',array('class'=>'span5')); ?>

	<?php #echo $form->textFieldRow($model,'cns',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cod_paciente',array('class'=>'span3','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'N_prontuario_liga',array('class'=>'span3')); ?>

	<?php #echo $form->textFieldRow($model,'estadoCivil_id',array('class'=>'span5')); ?>

	<?php #echo $form->textFieldRow($model,'sexo_id',array('class'=>'span5')); ?>

	<?php #echo $form->textFieldRow($model,'nacionalidade_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Filtar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
