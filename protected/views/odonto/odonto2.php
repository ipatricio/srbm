<?php
$this->breadcrumbs=array(
	'odontologia'=>array('index'),
	'Gerenciar',
);

$this->menu=array(
	array('label'=>'Listar Paciente','url'=>array('index')),
	array('label'=>'Criar Paciente','url'=>array('create')),
);

?>


    <a  href="<?php echo Yii::app()->baseUrl.'/relatorioodontologia/admin'.'/'.$model->id ?>">Cadastro Específico de odontologia</a> 
    <br><br>
     <a  href="<?php echo 'anexos/admin'.'?id='.$model->id  ?>">Anexos de odontologia</a>
    <br><br>
    <a  href="<?php echo  Yii::app()->baseUrl.'/procedimentoodontologia/admin/'.$model->id ?>">Procedimento de odontologia</a>
