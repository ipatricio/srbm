<?php
$this->breadcrumbs=array(
	'Mapa Venda Recebimentos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MapaVendaRecebimento','url'=>array('index')),
	array('label'=>'Manage MapaVendaRecebimento','url'=>array('admin')),
);
?>

<h1>Create MapaVendaRecebimento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>