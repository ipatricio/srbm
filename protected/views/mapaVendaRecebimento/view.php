<?php
$this->breadcrumbs=array(
	'Mapa Venda Recebimentos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MapaVendaRecebimento','url'=>array('index')),
	array('label'=>'Create MapaVendaRecebimento','url'=>array('create')),
	array('label'=>'Update MapaVendaRecebimento','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MapaVendaRecebimento','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MapaVendaRecebimento','url'=>array('admin')),
);
?>

<h1>View MapaVendaRecebimento #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'data',
		'vidas',
		'contrato',
		'cod_plano',
		'id_cliente',
		'id_vendedor',
		'valor',
		'pri_rest',
		'data_pri_rest',
		'pri_conf',
		'data_pri_conf',
		'seg_conf',
		'data_seg_conf',
	),
)); ?>
