<?php
$this->breadcrumbs=array(
	'Mapa Venda Recebimentos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MapaVendaRecebimento','url'=>array('index')),
	array('label'=>'Create MapaVendaRecebimento','url'=>array('create')),
	array('label'=>'View MapaVendaRecebimento','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage MapaVendaRecebimento','url'=>array('admin')),
);
?>

<h1>Update MapaVendaRecebimento <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>