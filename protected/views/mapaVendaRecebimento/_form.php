<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'mapa-venda-recebimento-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'data',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'vidas',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'contrato',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cod_plano',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_cliente',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_vendedor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'valor',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pri_rest',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'data_pri_rest',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pri_conf',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'data_pri_conf',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'seg_conf',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'data_seg_conf',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
