<?php
$this->breadcrumbs=array(
	'Mapa Venda Recebimentos',
);

$this->menu=array(
	array('label'=>'Create MapaVendaRecebimento','url'=>array('create')),
	array('label'=>'Manage MapaVendaRecebimento','url'=>array('admin')),
);
?>

<h1>Mapa Venda Recebimentos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
