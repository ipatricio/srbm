<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data')); ?>:</b>
	<?php echo CHtml::encode($data->data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vidas')); ?>:</b>
	<?php echo CHtml::encode($data->vidas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contrato')); ?>:</b>
	<?php echo CHtml::encode($data->contrato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cod_plano')); ?>:</b>
	<?php echo CHtml::encode($data->cod_plano); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cliente')); ?>:</b>
	<?php echo CHtml::encode($data->id_cliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_vendedor')); ?>:</b>
	<?php echo CHtml::encode($data->id_vendedor); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('valor')); ?>:</b>
	<?php echo CHtml::encode($data->valor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pri_rest')); ?>:</b>
	<?php echo CHtml::encode($data->pri_rest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_pri_rest')); ?>:</b>
	<?php echo CHtml::encode($data->data_pri_rest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pri_conf')); ?>:</b>
	<?php echo CHtml::encode($data->pri_conf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_pri_conf')); ?>:</b>
	<?php echo CHtml::encode($data->data_pri_conf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seg_conf')); ?>:</b>
	<?php echo CHtml::encode($data->seg_conf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_seg_conf')); ?>:</b>
	<?php echo CHtml::encode($data->data_seg_conf); ?>
	<br />

	*/ ?>

</div>