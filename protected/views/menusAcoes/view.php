<?php
$this->breadcrumbs=array(
	'Menus Acoes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MenusAcoes','url'=>array('index')),
	array('label'=>'Create MenusAcoes','url'=>array('create')),
	array('label'=>'Update MenusAcoes','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MenusAcoes','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MenusAcoes','url'=>array('admin')),
);
?>

<h1>View MenusAcoes #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descricao',
		'valor',
	),
)); ?>
