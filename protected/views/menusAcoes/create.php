<?php
$this->breadcrumbs=array(
	'Menus Acoes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MenusAcoes','url'=>array('index')),
	array('label'=>'Manage MenusAcoes','url'=>array('admin')),
);
?>

<h1>Create MenusAcoes</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>