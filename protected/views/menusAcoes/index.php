<?php
$this->breadcrumbs=array(
	'Menus Acoes',
);

$this->menu=array(
	array('label'=>'Create MenusAcoes','url'=>array('create')),
	array('label'=>'Manage MenusAcoes','url'=>array('admin')),
);
?>

<h1>Menus Acoes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
