<?php
$this->breadcrumbs=array(
	'Menus Acoes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MenusAcoes','url'=>array('index')),
	array('label'=>'Create MenusAcoes','url'=>array('create')),
	array('label'=>'View MenusAcoes','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage MenusAcoes','url'=>array('admin')),
);
?>

<h1>Update MenusAcoes <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>