<?php
$this->breadcrumbs = array(
    'Anexoses' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List Anexos', 'url' => array('index')),
    array('label' => 'Create Anexos', 'url' => array('create')),
    array('label' => 'Update Anexos', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Anexos', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Anexos', 'url' => array('admin')),
);
?>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        #	'id',
        array('label' => 'Imagem',
            'value' => CHtml::link(CHtml::encode($model->documento), array('view', 'id'=>$model->documento))),
        'comentario',
        array('label' => 'Área', 'value' => $model->areas->nome),
        array('label' => 'Paciente', 'value' => $model->paciente->nome),
    ),
));
?>
