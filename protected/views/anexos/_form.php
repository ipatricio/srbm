<?php
if (isset($_SESSION['msg']['tipo'])) {
    $user = Yii::app()->getComponent('user');
    $user->setFlash($_SESSION['msg']['tipo'], $_SESSION['msg']['descricao']);

    unset($_SESSION['msg']);

}
// Render them all with single `TbAlert`
$this->widget('bootstrap.widgets.TbAlert', array(
    'fade' => true,
    'closeText' => '&times;', // false equals no close link

));

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'anexos-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->fileFieldRow($model, 'documento', array('class' => 'span8', 'maxlength' => 45)); ?>

<?php echo $form->textFieldRow($model, 'comentario', array('class' => 'span8', 'maxlength' => 45)); ?>


<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
