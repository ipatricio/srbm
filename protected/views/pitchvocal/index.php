<?php
$this->breadcrumbs=array(
	'Pitchvocals',
);

$this->menu=array(
	array('label'=>'Create Pitchvocal','url'=>array('create')),
	array('label'=>'Manage Pitchvocal','url'=>array('admin')),
);
?>

<h1>Pitchvocals</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
