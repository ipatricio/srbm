<?php
$this->breadcrumbs=array(
	'Pitchvocals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Pitchvocal','url'=>array('index')),
	array('label'=>'Manage Pitchvocal','url'=>array('admin')),
);
?>

<h1>Create Pitchvocal</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>