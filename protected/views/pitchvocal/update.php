<?php
$this->breadcrumbs=array(
	'Pitchvocals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Pitchvocal','url'=>array('index')),
	array('label'=>'Create Pitchvocal','url'=>array('create')),
	array('label'=>'View Pitchvocal','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Pitchvocal','url'=>array('admin')),
);
?>

<h1>Update Pitchvocal <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>