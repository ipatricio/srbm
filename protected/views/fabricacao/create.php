<?php
$this->breadcrumbs=array(
	'Fabricacaos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Fabricacao','url'=>array('index')),
	array('label'=>'Manage Fabricacao','url'=>array('admin')),
);
?>

<h1>Create Fabricacao</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>