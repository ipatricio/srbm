<?php
$this->breadcrumbs=array(
	'Fabricacaos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Fabricacao','url'=>array('index')),
	array('label'=>'Create Fabricacao','url'=>array('create')),
	array('label'=>'Update Fabricacao','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Fabricacao','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Fabricacao','url'=>array('admin')),
);
?>

<h1>View Fabricacao #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
