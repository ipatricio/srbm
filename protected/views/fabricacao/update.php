<?php
$this->breadcrumbs=array(
	'Fabricacaos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Fabricacao','url'=>array('index')),
	array('label'=>'Create Fabricacao','url'=>array('create')),
	array('label'=>'View Fabricacao','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Fabricacao','url'=>array('admin')),
);
?>

<h1>Update Fabricacao <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>