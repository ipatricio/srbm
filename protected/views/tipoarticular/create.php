<?php
$this->breadcrumbs=array(
	'Tipoarticulars'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tipoarticular','url'=>array('index')),
	array('label'=>'Manage Tipoarticular','url'=>array('admin')),
);
?>

<h1>Create Tipoarticular</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>