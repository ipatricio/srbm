<?php
$this->breadcrumbs=array(
	'Tipoarticulars'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Tipoarticular','url'=>array('index')),
	array('label'=>'Create Tipoarticular','url'=>array('create')),
	array('label'=>'Update Tipoarticular','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Tipoarticular','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tipoarticular','url'=>array('admin')),
);
?>

<h1>View Tipoarticular #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
