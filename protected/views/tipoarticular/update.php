<?php
$this->breadcrumbs=array(
	'Tipoarticulars'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tipoarticular','url'=>array('index')),
	array('label'=>'Create Tipoarticular','url'=>array('create')),
	array('label'=>'View Tipoarticular','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Tipoarticular','url'=>array('admin')),
);
?>

<h1>Update Tipoarticular <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>