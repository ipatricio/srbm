<?php
$this->breadcrumbs=array(
	'Tipoarticulars',
);

$this->menu=array(
	array('label'=>'Create Tipoarticular','url'=>array('create')),
	array('label'=>'Manage Tipoarticular','url'=>array('admin')),
);
?>

<h1>Tipoarticulars</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
