<?php
$this->breadcrumbs=array(
	'Classificacaofonoarticulars'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Classificacaofonoarticular','url'=>array('index')),
	array('label'=>'Create Classificacaofonoarticular','url'=>array('create')),
	array('label'=>'Update Classificacaofonoarticular','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Classificacaofonoarticular','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Classificacaofonoarticular','url'=>array('admin')),
);
?>

<h1>View Classificacaofonoarticular #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
