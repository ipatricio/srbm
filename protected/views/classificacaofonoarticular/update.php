<?php
$this->breadcrumbs=array(
	'Classificacaofonoarticulars'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Classificacaofonoarticular','url'=>array('index')),
	array('label'=>'Create Classificacaofonoarticular','url'=>array('create')),
	array('label'=>'View Classificacaofonoarticular','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Classificacaofonoarticular','url'=>array('admin')),
);
?>

<h1>Update Classificacaofonoarticular <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>