<?php
$this->breadcrumbs=array(
	'Classificacaofonoarticulars'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Classificacaofonoarticular','url'=>array('index')),
	array('label'=>'Manage Classificacaofonoarticular','url'=>array('admin')),
);
?>

<h1>Create Classificacaofonoarticular</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>