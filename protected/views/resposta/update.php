<?php
$this->breadcrumbs=array(
	'Respostas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Resposta','url'=>array('index')),
	array('label'=>'Create Resposta','url'=>array('create')),
	array('label'=>'View Resposta','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Resposta','url'=>array('admin')),
);
?>

<h1>Update Resposta <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>