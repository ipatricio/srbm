<?php
$this->breadcrumbs=array(
	'Proteses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Protese','url'=>array('index')),
	array('label'=>'Create Protese','url'=>array('create')),
	array('label'=>'Update Protese','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Protese','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Protese','url'=>array('admin')),
);
?>

<h1>View Protese #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
