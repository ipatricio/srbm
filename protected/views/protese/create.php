<?php
$this->breadcrumbs=array(
	'Proteses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Protese','url'=>array('index')),
	array('label'=>'Manage Protese','url'=>array('admin')),
);
?>

<h1>Create Protese</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>