<?php
$this->breadcrumbs=array(
	'Proteses',
);

$this->menu=array(
	array('label'=>'Create Protese','url'=>array('create')),
	array('label'=>'Manage Protese','url'=>array('admin')),
);
?>

<h1>Proteses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
