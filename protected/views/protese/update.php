<?php
$this->breadcrumbs=array(
	'Proteses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Protese','url'=>array('index')),
	array('label'=>'Create Protese','url'=>array('create')),
	array('label'=>'View Protese','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Protese','url'=>array('admin')),
);
?>

<h1>Update Protese <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>