<?php
$this->breadcrumbs=array(
	'Estoques'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Estoque','url'=>array('index')),
	array('label'=>'Create Estoque','url'=>array('create')),
	array('label'=>'View Estoque','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Estoque','url'=>array('admin')),
);
?>

<h1>Update Estoque <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>