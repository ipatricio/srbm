<?php
$this->breadcrumbs=array(
	'Estoques'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Estoque','url'=>array('index')),
	array('label'=>'Create Estoque','url'=>array('create')),
	array('label'=>'Update Estoque','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Estoque','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Estoque','url'=>array('admin')),
);
?>

<h1>View Estoque #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
	),
)); ?>
