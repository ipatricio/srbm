<?php
$this->breadcrumbs=array(
	'Estoques'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Estoque','url'=>array('index')),
	array('label'=>'Manage Estoque','url'=>array('admin')),
);
?>

<h1>Create Estoque</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>