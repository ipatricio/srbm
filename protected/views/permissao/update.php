<?php
$this->breadcrumbs=array(
	'Permissaos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Permissao','url'=>array('index')),
	array('label'=>'Create Permissao','url'=>array('create')),
	array('label'=>'View Permissao','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Permissao','url'=>array('admin')),
);
?>

<h1>Update Permissao <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>