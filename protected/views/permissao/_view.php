<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('model_id')); ?>:</b>
	<?php echo CHtml::encode($data->model_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grupo_id')); ?>:</b>
	<?php echo CHtml::encode($data->grupo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('criar')); ?>:</b>
	<?php echo CHtml::encode($data->criar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ler')); ?>:</b>
	<?php echo CHtml::encode($data->ler); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('atualizar')); ?>:</b>
	<?php echo CHtml::encode($data->atualizar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deletar')); ?>:</b>
	<?php echo CHtml::encode($data->deletar); ?>
	<br />


</div>