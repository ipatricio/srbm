<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'permissao-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'model_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'grupo_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'criar',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ler',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'atualizar',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'deletar',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
