<?php
$this->breadcrumbs=array(
	'Permissaos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Permissao','url'=>array('index')),
	array('label'=>'Create Permissao','url'=>array('create')),
	array('label'=>'Update Permissao','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Permissao','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Permissao','url'=>array('admin')),
);
?>

<h1>View Permissao #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'model_id',
		'grupo_id',
		'criar',
		'ler',
		'atualizar',
		'deletar',
	),
)); ?>
