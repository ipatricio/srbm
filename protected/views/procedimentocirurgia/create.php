<?php
$this->breadcrumbs=array(
	'Procedimentocirurgias'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Procedimentocirurgia','url'=>array('index')),
	array('label'=>'Manage Procedimentocirurgia','url'=>array('admin')),
);
?>

<h1>Create Procedimentocirurgia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>