<?php
$this->breadcrumbs=array(
	'Procedimentocirurgias',
);

$this->menu=array(
	array('label'=>'Create Procedimentocirurgia','url'=>array('create')),
	array('label'=>'Manage Procedimentocirurgia','url'=>array('admin')),
);
?>

<h1>Procedimentocirurgias</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
