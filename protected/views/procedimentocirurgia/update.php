<?php
$this->breadcrumbs=array(
	'Procedimentocirurgias'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procedimentocirurgia','url'=>array('index')),
	array('label'=>'Create Procedimentocirurgia','url'=>array('create')),
	array('label'=>'View Procedimentocirurgia','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Procedimentocirurgia','url'=>array('admin')),
);
?>

<h1>Update Procedimentocirurgia <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>