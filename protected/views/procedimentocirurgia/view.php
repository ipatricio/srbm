<?php
$this->breadcrumbs=array(
	'Procedimentocirurgias'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Procedimentocirurgia','url'=>array('index')),
	array('label'=>'Create Procedimentocirurgia','url'=>array('create')),
	array('label'=>'Update Procedimentocirurgia','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Procedimentocirurgia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procedimentocirurgia','url'=>array('admin')),
);
?>

<h1>View Procedimentocirurgia #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'paciente_id',
	),
)); ?>
