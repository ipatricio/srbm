<?php
$this->breadcrumbs=array(
	'Procedimentofonoaudiologias'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procedimentofonoaudiologia','url'=>array('index')),
	array('label'=>'Create Procedimentofonoaudiologia','url'=>array('create')),
	array('label'=>'View Procedimentofonoaudiologia','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Procedimentofonoaudiologia','url'=>array('admin')),
);
?>

<h1>Update Procedimentofonoaudiologia <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>