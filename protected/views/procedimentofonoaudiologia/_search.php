<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'data_proced',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'tmf',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'obs',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'result_exames',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textAreaRow($model,'outros',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'paciente_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'velocidadeFala_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'loudess_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pitchVocal_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'hiperHiponasalidade_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'coordenacaoFonoarticular_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'TipoArticular_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'NivelComprometimento_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'classificacaoFala_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ressonanciaVocal_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'classificacaoFonoarticular_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'proceMedicina_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
