<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'procedimentofonoaudiologia-form',
	'enableAjaxValidation'=>false,
)); ?>


	<p class="help-block">Campos com <span class="required">*</span> são obrigatórios.</p>

	<?php echo $form->errorSummary($model); ?>
	<p>Data Procedimento<span class="required">*</span></p>
	<?php 

		$form->widget('zii.widgets.jui.CJuiDatePicker', array(
    'model' => $model,
    'attribute' => 'data_proced',
    #'flat'=>true,
    'language' => 'pt',
    // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js',
    'htmlOptions' => array(
        'size' => '10',
        'maxlength' => '10',
    ),	
	));
	?>
<?php echo $form->textFieldRow($model,'paciente_id',
	array('class'=>'span5','value'=>Paciente::model()->findByPk($_GET['id'])->nome,'disabled'=>true)); ?>

 <h4> 1.	Avaliação do quadro fonêmico.</h4>
    <h4>•	Solicitar a repetição de silabas acompanhadas da vogal /a/</h4>
    <h4>•	Avaliar a presença de troca, omissões, distorções articulatórias.</h4>
    <h5>/pa/                      /fa/                      /ma/</h5>
    <h5>/ta/                       /sa/                     /na/</h5>                                        
    <h5>/ka/                      /cha/                   /nha/ </h5>  
    <h5>/ba/                      /va/                     /ra/ </h5>
    <h5>/da/                      /za/                     /la/ </h5>
    <h5>/ga/                      /ja/                      /lha/</h5>
    <h5>grupo consonantal /pra/     /pla/  </h5>

    <h4>2.	Avaliação da fala espontânea.</h4>
    <h4>•	Propor um tema: como contar sobre sua família, sobre seu trabalho.</h4>
    <h4>•	Comentar a respeito de uma figura padronizada.</h4>
    
 <div class="form-group col-lg-6">
    <div class="span8"> 
	<?php echo $form->dropDownListRow($model,'classificacaoFala_id', Classificacaofala::model()->listData(),array('class'=>'span12')); ?>
    </div>
        
    <div class="span4">
	<?php echo $form->dropDownListRow($model,'classificacaoFonoarticular_id', Classificacaofonoarticular::model()->listData(),array('class'=>'span8')); ?>
    </div>
        
 </div>    
        
<div class="form form-group col-lg-6">

    <div class="span3">
    <?php echo $form->dropDownListRow($model,'TipoArticular_id', Tipoarticular::model()->listData(),array('class'=>'span12')); ?>
    </div>
    
    <div class="span6">    
	<?php echo $form->dropDownListRow($model,'coordenacaoFonoarticular_id', Coordenacaofonoarticular::model()->listData(),array('class'=>'span8')); ?>
    </div>
    
    <div class="span3">    
        <?php echo $form->dropDownListRow($model,'velocidadeFala_id', Velocidadefala::model()->listData(),array('class'=>'span12')); ?>
    </div>
    
</div>    

<h4>3. Triagem vocal</h4>
<h4>• Solicitar a emissão sustentada da vogal /a/.</h4>

<div class="form-group col-lg-6">
    
<div class="span8" > 	
	<?php echo $form->dropDownListRow($model,'NivelComprometimento_id', Nivelcomprometimento::model()->listData(),array('class'=>'span8')); ?>
</div>

<div class="span4"> 	
	<?php echo $form->dropDownListRow($model,'ressonanciaVocal_id', Ressonanciavocal::model()->listData(),array('class'=>'span10')); ?>
</div>
</div>

<div class="form-group col-lg-6">
    <div class="span4">    
        <?php echo $form->dropDownListRow($model,'pitchVocal_id', Pitchvocal::model()->listData(),array('class'=>'span8')); ?>
    </div>

    <div class="span4">    
        <?php echo $form->dropDownListRow($model,'loudess_id', Loudess::model()->listData(),array('class'=>'span8')); ?>
    </div>    
</div> 


<div class='clear'></div>

	<?php echo $form->textAreaRow($model,'tmf',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
<h4>4.	Ressonância da fala</h4>
 <h4>•	Contagem  de 1 ate 20;</h4>
 <h4>•	Repetição de silabas /pi/, /ti/, /ki/, /fi/, /si/, /chi/</h4>
<?php echo $form->dropDownListRow($model,'hiperHiponasalidade_id', Hiperhiponasalidade::model()->listData(),array('class'=>'span3')); ?>
	<?php echo $form->textAreaRow($model,'obs',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'result_exames',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'outros',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	

	

	







	<?php #echo $form->textFieldRow($model,'proceMedicina_id',array('class'=>'span8')); ?>
	<?php #echo $form->dropDownListRow($model,'proceMedicina_id', Procemedicina::model()->listData()); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Cadastrar' : 'Salvar',
		)); ?>
         <input type="button" class="btn btn-primary"  onclick="javascript: history.go(-1)" value="Voltar"> 
	</div>

<?php $this->endWidget(); ?>
