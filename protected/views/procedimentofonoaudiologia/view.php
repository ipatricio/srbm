<?php
$this->breadcrumbs=array(
	'Procedimentofonoaudiologias'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Procedimentofonoaudiologia','url'=>array('index')),
	array('label'=>'Create Procedimentofonoaudiologia','url'=>array('create')),
	array('label'=>'Update Procedimentofonoaudiologia','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Procedimentofonoaudiologia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procedimentofonoaudiologia','url'=>array('admin')),
);
?>

<h1>Procedimento Fonoaudiologia de<?php echo $model->paciente->nome; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		#'id',
		'data_proced',
		'tmf',
		'obs',
		'result_exames',
		'outros',
		'paciente.nome',
		'velocidadeFala_id',
		'loudess_id',
		'pitchVocal_id',
		'hiperHiponasalidade_id',
		'coordenacaoFonoarticular_id',
		'TipoArticular_id',
		'NivelComprometimento_id',
		'classificacaoFala_id',
		'ressonanciaVocal_id',
		'classificacaoFonoarticular_id',
		'proceMedicina_id',
	),
)); ?>
