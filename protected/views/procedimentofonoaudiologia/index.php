<?php
$this->breadcrumbs=array(
	'Procedimentofonoaudiologias',
);

$this->menu=array(
	array('label'=>'Create Procedimentofonoaudiologia','url'=>array('create')),
	array('label'=>'Manage Procedimentofonoaudiologia','url'=>array('admin')),
);
?>

<h1>Procedimentofonoaudiologias</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
