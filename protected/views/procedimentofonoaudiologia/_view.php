<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_proced')); ?>:</b>
	<?php echo CHtml::encode($data->data_proced); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tmf')); ?>:</b>
	<?php echo CHtml::encode($data->tmf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obs')); ?>:</b>
	<?php echo CHtml::encode($data->obs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('result_exames')); ?>:</b>
	<?php echo CHtml::encode($data->result_exames); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('outros')); ?>:</b>
	<?php echo CHtml::encode($data->outros); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paciente_id')); ?>:</b>
	<?php echo CHtml::encode($data->paciente_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('velocidadeFala_id')); ?>:</b>
	<?php echo CHtml::encode($data->velocidadeFala_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loudess_id')); ?>:</b>
	<?php echo CHtml::encode($data->loudess_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pitchVocal_id')); ?>:</b>
	<?php echo CHtml::encode($data->pitchVocal_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hiperHiponasalidade_id')); ?>:</b>
	<?php echo CHtml::encode($data->hiperHiponasalidade_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coordenacaoFonoarticular_id')); ?>:</b>
	<?php echo CHtml::encode($data->coordenacaoFonoarticular_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoArticular_id')); ?>:</b>
	<?php echo CHtml::encode($data->TipoArticular_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NivelComprometimento_id')); ?>:</b>
	<?php echo CHtml::encode($data->NivelComprometimento_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('classificacaoFala_id')); ?>:</b>
	<?php echo CHtml::encode($data->classificacaoFala_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ressonanciaVocal_id')); ?>:</b>
	<?php echo CHtml::encode($data->ressonanciaVocal_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('classificacaoFonoarticular_id')); ?>:</b>
	<?php echo CHtml::encode($data->classificacaoFonoarticular_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proceMedicina_id')); ?>:</b>
	<?php echo CHtml::encode($data->proceMedicina_id); ?>
	<br />

	*/ ?>

</div>