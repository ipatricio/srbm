<?php
$this->breadcrumbs=array(
	'Relatório de odontologia'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar relatório de odontologia','url'=>array('index')),
	array('label'=>'Gerenciar relatório odontologia','url'=>array('admin')),
);
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>