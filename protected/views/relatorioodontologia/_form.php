<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'relatorioodontologia-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Campos com <span class="required">*</span> são obrigatórios.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php echo $form->textFieldRow($model,'data_procedimento',array('class'=>'span4')); ?>
        <?php echo $form->textFieldRow($model,'paciente_id',
	array('class'=>'span5','value'=>Paciente::model()->findByPk($_GET['id'])->nome,'disabled'=>true)); ?>

	<?php echo $form->textAreaRow($model,'tratamento_medico',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'usa_protese',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'doencas_familiares',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'sofreu_intervencao_cirurgica_bucal_facial',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'problemas_odontologicos',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'proble_mastigacao',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'aparelho_ortodontico',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'dificul_fonacao',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'osteoporose',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php echo $form->textAreaRow($model,'qual_motivo_procurou',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Cadastrar' : 'Save',
		)); ?>
        <input type="button" class="btn btn-primary"  onclick="javascript: history.go(-1)" value="Voltar"> 
   
	</div>

<?php $this->endWidget(); ?>
