<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tratamento_medico')); ?>:</b>
	<?php echo CHtml::encode($data->tratamento_medico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usa_protese')); ?>:</b>
	<?php echo CHtml::encode($data->usa_protese); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('doencas_familiares')); ?>:</b>
	<?php echo CHtml::encode($data->doencas_familiares); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sofreu_intervencao_cirurgica_bucal_facial')); ?>:</b>
	<?php echo CHtml::encode($data->sofreu_intervencao_cirurgica_bucal_facial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('problemas_odontologicos')); ?>:</b>
	<?php echo CHtml::encode($data->problemas_odontologicos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proble_mastigacao')); ?>:</b>
	<?php echo CHtml::encode($data->proble_mastigacao); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('aparelho_ortodontico')); ?>:</b>
	<?php echo CHtml::encode($data->aparelho_ortodontico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dificul_fonacao')); ?>:</b>
	<?php echo CHtml::encode($data->dificul_fonacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('osteoporose')); ?>:</b>
	<?php echo CHtml::encode($data->osteoporose); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qual_motivo_procurou')); ?>:</b>
	<?php echo CHtml::encode($data->qual_motivo_procurou); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paciente_id')); ?>:</b>
	<?php echo CHtml::encode($data->paciente_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_procedimento')); ?>:</b>
	<?php echo CHtml::encode($data->data_procedimento); ?>
	<br />

	*/ ?>

</div>