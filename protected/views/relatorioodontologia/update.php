<?php
$this->breadcrumbs=array(
	'Relatorioodontologias'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Relatorioodontologia','url'=>array('index')),
	array('label'=>'Create Relatorioodontologia','url'=>array('create')),
	array('label'=>'View Relatorioodontologia','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Relatorioodontologia','url'=>array('admin')),
);
?>

<h1>Update Relatorioodontologia <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>