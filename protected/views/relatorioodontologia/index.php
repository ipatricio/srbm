<?php
$this->breadcrumbs=array(
	'Relatorioodontologias',
);

$this->menu=array(
	array('label'=>'Create Relatorioodontologia','url'=>array('create')),
	array('label'=>'Manage Relatorioodontologia','url'=>array('admin')),
);
?>

<h1>Relatorioodontologias</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
