<?php
$this->breadcrumbs=array(
	'Relatorioodontologias'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Relatorioodontologia','url'=>array('index')),
	array('label'=>'Create Relatorioodontologia','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('relatorioodontologia-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Relatorioodontologias</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'relatorioodontologia-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'tratamento_medico',
		'usa_protese',
		'doencas_familiares',
		'sofreu_intervencao_cirurgica_bucal_facial',
		'problemas_odontologicos',
		/*
		'proble_mastigacao',
		'aparelho_ortodontico',
		'dificul_fonacao',
		'osteoporose',
		'qual_motivo_procurou',
		'paciente_id',
		'data_procedimento',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
