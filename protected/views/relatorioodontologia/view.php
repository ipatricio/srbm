<?php
$this->breadcrumbs=array(
	'Relatorioodontologias'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Relatorioodontologia','url'=>array('index')),
	array('label'=>'Create Relatorioodontologia','url'=>array('create')),
	array('label'=>'Update Relatorioodontologia','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Relatorioodontologia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Relatorioodontologia','url'=>array('admin')),
);
?>

<h1>View Relatorioodontologia #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tratamento_medico',
		'usa_protese',
		'doencas_familiares',
		'sofreu_intervencao_cirurgica_bucal_facial',
		'problemas_odontologicos',
		'proble_mastigacao',
		'aparelho_ortodontico',
		'dificul_fonacao',
		'osteoporose',
		'qual_motivo_procurou',
		'paciente_id',
		'data_procedimento',
	),
)); ?>
