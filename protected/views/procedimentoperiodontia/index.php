<?php
$this->breadcrumbs=array(
	'Procedimentoperiodontias',
);

$this->menu=array(
	array('label'=>'Create Procedimentoperiodontia','url'=>array('create')),
	array('label'=>'Manage Procedimentoperiodontia','url'=>array('admin')),
);
?>

<h1>Procedimentoperiodontias</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
