<?php
$this->breadcrumbs=array(
	'Procedimentoperiodontias'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Procedimentoperiodontia','url'=>array('index')),
	array('label'=>'Create Procedimentoperiodontia','url'=>array('create')),
	array('label'=>'View Procedimentoperiodontia','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Procedimentoperiodontia','url'=>array('admin')),
);
?>

<h1>Update Procedimentoperiodontia <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>