<?php
$this->breadcrumbs=array(
	'Procedimentoperiodontias'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Procedimentoperiodontia','url'=>array('index')),
	array('label'=>'Manage Procedimentoperiodontia','url'=>array('admin')),
);
?>

<h1>Create Procedimentoperiodontia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>