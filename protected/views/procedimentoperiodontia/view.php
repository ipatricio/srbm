<?php
$this->breadcrumbs=array(
	'Procedimentoperiodontias'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Procedimentoperiodontia','url'=>array('index')),
	array('label'=>'Create Procedimentoperiodontia','url'=>array('create')),
	array('label'=>'Update Procedimentoperiodontia','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Procedimentoperiodontia','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Procedimentoperiodontia','url'=>array('admin')),
);
?>

<h1>View Procedimentoperiodontia #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'paciente_id',
	),
)); ?>
