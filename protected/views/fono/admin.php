<?php
$this->breadcrumbs=array(
	'Pacientes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Listar Paciente','url'=>array('/paciente/index')),
	array('label'=>'Criar Paciente','url'=>array('/paciente/create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('paciente-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo CHtml::link('Busca Avançada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">

<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'paciente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		#'id',
		'nome',
		#'nome_pai',
		#'nome_mae',
		#'data_nascimento',
		#'nome_conjuge',
		
		#'ocupacao',
		'cpf',
		/*'identidade',
		'orgao_emissor',
		'data_emissao',
		'uf',
		'cidade',
		'cidade_nascimento',
		'cep',
		'bairro',
		'endereco',
		'numero',
		'complemento',
		'email',
		'telefone',
		'celular',
		'telefone_trabalho',
		'N_carteira_plano_saude',
		'data_validade_carteira',
		'cns',*/
		'cod_paciente',
		'N_prontuario_liga',
		#'estadoCivil_id',
		#'sexo_id',
		#'nacionalidade_id',
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view}',
			'buttons'=>array(
		        'view' => array(
		            'label'=>'Visualizar',
		            'url'=>'Yii::app()->createUrl("fono/view", array("id"=>$data->id,"view"=>2))',
		        ),
			),
	),
	)
)); ?>
