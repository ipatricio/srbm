<?php
$this->breadcrumbs=array(
	'Pacientes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Listar Paciente','url'=>array('index')),
	array('label'=>'Criar Paciente','url'=>array('create')),
);

?>


    <a  href="<?php echo Yii::app()->baseUrl.'/relatoriofonoaudiologia/view'.'/'.$model->id ?>">Cadastro Específico de Fonoaudiologia</a> 
    <br><br>
     <a  href="<?php echo Yii::app()->baseUrl.'/anexos/view'.'?id='.$model->id.'&a=1'  ?>">Anexos de Fonoaudiologia</a>
    <br><br>
    <a  href="<?php echo  Yii::app()->baseUrl.'/procedimentofonoaudiologia/view/'.$model->id ?>">Procedimento de Fonoaudiologia</a>
