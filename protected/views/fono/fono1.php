<?php
$this->breadcrumbs=array(
	'Fonoaudiologia',
	
);

$this->menu=array(
	array('label'=>'Listar Paciente','url'=>array('index')),
	array('label'=>'Criar Paciente','url'=>array('create')),
);

?>


    <a  href="<?php echo Yii::app()->baseUrl.'/relatoriofonoaudiologia/create'.'?id='.$model->id ?>">Cadastro Específico de Fonoaudiologia</a> 
    <br><br>
     <a  href="<?php echo Yii::app()->baseUrl.'/anexos/create'.'?id='.$model->id.'&a=1'  ?>">Anexos de Fonoaudiologia</a>
    <br><br>
    <a  href="<?php echo  Yii::app()->baseUrl.'/procedimentofonoaudiologia/create?id='.$model->id ?>">Procedimento de Fonoaudiologia</a>
    <br><br>
    <a  href="<?php echo Yii::app()->baseUrl.'/fono/dadoGeral'.'?id='.$model->id ?>">Dados Gerais</a>
