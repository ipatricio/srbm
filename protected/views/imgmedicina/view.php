<?php
$this->breadcrumbs=array(
	'Imgmedicinas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Imgmedicina','url'=>array('index')),
	array('label'=>'Create Imgmedicina','url'=>array('create')),
	array('label'=>'Update Imgmedicina','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Imgmedicina','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Imgmedicina','url'=>array('admin')),
);
?>

<h1>View Imgmedicina #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
