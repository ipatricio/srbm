<?php
$this->breadcrumbs=array(
	'Imgmedicinas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Imgmedicina','url'=>array('index')),
	array('label'=>'Create Imgmedicina','url'=>array('create')),
	array('label'=>'View Imgmedicina','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Imgmedicina','url'=>array('admin')),
);
?>

<h1>Update Imgmedicina <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>