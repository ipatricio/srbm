<?php
$this->breadcrumbs=array(
	'Imgmedicinas',
);

$this->menu=array(
	array('label'=>'Create Imgmedicina','url'=>array('create')),
	array('label'=>'Manage Imgmedicina','url'=>array('admin')),
);
?>

<h1>Imgmedicinas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
