<?php
$this->breadcrumbs=array(
	'Imgmedicinas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Imgmedicina','url'=>array('index')),
	array('label'=>'Manage Imgmedicina','url'=>array('admin')),
);
?>

<h1>Create Imgmedicina</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>