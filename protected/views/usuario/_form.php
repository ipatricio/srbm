<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'usuario-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Campos com <span class="required">*</span> são obrigatórios.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <div class="form-group col-lg-6">

	<?php echo $form->textFieldRow($model,'nome',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'data_nascimento',array('class'=>'span5')); ?>
        </div>    

	<?php echo $form->textFieldRow($model,'cpf',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'identidade',array('class'=>'span5','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'uf',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cidade',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cep',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'endereco',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'complemento',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'numero',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'bairro',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'telefone',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'instituicao_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'vinculoProjeto_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'areaUser_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sexo_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'estadoCivil_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'grupo_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
