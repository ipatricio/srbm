<?php
$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Usuario','url'=>array('index')),
	array('label'=>'Create Usuario','url'=>array('create')),
	array('label'=>'Update Usuario','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Usuario','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Usuario','url'=>array('admin')),
);
?>

<h1>View Usuario #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
		'data_nascimento',
		'cpf',
		'identidade',
		'uf',
		'cidade',
		'cep',
		'endereco',
		'complemento',
		'numero',
		'email',
		'bairro',
		'telefone',
		'instituicao_id',
		'vinculoProjeto_id',
		'areaUser_id',
		'sexo_id',
		'estadoCivil_id',
		'grupo_id',
	),
)); ?>
