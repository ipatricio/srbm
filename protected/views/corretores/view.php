<?php
$this->breadcrumbs=array(
	'Corretores'=>array('index'),
	$model->idcorretores,
);

$this->menu=array(
	array('label'=>'List Corretores','url'=>array('index')),
	array('label'=>'Create Corretores','url'=>array('create')),
	array('label'=>'Update Corretores','url'=>array('update','id'=>$model->idcorretores)),
	array('label'=>'Delete Corretores','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->idcorretores),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Corretores','url'=>array('admin')),
);
?>

<h1>View Corretores #<?php echo $model->idcorretores; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'idcorretores',
		'nome',
		'telefone',
		'email',
	),
)); ?>
