<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idcorretores')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idcorretores),array('view','id'=>$data->idcorretores)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefone')); ?>:</b>
	<?php echo CHtml::encode($data->telefone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />


</div>