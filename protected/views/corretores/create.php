<?php
$this->breadcrumbs=array(
	'Corretores'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Corretores','url'=>array('index')),
	array('label'=>'Manage Corretores','url'=>array('admin')),
);
?>

<h1>Create Corretores</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>