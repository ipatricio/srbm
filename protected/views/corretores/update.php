<?php
$this->breadcrumbs=array(
	'Corretores'=>array('index'),
	$model->idcorretores=>array('view','id'=>$model->idcorretores),
	'Update',
);

$this->menu=array(
	array('label'=>'List Corretores','url'=>array('index')),
	array('label'=>'Create Corretores','url'=>array('create')),
	array('label'=>'View Corretores','url'=>array('view','id'=>$model->idcorretores)),
	array('label'=>'Manage Corretores','url'=>array('admin')),
);
?>

<h1>Update Corretores <?php echo $model->idcorretores; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>