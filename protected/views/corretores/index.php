<?php
$this->breadcrumbs=array(
	'Corretores',
);

$this->menu=array(
	array('label'=>'Create Corretores','url'=>array('create')),
	array('label'=>'Manage Corretores','url'=>array('admin')),
);
?>

<h1>Corretores</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
