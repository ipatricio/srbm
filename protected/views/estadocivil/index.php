<?php
$this->breadcrumbs=array(
	'Estadocivils',
);

$this->menu=array(
	array('label'=>'Create Estadocivil','url'=>array('create')),
	array('label'=>'Manage Estadocivil','url'=>array('admin')),
);
?>

<h1>Estadocivils</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
