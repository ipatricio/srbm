<?php
$this->breadcrumbs=array(
	'Estadocivils'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Estadocivil','url'=>array('index')),
	array('label'=>'Create Estadocivil','url'=>array('create')),
	array('label'=>'View Estadocivil','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Estadocivil','url'=>array('admin')),
);
?>

<h1>Update Estadocivil <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>