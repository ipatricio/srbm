<?php
$this->breadcrumbs=array(
	'Estadocivils'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Estadocivil','url'=>array('index')),
	array('label'=>'Manage Estadocivil','url'=>array('admin')),
);
?>

<h1>Create Estadocivil</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>