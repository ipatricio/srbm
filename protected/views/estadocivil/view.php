<?php
$this->breadcrumbs=array(
	'Estadocivils'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Estadocivil','url'=>array('index')),
	array('label'=>'Create Estadocivil','url'=>array('create')),
	array('label'=>'Update Estadocivil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Estadocivil','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Estadocivil','url'=>array('admin')),
);
?>

<h1>View Estadocivil #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
