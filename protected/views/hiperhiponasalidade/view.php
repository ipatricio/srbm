<?php
$this->breadcrumbs=array(
	'Hiperhiponasalidades'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Hiperhiponasalidade','url'=>array('index')),
	array('label'=>'Create Hiperhiponasalidade','url'=>array('create')),
	array('label'=>'Update Hiperhiponasalidade','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Hiperhiponasalidade','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Hiperhiponasalidade','url'=>array('admin')),
);
?>

<h1>View Hiperhiponasalidade #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
