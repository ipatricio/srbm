<?php
$this->breadcrumbs=array(
	'Hiperhiponasalidades'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Hiperhiponasalidade','url'=>array('index')),
	array('label'=>'Create Hiperhiponasalidade','url'=>array('create')),
	array('label'=>'View Hiperhiponasalidade','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Hiperhiponasalidade','url'=>array('admin')),
);
?>

<h1>Update Hiperhiponasalidade <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>