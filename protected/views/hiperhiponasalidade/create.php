<?php
$this->breadcrumbs=array(
	'Hiperhiponasalidades'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Hiperhiponasalidade','url'=>array('index')),
	array('label'=>'Manage Hiperhiponasalidade','url'=>array('admin')),
);
?>

<h1>Create Hiperhiponasalidade</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>