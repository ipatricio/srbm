<?php
$this->breadcrumbs=array(
	'Hiperhiponasalidades',
);

$this->menu=array(
	array('label'=>'Create Hiperhiponasalidade','url'=>array('create')),
	array('label'=>'Manage Hiperhiponasalidade','url'=>array('admin')),
);
?>

<h1>Hiperhiponasalidades</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
