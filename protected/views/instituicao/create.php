<?php
$this->breadcrumbs=array(
	'Instituicaos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Instituicao','url'=>array('index')),
	array('label'=>'Manage Instituicao','url'=>array('admin')),
);
?>

<h1>Create Instituicao</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>