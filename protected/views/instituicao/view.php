<?php
$this->breadcrumbs=array(
	'Instituicaos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Instituicao','url'=>array('index')),
	array('label'=>'Create Instituicao','url'=>array('create')),
	array('label'=>'Update Instituicao','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Instituicao','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Instituicao','url'=>array('admin')),
);
?>

<h1>View Instituicao #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
