<?php
$this->breadcrumbs=array(
	'Instituicaos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Instituicao','url'=>array('index')),
	array('label'=>'Create Instituicao','url'=>array('create')),
	array('label'=>'View Instituicao','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Instituicao','url'=>array('admin')),
);
?>

<h1>Update Instituicao <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>