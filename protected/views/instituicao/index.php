<?php
$this->breadcrumbs=array(
	'Instituicaos',
);

$this->menu=array(
	array('label'=>'Create Instituicao','url'=>array('create')),
	array('label'=>'Manage Instituicao','url'=>array('admin')),
);
?>

<h1>Instituicaos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
