<?php
$this->breadcrumbs=array(
	'Laboratorios'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Laboratorio','url'=>array('index')),
	array('label'=>'Create Laboratorio','url'=>array('create')),
	array('label'=>'Update Laboratorio','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Laboratorio','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Laboratorio','url'=>array('admin')),
);
?>

<h1>View Laboratorio #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
		'cnpj',
		'cnae',
		'uf',
		'cidade',
		'logradouro',
		'numero',
		'cep',
		'email',
		'telefone1',
		'telefone2',
		'nome_resp',
		'datadenascimento',
		'cpf',
		'identidade',
		'uf_resp',
		'cidade_resp',
		'cep_resp',
		'endereco_resp',
		'complemento',
		'numero_resp',
		'email_resp',
		'qualificacao',
		'telefone1_resp',
		'telefone2_resp',
		'bairro_lab',
		'bairro_resp',
		'sexo_id',
		'nacionalidade_id',
		'estadoCivil_id',
	),
)); ?>
