<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cnpj')); ?>:</b>
	<?php echo CHtml::encode($data->cnpj); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cnae')); ?>:</b>
	<?php echo CHtml::encode($data->cnae); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uf')); ?>:</b>
	<?php echo CHtml::encode($data->uf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cidade')); ?>:</b>
	<?php echo CHtml::encode($data->cidade); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logradouro')); ?>:</b>
	<?php echo CHtml::encode($data->logradouro); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('numero')); ?>:</b>
	<?php echo CHtml::encode($data->numero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cep')); ?>:</b>
	<?php echo CHtml::encode($data->cep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefone1')); ?>:</b>
	<?php echo CHtml::encode($data->telefone1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefone2')); ?>:</b>
	<?php echo CHtml::encode($data->telefone2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_resp')); ?>:</b>
	<?php echo CHtml::encode($data->nome_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datadenascimento')); ?>:</b>
	<?php echo CHtml::encode($data->datadenascimento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cpf')); ?>:</b>
	<?php echo CHtml::encode($data->cpf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('identidade')); ?>:</b>
	<?php echo CHtml::encode($data->identidade); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uf_resp')); ?>:</b>
	<?php echo CHtml::encode($data->uf_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cidade_resp')); ?>:</b>
	<?php echo CHtml::encode($data->cidade_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cep_resp')); ?>:</b>
	<?php echo CHtml::encode($data->cep_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endereco_resp')); ?>:</b>
	<?php echo CHtml::encode($data->endereco_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('complemento')); ?>:</b>
	<?php echo CHtml::encode($data->complemento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero_resp')); ?>:</b>
	<?php echo CHtml::encode($data->numero_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_resp')); ?>:</b>
	<?php echo CHtml::encode($data->email_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qualificacao')); ?>:</b>
	<?php echo CHtml::encode($data->qualificacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefone1_resp')); ?>:</b>
	<?php echo CHtml::encode($data->telefone1_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefone2_resp')); ?>:</b>
	<?php echo CHtml::encode($data->telefone2_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bairro_lab')); ?>:</b>
	<?php echo CHtml::encode($data->bairro_lab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bairro_resp')); ?>:</b>
	<?php echo CHtml::encode($data->bairro_resp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sexo_id')); ?>:</b>
	<?php echo CHtml::encode($data->sexo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nacionalidade_id')); ?>:</b>
	<?php echo CHtml::encode($data->nacionalidade_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estadoCivil_id')); ?>:</b>
	<?php echo CHtml::encode($data->estadoCivil_id); ?>
	<br />

	*/ ?>

</div>