<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nome',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cnpj',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'cnae',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($model,'uf',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cidade',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'logradouro',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'numero',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cep',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'telefone1',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'telefone2',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'nome_resp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'datadenascimento',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cpf',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'identidade',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'uf_resp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cidade_resp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'cep_resp',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'endereco_resp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'complemento',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'numero_resp',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'email_resp',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'qualificacao',array('class'=>'span5','maxlength'=>45)); ?>

	<?php echo $form->textFieldRow($model,'telefone1_resp',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'telefone2_resp',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bairro_lab',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'bairro_resp',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'sexo_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nacionalidade_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'estadoCivil_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
