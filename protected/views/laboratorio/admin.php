<?php
$this->breadcrumbs=array(
	'Laboratorios'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Laboratorio','url'=>array('index')),
	array('label'=>'Create Laboratorio','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('laboratorio-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Laboratorios</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'laboratorio-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nome',
		'cnpj',
		'cnae',
		'uf',
		'cidade',
		/*
		'logradouro',
		'numero',
		'cep',
		'email',
		'telefone1',
		'telefone2',
		'nome_resp',
		'datadenascimento',
		'cpf',
		'identidade',
		'uf_resp',
		'cidade_resp',
		'cep_resp',
		'endereco_resp',
		'complemento',
		'numero_resp',
		'email_resp',
		'qualificacao',
		'telefone1_resp',
		'telefone2_resp',
		'bairro_lab',
		'bairro_resp',
		'sexo_id',
		'nacionalidade_id',
		'estadoCivil_id',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
