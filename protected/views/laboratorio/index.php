<?php
$this->breadcrumbs=array(
	'Laboratorios',
);

$this->menu=array(
	array('label'=>'Create Laboratorio','url'=>array('create')),
	array('label'=>'Manage Laboratorio','url'=>array('admin')),
);
?>

<h1>Laboratorios</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
