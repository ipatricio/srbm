<?php
$this->breadcrumbs=array(
	'Cidcancers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Cidcancer','url'=>array('index')),
	array('label'=>'Create Cidcancer','url'=>array('create')),
	array('label'=>'Update Cidcancer','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Cidcancer','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cidcancer','url'=>array('admin')),
);
?>

<h1>View Cidcancer #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
	),
)); ?>
