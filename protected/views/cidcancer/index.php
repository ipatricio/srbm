<?php
$this->breadcrumbs=array(
	'Cidcancers',
);

$this->menu=array(
	array('label'=>'Create Cidcancer','url'=>array('create')),
	array('label'=>'Manage Cidcancer','url'=>array('admin')),
);
?>

<h1>Cidcancers</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
