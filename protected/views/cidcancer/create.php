<?php
$this->breadcrumbs=array(
	'Cidcancers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Cidcancer','url'=>array('index')),
	array('label'=>'Manage Cidcancer','url'=>array('admin')),
);
?>

<h1>Create Cidcancer</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>