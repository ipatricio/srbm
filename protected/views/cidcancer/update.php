<?php
$this->breadcrumbs=array(
	'Cidcancers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Cidcancer','url'=>array('index')),
	array('label'=>'Create Cidcancer','url'=>array('create')),
	array('label'=>'View Cidcancer','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Cidcancer','url'=>array('admin')),
);
?>

<h1>Update Cidcancer <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>