<?php
session_start();
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user = Login::model()->find('usuario=:nome AND senha=:senha',
					array( 
						":nome" => $this->username,
						":senha" => md5($this->password),						
						)
			);

		if($user != null){
			$this->errorCode=self::ERROR_NONE;
			
			$this->_id = $user->grupos_id;

		}
		return !$this->errorCode;
	}

	public function getId(){
		return $this->_id;
	}
}