<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php /* $this->beginWidget('bootstrap.widgets.TbHeroUnit',array(
    'heading'=>'Welcome to '.CHtml::encode(Yii::app()->name),
)); ?>


<p>Congratulations! You have successfully created your Yii application.</p>
<?php $this->endWidget(); */?>


<?php $this->widget('bootstrap.widgets.TbCarousel', array(
    'items'=>array(
        array('image'=> Yii::app()->theme->baseUrl.'/images/LIGA2.jpg' , 'label'=>'First Thumbnail',
          'caption'=>'Algum texto aqui!',
          'imageOptions'=>array('style'=>'width:1200px; height:450px;'),
          ),
        array('image'=>'http://placehold.it/770x400&text=Second+thumbnail',
         'label'=>'Second Thumbnail label', 'caption'=>'Algum texto aqui!',
         'imageOptions'=>array('style'=>'width:1200px; height:450px;'),
         ),
        array('image'=>'http://placehold.it/770x400&text=Third+thumbnail',
         'label'=>'Third Thumbnail label', 'caption'=>'Algum texto aqui!',
         'imageOptions'=>array('style'=>'width:1200px; height:450px;'),
         ),
    ),
)); ?>

<p>You may change the content of this page by modifying the following two files:</p>

<ul>
    <li>View file: <code><?php echo __FILE__; ?></code></li>
    <li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul>

<p>For more details on how to further develop this application, please read
    the <a href="http://www.yiiframework.com/doc/">documentation</a>.
    Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
    should you have any questions.</p>
