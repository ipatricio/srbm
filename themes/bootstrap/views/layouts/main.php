<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php Yii::app()->bootstrap->register(); ?>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
	'brand' => 'SRBM',
	'fixed' => true,
	'fluid' => false,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'type'=>'navbar',
            'items'=>Login::buildMenu(),
    ),
))); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>



</div><!-- page -->

<div id="footer" class='navbar navbar-bottom'>
		<div class='text-center'>
		Copyright &copy; <?php echo date('Y'); ?> ISR.<br/>
		Todos os direitos reservados.<br/>
		</div>
	</div><!-- footer -->	
</body>
</html>
